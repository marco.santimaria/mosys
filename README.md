# Mosys Operating System

This is currently designed for use with [rosco_m68k](https://rosco-m68k.com/).
Currently builds for [tags/v2.2](https://github.com/rosco-m68k/rosco_m68k/tree/v2.2).

This project is unstable, and in early development. The kernel API will never be stable. The user API is not currently stable.

The [mosys-user](https://gitlab.com/0xTJ/mosys-user) repository needs to be cloned as adjacent to [mosys](https://gitlab.com/0xTJ/mosys).

For rosco_m68k, use [moboot](https://gitlab.com/0xTJ/moboot) to load output ELF file from an SD card, or boot `ROSCODE1.BIN` using Kermit or an SD card.

## Configuration

Kernel configuration is done in `include/config.h`. This includes default console, networking, etc.

Kernel information is done in `include/info.h`. This includes kernel version, release status, etc.

## Building

To build, use `cmake`, targetting the root of the repository, with the toolchain set to one in the `toolchains` directory (e.g. `mkdir build && cd build && cmake -DCMAKE_TOOLCHAIN_FILE=../toolchains/m68k-none.cmake .. && make`).

To increase build speed, pass the argument `-j<thread count>` to `make` (e.g. `make -j6`).

### Undefined behaviour sanitizer

To enable kernel UB instrumentation, uncomment `add_compile_options(-fsanitize=undefined)` in `CMakeLists.txt`. This will significantly increase the size of the produced binary and decrease performance.
