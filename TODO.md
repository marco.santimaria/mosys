# To-Do List

* Extend, instead of replacing, PDCLib headers, where possible.
* Integrate sockets and networking
* Signals
* Make all header guards are consistent in PDCLib
* Use proper export defintion for all PDCLib symbols
* Reimplement console properly
* Fix r1 rosco_m68k support
* Add UART ioctl
* Use better OOP patterns
* Fix 68901 as TTY
* Add . and .. directory entries
