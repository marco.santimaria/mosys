#pragma once

#include <assert.h>
#include <stdint.h>

enum exception_vector {
    EXCEPTION_VECTOR_START                                      = 0,

    EXCEPTION_VECTOR_RESET_INITIAL_INTERRUPT_STACK_POINTER      = 0,
    EXCEPTION_VECTOR_RESET_INITIAL_PROGRAM_COUNTER              = 1,
    EXCEPTION_VECTOR_ACCESS_FAULT                               = 2,
    EXCEPTION_VECTOR_ADDRESS_ERROR                              = 3,

    EXCEPTION_VECTOR_ILLEGAL_INSTRUCTION                        = 4,
    EXCEPTION_VECTOR_INTEGER_DIVIDE_BY_ZERO                     = 5,
    EXCEPTION_VECTOR_CHK_CHK2_INSTRUCTIONS                      = 6,
    EXCEPTION_VECTOR_FTRAPCC_TRAPCC_TRAPV_INSTRUCTIONS          = 7,

    EXCEPTION_VECTOR_PRIVILEGE_VIOLATION                        = 8,
    EXCEPTION_VECTOR_TRACE                                      = 9,
    EXCEPTION_VECTOR_LINE_1010_EMULATOR                         = 10,
    EXCEPTION_VECTOR_LINE_1111_EMULATOR                         = 11,

    EXCEPTION_VECTOR_COPROCESSOR_PROTOCOL_VIOLATION             = 13,
    EXCEPTION_VECTOR_FORMAT_ERROR                               = 14,
    EXCEPTION_VECTOR_UNINITIALIZED_INTERRUPT                    = 15,

    EXCEPTION_VECTOR_SPURIOUS_INTERRUPT                         = 24,
    EXCEPTION_VECTOR_LEVEL_1_INTERRUPT_AUTOVECTOR               = 25,
    EXCEPTION_VECTOR_LEVEL_2_INTERRUPT_AUTOVECTOR               = 26,
    EXCEPTION_VECTOR_LEVEL_3_INTERRUPT_AUTOVECTOR               = 27,

    EXCEPTION_VECTOR_LEVEL_4_INTERRUPT_AUTOVECTOR               = 28,
    EXCEPTION_VECTOR_LEVEL_5_INTERRUPT_AUTOVECTOR               = 29,
    EXCEPTION_VECTOR_LEVEL_6_INTERRUPT_AUTOVECTOR               = 30,
    EXCEPTION_VECTOR_LEVEL_7_INTERRUPT_AUTOVECTOR               = 31,

    EXCEPTION_VECTOR_TRAP_INSTRUCTION_VECTORS                   = 32,

    EXCEPTION_VECTOR_FP_BRANCH_OR_SET_ON_UNORDERED_CONDITION    = 48,
    EXCEPTION_VECTOR_FP_INEXACT_RESULT                          = 49,
    EXCEPTION_VECTOR_FP_DIVIDE_BY_ZERO                          = 50,
    EXCEPTION_VECTOR_FP_UNDERFLOW                               = 51,

    EXCEPTION_VECTOR_FP_OPERAND_ERROR                           = 52,
    EXCEPTION_VECTOR_FP_OVERFLOW                                = 53,
    EXCEPTION_VECTOR_FP_SIGNALING_NAN                           = 54,
    EXCEPTION_VECTOR_FP_UNIMPLEMENTED_DATA_TYPE                 = 55,

    EXCEPTION_VECTOR_MMU_CONFIGURATION_ERROR                    = 56,
    EXCEPTION_VECTOR_MMU_ILLEGAL_OPERATION_ERROR                = 57,
    EXCEPTION_VECTOR_MMU_ACCESS_LEVEL_VIOLATION_ERROR           = 58,

    EXCEPTION_VECTOR_USER_DEFINED_VECTORS                       = 64,

    EXCEPTION_VECTOR_END                                        = 256
};

struct exception_state_format_0x0 {
    uint16_t _dummy;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x0) == (4 - 4) * sizeof(uint16_t) + sizeof(uint16_t));

struct exception_state_format_0x1 {
    uint16_t _dummy;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x1) == (4 - 4) * sizeof(uint16_t) + sizeof(uint16_t));

struct exception_state_format_0x2 {
    uint32_t address;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x2) == (6 - 4) * sizeof(uint16_t));

struct exception_state_format_0x3 {
    uint32_t effective_address;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x3) == (6 - 4) * sizeof(uint16_t));

struct exception_state_format_0x4 {
    uint32_t effective_address;
    uint32_t pc_of_faulted_instruction;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x4) == (8 - 4) * sizeof(uint16_t));

struct exception_state_format_0x7 {
    uint32_t effective_address;
    uint16_t special_status_word;
    uint16_t writeback_3_status;
    uint16_t writeback_2_status;
    uint16_t writeback_1_status;
    uint32_t fault_address;
    uint32_t writeback_3_address;
    uint32_t writeback_3_data;
    uint32_t writeback_2_address;
    uint32_t writeback_2_data;
    uint32_t writeback_1_address;
    uint32_t writeback_1_data_push_data_lw0;
    uint32_t push_data_lw1;
    uint32_t push_data_lw2;
    uint32_t push_data_lw3;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x7) == (30 - 4) * sizeof(uint16_t));

struct exception_state_format_0x8 {
    uint16_t special_status_word;
    uint32_t fault_address;
    uint16_t _reserved_0;
    uint16_t data_output_buffer;
    uint16_t _reserved_1;
    uint16_t data_input_buffer;
    uint16_t _reserved_2;
    uint16_t instruction_output_buffer;
    uint16_t internal_information[16];
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x8) == (29 - 4) * sizeof(uint16_t));

struct exception_state_format_0x9 {
    uint32_t instruction_address;
    uint16_t internal_registers_0[4];
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0x9) == (10 - 4) * sizeof(uint16_t));

struct exception_state_format_0xa {
    uint16_t internal_register_0;
    uint16_t special_status_word;
    uint16_t instruction_pipe_stage_c;
    uint16_t instruction_pipe_stage_b;
    uint32_t data_cycle_fault_address;
    uint16_t internal_register_1;
    uint16_t internal_register_2;
    uint32_t data_output_buffer;
    uint16_t internal_register_3;
    uint16_t internal_register_4;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0xa) == (16 - 4) * sizeof(uint16_t));

struct exception_state_format_0xb {
    uint16_t internal_register_0;
    uint16_t special_status_word;
    uint16_t instruction_pipe_stage_c;
    uint16_t instruction_pipe_stage_b;
    uint32_t data_cycle_fault_address;
    uint16_t internal_register_1;
    uint16_t internal_register_2;
    uint32_t data_output_buffer;
    uint16_t internal_registers_3[4];
    uint32_t stage_b_address;
    uint16_t internal_registers_4[2];
    uint32_t data_input_buffer;
    uint16_t internal_registers_5[3];
    uint16_t version_number_internal_information;
    uint16_t internal_registers_6[18];
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0xb) == (46 - 4) * sizeof(uint16_t));

struct exception_state_format_0xc {
    uint32_t faulted_address;
    union {
        struct {
            uint32_t dbuf;
            uint32_t current_instruction_pc;
        };
        struct {
            uint16_t pre_exception_status_register;
            uint16_t faulted_exception_format_vector_word;
            uint32_t faulted_instruction_program_counter;
        };
    };
    uint16_t internal_transfer_count_register;
    uint16_t special_status_word;
} __attribute__((packed));
static_assert(sizeof(struct exception_state_format_0xc) == (12 - 4) * sizeof(uint16_t));

struct exception_state {
    // Kernel-stacked words
    uint32_t d[8];
    uint32_t a[7];
    uint32_t usp;

    // CPU-stacked words
    uint16_t sr;
    uint32_t pc;
    uint16_t format_vector_offset;

    // Format-dependant CPU-stacked words
    union {
        struct exception_state_format_0x0 fmt_0x0;
        struct exception_state_format_0x1 fmt_0x1;
        struct exception_state_format_0x2 fmt_0x2;
        struct exception_state_format_0x3 fmt_0x3;
        struct exception_state_format_0x7 fmt_0x7;
        struct exception_state_format_0x8 fmt_0x8;
        struct exception_state_format_0x9 fmt_0x9;
        struct exception_state_format_0xa fmt_0xa;
        struct exception_state_format_0xb fmt_0xb;
        struct exception_state_format_0xc fmt_0xc;
    };
} __attribute__((packed));

typedef void (*exception_raw_func)(void);
typedef void (*exception_func)(struct exception_state *state);

// These two functions should not be called from C
// They don't expect a return address on the stack
void exception_entry(void);
void exception_leave(void);

exception_raw_func exception_vector_raw_replace(exception_raw_func function, enum exception_vector vector);
exception_func exception_vector_replace(exception_func function, enum exception_vector vector);

void exception_init(void);
void exception(struct exception_state *state);
void exception_stack_frame_print(struct exception_state *state, void *ssp);
