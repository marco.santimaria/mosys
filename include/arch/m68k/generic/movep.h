#pragma once

#include <stdint.h>

static inline __attribute__((always_inline)) uint16_t movep_from_w(const volatile void *addr, int16_t disp) {
    uint16_t data;
    if (!disp || !__builtin_constant_p((char *) addr + disp)) {
        __asm__ volatile (
            "movep%.w 0%[addr], %[data] \n"
            : [data] "=d" (data)
            : [addr] "Q" (*(uint16_t *) ((char *) addr + disp))
        );
    } else {
        __asm__ volatile (
            "movep%.w %[addr], %[data] \n"
            : [data] "=d" (data)
            : [addr] "U" (*(uint16_t *) ((char *) addr + disp))
        );
    }
    return data;
}

static inline __attribute__((always_inline)) uint32_t movep_from_l(const volatile void *addr, int16_t disp) {
    uint16_t data;
    if (!disp || !__builtin_constant_p((char *) addr + disp)) {
        __asm__ volatile (
            "movep%.l 0%[addr], %[data] \n"
            : [data] "=d" (data)
            : [addr] "Q" (*(uint32_t *) ((char *) addr + disp))
        );
    } else {
        __asm__ volatile (
            "movep%.l %[addr], %[data] \n"
            : [data] "=d" (data)
            : [addr] "U" (*(uint32_t *) ((char *) addr + disp))
        );
    }
    return data;
}

static inline __attribute__((always_inline)) void movep_to_w(volatile void *addr, int16_t disp, uint16_t data) {
    if (!disp || !__builtin_constant_p((char *) addr + disp)) {
        __asm__ volatile (
            "movep%.w %[data], 0%[addr] \n"
            :
            : [data] "d" (data),
              [addr] "Q" (*(uint16_t *) ((char *) addr + disp))
        );
    } else {
        __asm__ volatile (
            "movep%.w %[data], %[addr] \n"
            :
            : [data] "d" (data),
              [addr] "U" (*(uint16_t *) ((char *) addr + disp))
        );
    }
}

static inline __attribute__((always_inline)) void movep_to_l(volatile void *addr, int16_t disp, uint32_t data) {
    if (!disp || !__builtin_constant_p((char *) addr + disp)) {
        __asm__ volatile (
            "movep%.l %[data], 0%[addr] \n"
            :
            : [data] "d" (data),
              [addr] "Q" (*(uint32_t *) ((char *) addr + disp))
        );
    } else {
        __asm__ volatile (
            "movep%.l %[data], %[addr] \n"
            :
            : [data] "d" (data),
              [addr] "U" (*(uint32_t *) ((char *) addr + disp))
        );
    }
}
