#pragma once

#include "config.h"
#include "device/hardware/mc68681.h"
#include "mem/mem.h"

#if DEV_MC68681_ENABLE

static inline unsigned long mc68681_xtal_freq_get(void) {
    return 3686400;
}

#define MC68681_BASE    (mem_io_start + 0x01)

#define MC68681_MRA_R(base)     (base + 0x00)
#define MC68681_MRA_W(base)     (base + 0x00)
#define MC68681_SRA_R(base)     (base + 0x02)
#define MC68681_CSRA_W(base)    (base + 0x02)
#define MC68681_MISR_R(base)    (base + 0x04)
#define MC68681_CRA_W(base)     (base + 0x04)
#define MC68681_RHRA_R(base)    (base + 0x06)
#define MC68681_THRA_W(base)    (base + 0x06)
#define MC68681_IPCR_R(base)    (base + 0x08)
#define MC68681_ACR_W(base)     (base + 0x08)
#define MC68681_ISR_R(base)     (base + 0x0A)
#define MC68681_IMR_W(base)     (base + 0x0A)
#define MC68681_CTUR_R(base)    (base + 0x0C)
#define MC68681_CTUR_W(base)    (base + 0x0C)
#define MC68681_CTLR_R(base)    (base + 0x0E)
#define MC68681_CTLR_W(base)    (base + 0x0E)
#define MC68681_MRB_R(base)     (base + 0x10)
#define MC68681_MRB_W(base)     (base + 0x10)
#define MC68681_SRB_R(base)     (base + 0x12)
#define MC68681_CSRB_W(base)    (base + 0x12)
// Address (base + 0x14) read RESERVED
#define MC68681_CRB_W(base)     (base + 0x14)
#define MC68681_RHRB_R(base)    (base + 0x16)
#define MC68681_THRB_W(base)    (base + 0x16)
#define MC68681_IVR_R(base)     (base + 0x18)
#define MC68681_IVR_W(base)     (base + 0x18)
#define MC68681_IP_R(base)      (base + 0x1A)
#define MC68681_OPCR_W(base)    (base + 0x1A)
#define MC68681_SCC_R(base)     (base + 0x1C)
#define MC68681_SOPBC_W(base)   (base + 0x1C)
#define MC68681_STC_R(base)     (base + 0x1E)
#define MC68681_COPBC_W(base)   (base + 0x1E)

#endif
