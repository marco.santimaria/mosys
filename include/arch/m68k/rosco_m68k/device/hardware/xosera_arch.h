#pragma once

#include "config.h"
#include "movep.h"
#include "device/hardware/xosera.h"
#include <stdint.h>

#if DEV_XOSERA_ENABLE

#define XOSERA_BASE (mem_io_start + 0x60)

static inline uint16_t xosera_xm_get_w(volatile void *base, xosera_xm xm) {
    return movep_from_w(base, xm * 4);
}

static inline void xosera_xm_set_w(volatile void *base, xosera_xm xm, uint16_t value) {
    movep_to_w(base, xm * 4, value);
}

static inline uint32_t xosera_xm_get_l(volatile void *base, xosera_xm xm) {
    return movep_from_l(base, xm * 4);
}

static inline void xosera_xm_set_l(volatile void *base, xosera_xm xm, uint32_t value) {
    movep_to_l(base, xm * 4, value);
}

#endif
