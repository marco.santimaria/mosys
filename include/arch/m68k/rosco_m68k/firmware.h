#pragma once

#include <stdbool.h>
#include <stdint.h>

// Basic IO routines (TRAP 14)

typedef enum firmware_trap_14_func {
    FIRMWARE_TRAP_14_FUNC_PRINT = 0,
    FIRMWARE_TRAP_14_FUNC_PRINTLN = 1,
    FIRMWARE_TRAP_14_FUNC_SENDCHAR = 2,
    FIRMWARE_TRAP_14_FUNC_RECVCHAR = 3,
    FIRMWARE_TRAP_14_FUNC_PRINTCHAR = 4,
    FIRMWARE_TRAP_14_FUNC_SETCURSOR = 5,
    FIRMWARE_TRAP_14_FUNC_CHECKCHAR = 6,
} firmware_trap_14_func;

static inline const char *firmware_trap_14_print_const(const char *str) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_PRINT;
    register const char *str_reg __asm__ ("a0") = str;
    __asm__ volatile (
        "trap #14\n"
        : "+a" (str_reg)
        : "d" (func_code_reg)
    );
    return str_reg;
}
static inline char *firmware_trap_14_print_noconst(char *str) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_PRINT;
    register char *str_reg __asm__ ("a0") = str;
    __asm__ volatile (
        "trap #14\n"
        : "+a" (str_reg)
        : "d" (func_code_reg)
    );
    return str_reg;
}
#define firmware_trap_14_print(str) _Generic((str), \
                        char *          : firmware_trap_14_print_noconst,   \
    const               char *          : firmware_trap_14_print_const,     \
                        char *restrict  : firmware_trap_14_print_noconst,   \
    const               char *restrict  : firmware_trap_14_print_const,     \
                        void *          : firmware_trap_14_print_noconst,   \
    const               void *          : firmware_trap_14_print_const,     \
                        void *restrict  : firmware_trap_14_print_noconst,   \
    const               void *restrict  : firmware_trap_14_print_const,     \
)(str)

static inline const char *firmware_trap_14_println_const(const char *str) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_PRINTLN;
    register const char *str_reg __asm__ ("a0") = str;
    __asm__ volatile (
        "trap #14\n"
        : "+a" (str_reg)
        : "d" (func_code_reg)
    );
    return str_reg;
}
static inline char *firmware_trap_14_println_noconst(char *str) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_PRINTLN;
    register char *str_reg __asm__ ("a0") = str;
    __asm__ volatile (
        "trap #14\n"
        : "+a" (str_reg)
        : "d" (func_code_reg)
    );
    return str_reg;
}
#define firmware_trap_14_println(str) _Generic((str),   \
                        char *          : firmware_trap_14_println_noconst, \
    const               char *          : firmware_trap_14_println_const,   \
                        char *restrict  : firmware_trap_14_println_noconst, \
    const               char *restrict  : firmware_trap_14_println_const,   \
                        void *          : firmware_trap_14_println_noconst, \
    const               void *          : firmware_trap_14_println_const,   \
                        void *restrict  : firmware_trap_14_println_noconst, \
    const               void *restrict  : firmware_trap_14_println_const,   \
)(str)

static inline void firmware_trap_14_sendchar(char chr) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_SENDCHAR;
    register char chr_reg __asm__ ("d0") = chr;
    __asm__ volatile (
        "trap #14\n"
        : "+d" (chr_reg)
        : "d" (func_code_reg)
    );
}

static inline char firmware_trap_14_recvchar() {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_RECVCHAR;
    register char chr_reg __asm__ ("d0");
    __asm__ volatile (
        "trap #14\n"
        : "=d" (chr_reg)
        : "d" (func_code_reg)
    );
    return chr_reg;
}

static inline void firmware_trap_14_printchar(char chr) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_PRINTCHAR;
    register char chr_reg __asm__ ("d0") = chr;
    __asm__ volatile (
        "trap #14\n"
        : "+d" (chr_reg)
        : "d" (func_code_reg)
    );
}

static inline void firmware_trap_14_setcursor(bool show_cursor) {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_SETCURSOR;
    register char show_cursor_reg __asm__ ("d0") = show_cursor;
    __asm__ volatile (
        "trap #14\n"
        : "+d" (show_cursor_reg)
        : "d" (func_code_reg)
    );
}

static inline bool firmware_trap_14_checkchar() {
    register firmware_trap_14_func func_code_reg __asm__ ("d1") = FIRMWARE_TRAP_14_FUNC_CHECKCHAR;
    register char chr_waiting_reg __asm__ ("d0");
    __asm__ volatile (
        "trap #14\n"
        : "=d" (chr_waiting_reg)
        : "d" (func_code_reg)
    );
    return chr_waiting_reg;
}

// Basic System Data Block (SDB)

#define FIRMWARE_SDB_MAGIC 0xB105DA7A

typedef struct firmware_system_data_block {
    uint32_t magic;
    uint32_t oshi_code;
    uint16_t heartbeat_counter;
    uint16_t heartbeat_frequency;
    uint32_t upticks;
    uint32_t e68k_reserved;
    uint32_t memsize;
    uint32_t uartbase;
    uint32_t cpu_model : 3;
    uint32_t cpu_speed : 29;
} __attribute__ ((packed)) firmware_system_data_block;

extern firmware_system_data_block _SDB;
