#pragma once

#include "device/hardware/mc68681.h"
#include "util/util.h"
#include <stdbool.h>
#include <stdint.h>

#define GPIO_VERIFY 0

typedef enum gpio_num {
    GPIO_NUM_IP0            = 0x00,
    GPIO_NUM_IP1            = 0x01,
    GPIO_NUM_IP2            = 0x02,
    GPIO_NUM_IP3            = 0x03,
    GPIO_NUM_IP4            = 0x04,
    GPIO_NUM_IP5            = 0x05,

    GPIO_NUM_OP0            = 0x10,
    GPIO_NUM_OP1            = 0x11,
    GPIO_NUM_OP2            = 0x12,
    GPIO_NUM_OP3            = 0x13,
    GPIO_NUM_OP4            = 0x14,
    GPIO_NUM_OP5            = 0x15,
    GPIO_NUM_OP6            = 0x16,
    GPIO_NUM_OP7            = 0x17,

    GPIO_NUM_UART_CTSA      = GPIO_NUM_IP0,
    GPIO_NUM_UART_CTSB      = GPIO_NUM_IP1,
    GPIO_NUM_SPI_MISO       = GPIO_NUM_IP2,

    GPIO_NUM_UART_RTSA      = GPIO_NUM_OP0,
    GPIO_NUM_UART_RTSB      = GPIO_NUM_OP1,
    GPIO_NUM_SPI_CS0        = GPIO_NUM_OP2,
    GPIO_NUM_LED_RED        = GPIO_NUM_OP3,
    GPIO_NUM_SPI_SCLK       = GPIO_NUM_OP4,
    GPIO_NUM_LED_GREEN      = GPIO_NUM_OP5,
    GPIO_NUM_SPI_MOSI       = GPIO_NUM_OP6,
    GPIO_NUM_SPI_CS1        = GPIO_NUM_OP7,

    GPIO_NUM_INPUTS_START   = GPIO_NUM_IP0,
    GPIO_NUM_INPUTS_END     = GPIO_NUM_IP5 + 1,
    GPIO_NUM_OUTPUTS_START  = GPIO_NUM_OP0,
    GPIO_NUM_OUTPUTS_END    = GPIO_NUM_OP7 + 1,
} gpio_num;

typedef uint32_t gpio_mask;
static inline gpio_mask gpio_to_mask(gpio_num num) { return UINT32_C(1) << num; }

typedef enum gpio_pindir {
    GPIO_PINDIR_INPUT,
    GPIO_PINDIR_OUTPUT,
} gpio_pindir;

#if GPIO_VERIFY
void gpio_assert_valid_pindir(gpio_mask mask, gpio_pindir pindir);
#endif
static inline void gpio_pindir_set(gpio_mask mask, gpio_pindir pindir);
static inline bool gpio_input_read(gpio_num num);
static inline gpio_mask gpio_input_read_mask(gpio_mask mask);
static inline gpio_mask gpio_input_read_mask_all(void);
static inline void gpio_output_write(gpio_num num, bool state);
static inline void gpio_output_write_mask(gpio_mask mask, gpio_mask state);
static inline void gpio_output_set(gpio_mask mask);
static inline void gpio_output_clear(gpio_mask mask);

static inline void gpio_pindir_set(gpio_mask mask, gpio_pindir pindir) {
#if GPIO_VERIFY
    gpio_assert_valid_pindir(mask, pindir);
#else
    UNUSED(mask);
#endif
    if (pindir == GPIO_PINDIR_INPUT) {
        // Pins are permanent inputs
    } else if (pindir == GPIO_PINDIR_OUTPUT) {
        // Pins are permanent outputs
    }
}

static inline bool gpio_input_read(gpio_num num) {
    return (gpio_input_read_mask(gpio_to_mask(num)) >> num) & 1;
}

static inline gpio_mask gpio_input_read_mask(gpio_mask mask) {
#if GPIO_VERIFY
    gpio_assert_valid_pindir(mask, GPIO_PINDIR_INPUT);
#endif
    return gpio_input_read_mask_all() & mask;
}

static inline gpio_mask gpio_input_read_mask_all(void) {
    gpio_mask result = (gpio_mask) mc68681_ip_get() << GPIO_NUM_INPUTS_START;
    result &= 0x3F;
    return result;
}

static inline void gpio_output_write(gpio_num num, bool state) {
    if (state) {
        gpio_output_set(gpio_to_mask(num));
    } else {
        gpio_output_clear(gpio_to_mask(num));
    }
}

static inline void gpio_output_write_mask(gpio_mask mask, gpio_mask state) {
    const gpio_mask set_mask = mask & state;
    const gpio_mask clear_mask = mask & ~state;
    if (set_mask) gpio_output_set(set_mask);
    if (clear_mask) gpio_output_clear(clear_mask);
}

static inline void gpio_output_set(gpio_mask mask) {
#if GPIO_VERIFY
    gpio_assert_valid_pindir(mask, GPIO_PINDIR_OUTPUT);
#endif
    mc68681_copbc_set(mask >> GPIO_NUM_OUTPUTS_START);
}

static inline void gpio_output_clear(gpio_mask mask) {
#if GPIO_VERIFY
    gpio_assert_valid_pindir(mask, GPIO_PINDIR_OUTPUT);
#endif
    mc68681_sopbc_set(mask >> GPIO_NUM_OUTPUTS_START);
}
