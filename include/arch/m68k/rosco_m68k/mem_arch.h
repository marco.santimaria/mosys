#pragma once

#include <stdint.h>

#define MEM_PAGE_SIZE 0x1000
#define MEM_LEN 0x1000000

extern char _ram_start[], _ram_end[];
extern char _expansion_start[], _expansion_end[];
extern char _rom_start[], _rom_end[];
extern volatile char _io_start[], _io_end[];
#define mem_ram_start       _ram_start
#define mem_ram_end         _ram_end
#define mem_expansion_start _expansion_start
#define mem_expansion_end   _expansion_end
#define mem_rom_start       _rom_start
#define mem_rom_end         _rom_end
#define mem_io_start        _io_start
#define mem_io_end          _io_end

typedef uint64_t mem_bitmap_entry;
#define MEM_BITMAP_C(x) UINT64_C(x)

// The mem_bitmap_mtx must already be held on call
void mem_arch_init(void);
