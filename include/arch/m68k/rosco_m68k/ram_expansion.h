#pragma once

#include "mem/mem.h"
#include <stddef.h>
#include <stdint.h>

#define RAM_EXPANSION_BANK_SIZE 0x100000
#define RAM_EXPANSION_BANKS ((mem_expansion_end - mem_expansion_start) / RAM_EXPANSION_BANK_SIZE)
#define RAM_EXPANSION_BANKS_MAX 16

typedef unsigned short ram_expansion_bank;
typedef unsigned short ram_expansion_bank_count;
typedef unsigned short ram_expansion_block;
typedef unsigned short ram_expansion_chip_count;

typedef enum ram_expansion_chips {
    RAM_EXPANSION_CHIPS_NONE = 0,
    RAM_EXPANSION_CHIPS_ODD  = 1,
    RAM_EXPANSION_CHIPS_EVEN = 2,
    RAM_EXPANSION_CHIPS_BOTH = RAM_EXPANSION_CHIPS_ODD | RAM_EXPANSION_CHIPS_EVEN,
    RAM_EXPANSION_CHIPS_INVALID,    // Used for out-of-range bank results, no real bank can have this value
} ram_expansion_chips;

extern ram_expansion_chips ram_expansion_populated[RAM_EXPANSION_BANKS_MAX];

// This is a destructive scan, which may overwrite contents of expansion RAM
ram_expansion_chip_count ram_expansion_scan(void);

ram_expansion_bank_count ram_expansion_block_length_at_bank(ram_expansion_bank bank);
ram_expansion_bank ram_expansion_bank_from_block(ram_expansion_block block);
ram_expansion_chips ram_expansion_get_block(unsigned short block, void **base, size_t *size);
