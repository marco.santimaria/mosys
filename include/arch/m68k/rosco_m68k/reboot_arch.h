#pragma once

#include <stdnoreturn.h>

noreturn void reboot_arch_restart(const char *arg);
noreturn void reboot_arch_halt(void);
noreturn void reboot_arch_power_off(void);
