#pragma once

#include "mem/flex.h"
#include "module.h"
#include "uapi/mosys/fb.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

struct fb_ops;

typedef struct fb_info {
    struct fb_ops *ops;

    struct fb_fix_screeninfo fix;
    struct fb_var_screeninfo var;
} fb_info;

typedef struct fb_ops {
    int (*open)(struct fb_info *info, int *errno_ptr);
    int (*close)(struct fb_info *info, int *errno_ptr);

    ssize_t (*read)(struct fb_info *info, flex_mut buf, size_t count, unsigned long long ppos, int *errno_ptr);
    ssize_t (*write)(struct fb_info *info, flex_const buf, size_t count, unsigned long long ppos, int *errno_ptr);

    int (*setcolreg)(struct fb_info *info, unsigned regno, unsigned red, unsigned green, unsigned blue, unsigned transp, int *errno_ptr);
    int (*setcmap)(struct fb_info *info, struct fb_cmap *cmap, int *errno_ptr);

    int (*blank)(struct fb_info *info, int blank, int *errno_ptr);

    int (*pan_display)(struct fb_var_screeninfo *var, struct fb_info *info, int *errno_ptr);

    void (*fillrect)(struct fb_info *info, const struct fb_fillrect *rect, int *errno_ptr);
	void (*copyarea)(struct fb_info *info, const struct fb_copyarea *region, int *errno_ptr);
	void (*imageblit)(struct fb_info *info, const struct fb_image *image, int *errno_ptr);

    int (*ioctl)(struct fb_info *info, unsigned long request, flex_mut argp, int *errno_ptr);
} fb_ops;

fb_info *fb_new(void);
void fb_delete(fb_info *fb);
void fb_register(fb_info *fb);
