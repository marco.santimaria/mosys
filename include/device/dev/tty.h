#pragma once

#include "config.h"
#include "mem/flex.h"
#include "mem/mem.h"
#include "uapi/limits.h"
#include "uapi/termios.h"
#include "util/circbuf.h"
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

struct tty_ops;

// Leave space for NL or EOL
// A circbuf can hold one less than the size of its buffer
#define IN_BUF_SIZE (MAX_CANON + 1 + 1)

typedef struct tty_info {
    struct tty_ops *ops;

    // Reserved for use by underlying driver
    void *data_ptr;
    mem_addr data_mem;
    int data_int;

    struct termios termios_p;

    circbuf_char in_crcbf;
    char in_buf[IN_BUF_SIZE];

    bool line_complete;
} tty_info;

static_assert(IN_BUF_SIZE > MAX_CANON && IN_BUF_SIZE > MAX_INPUT, "Size of in_buf must be maximum of MAX_CANON and MAX_INPUT");

typedef struct tty_ops {
    int (*open)(struct tty_info *info, int *errno_ptr);
    int (*close)(struct tty_info *info, int *errno_ptr);

    ssize_t (*read)(struct tty_info *info, flex_mut buf, size_t count, unsigned long long ppos, int *errno_ptr);
    ssize_t (*write)(struct tty_info *info, flex_const buf, size_t count, unsigned long long ppos, int *errno_ptr);

    int (*ioctl)(struct tty_info *info, unsigned long request, flex_mut argp, int *errno_ptr);
} tty_ops;

tty_info *tty_new(void);
void tty_delete(tty_info *tty);
void tty_register(tty_info *tty, const char *name_format);
