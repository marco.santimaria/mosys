#pragma once

#include "config.h"
#include "mem/flex.h"
#include "uapi/errno.h"
#include "uapi/sys/types.h"
#include "util/rwlock.h"
#include <stddef.h>
#include <stdint.h>

typedef struct device_operations {
    // TODO: Add open() and close() equivalents
    ssize_t (*read)(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
    ssize_t (*write)(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
    int (*ioctl)(int minor, unsigned long request, flex_mut argp, int *errno_ptr);
} device_operations;

typedef struct device {
    device_operations *dops;
} device;

int device_register(int major, device_operations *dops);
void device_unregister(int major);

ssize_t device_read(int major, int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
ssize_t device_write(int major, int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
int device_ioctl(int major, int minor, unsigned long request, flex_mut argp, int *errno_ptr);
