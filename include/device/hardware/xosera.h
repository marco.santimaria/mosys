#pragma once

#include "config.h"
#include <stdbool.h>
#include <stdint.h>

#if DEV_XOSERA_ENABLE

typedef enum xosera_xm {
    XOSERA_XM_XR_ADDR       = 0x00,
    XOSERA_XM_XR_DATA       = 0x01,
    XOSERA_XM_RD_INCR       = 0x02,
    XOSERA_XM_RD_ADDR       = 0x03,
    XOSERA_XM_WR_INCR       = 0x04,
    XOSERA_XM_WR_ADDR       = 0x05,
    XOSERA_XM_DATA          = 0x06,
    XOSERA_XM_DATA_2        = 0x07,
    XOSERA_XM_SYS_CTRL      = 0x08,
    XOSERA_XM_TIMER         = 0x09,
    XOSERA_XM_LFSR          = 0x0A,
    XOSERA_XM_RW_INCR       = 0x0C,
    XOSERA_XM_RW_ADDR       = 0x0D,
    XOSERA_XM_RW_DATA       = 0x0E,
    XOSERA_XM_RW_DATA_2     = 0x0F,

    XOSERA_XM_MAX           = 0x0F,
} xosera_xm;

typedef enum xosera_xr {
    // Video Config and Copper XR Registers
    XOSERA_XR_VID_CTRL      = 0x00,
    XOSERA_XR_COPP_CTRL     = 0x01,
    XOSERA_XR_VID_LEFT      = 0x06,
    XOSERA_XR_VID_RIGHT     = 0x07,
    XOSERA_XR_SCANLINE      = 0x08,
    XOSERA_XR_VERSION       = 0x0A,
    XOSERA_XR_GITHASH_H     = 0x0B,
    XOSERA_XR_GITHASH_L     = 0x0C,
    XOSERA_XR_VID_HSIZE     = 0x0D,
    XOSERA_XR_VID_VSIZE     = 0x0E,
    XOSERA_XR_VID_VFREQ     = 0x0F,

    // Playfield A & B Control XR Registers
    XOSERA_XR_PA_GFX_CTRL   = 0x10,
    XOSERA_XR_PA_TILE_CTRL  = 0x11,
    XOSERA_XR_PA_DISP_ADDR  = 0x12,
    XOSERA_XR_PA_LINE_LEN   = 0x13,
    XOSERA_XR_PA_HV_SCROLL  = 0x14,
    XOSERA_XR_PA_LINE_ADDR  = 0x15,
    XOSERA_XR_PA_HV_FSCALE  = 0x16,
    XOSERA_XR_PB_GFX_CTRL   = 0x18,
    XOSERA_XR_PB_TILE_CTRL  = 0x19,
    XOSERA_XR_PB_DISP_ADDR  = 0x1A,
    XOSERA_XR_PB_LINE_LEN   = 0x1B,
    XOSERA_XR_PB_HV_SCROLL  = 0x1C,
    XOSERA_XR_PB_LINE_ADDR  = 0x1D,
    XOSERA_XR_PB_HV_FSCALE  = 0x1E,

    // 2D Blitter Engine XR Registers
    XOSERA_XR_BLIT_CTRL     = 0x20,
    XOSERA_XR_BLIT_MOD_A    = 0x21,
    XOSERA_XR_BLIT_SRC_A    = 0x22,
    XOSERA_XR_BLIT_MOD_B    = 0x23,
    XOSERA_XR_BLIT_SRC_B    = 0x24,
    XOSERA_XR_BLIT_MOD_C    = 0x25,
    XOSERA_XR_BLIT_VAL_C    = 0x26,
    XOSERA_XR_BLIT_MOD_D    = 0x27,
    XOSERA_XR_BLIT_DST_D    = 0x28,
    XOSERA_XR_BLIT_SHIFT    = 0x29,
    XOSERA_XR_BLIT_LINES    = 0x2A,
    XOSERA_XR_BLIT_WORDS    = 0x2B,

    // Extended Memory Regions
    XOSERA_XR_COLOUR_ADDR   = 0x8000,
    XOSERA_XR_COLOUR_A_ADDR = XOSERA_XR_COLOUR_ADDR + 0x0000,
    XOSERA_XR_COLOUR_B_ADDR = XOSERA_XR_COLOUR_ADDR + 0x0100,
    XOSERA_XR_TILE_ADDR     = 0xA000,
    XOSERA_XR_COPPER_ADDR   = 0xC000,

    XOSERA_XR_MAX           = 0xFFFF,
} xosera_xr;

static const uint16_t XOSERA_XR_COLOUR_SIZE    = 0x0200;
static const uint16_t XOSERA_XR_COLOUR_A_SIZE  = 0x0100;
static const uint16_t XOSERA_XR_COLOUR_B_SIZE  = 0x0100;
static const uint16_t XOSERA_XR_TILE_SIZE      = 0x1400;
static const uint16_t XOSERA_XR_COPPER_SIZE    = 0x0800;

bool xosera_sync(volatile void *base);
bool xosera_init(volatile void *base, int reconfig_num);

static inline uint16_t xosera_xm_get_w(volatile void *base, xosera_xm xm);
static inline void xosera_xm_set_w(volatile void *base, xosera_xm xm, uint16_t value);
static inline uint32_t xosera_xm_get_l(volatile void *base, xosera_xm xm);
static inline void xosera_xm_set_l(volatile void *base, xosera_xm xm, uint32_t value);

static inline uint16_t xosera_xr_get(volatile void *base, xosera_xr xr) {
    xosera_xm_set_w(base, XOSERA_XM_XR_ADDR, xr);
    return xosera_xm_get_w(base, XOSERA_XM_XR_DATA);
}

static inline void xosera_xr_set(volatile void *base, xosera_xr xr, uint16_t data) {
    xosera_xm_set_w(base, XOSERA_XM_XR_ADDR, xr);
    xosera_xm_set_w(base, XOSERA_XM_XR_DATA, data);
}

#endif

#include "device/hardware/xosera_arch.h"
