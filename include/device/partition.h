#pragma once

#include <stdbool.h>
#include <stdint.h>

// TODO: The sector size shouldn't be hard-coded
#define PARTITION_SECTOR_SIZE 512U
#define PARTITION_ENTRIES_COUNT 4

typedef enum partition_type {
    PARITION_TYPE_MAX   = 0xFF,
} partition_type;

typedef struct partition_entry {
    uint8_t status;
    partition_type type;
    uint32_t first_sector;
    uint32_t count_sectors;
} partition_entry;

typedef struct partition_disk_info {
    partition_entry entries[PARTITION_ENTRIES_COUNT];
} partition_disk_info;

bool partition_scan_device(int major, int minor, partition_disk_info *disk_info, int *errno_ptr);
