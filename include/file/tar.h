#pragma once

#include "uapi/sys/types.h"
#include <stdbool.h>
#include <stddef.h>

struct tar_header {
    char name[100];
    char mode[8];
    char uid[8];
    char gid[8];
    char size[12];
    char mtime[12];
    char chksum[8];
    char typeflag;
    char linkname[100];
} __attribute__((packed));

struct tar_entry {
    struct tar_header header;
    char padding[512 - sizeof(struct tar_header)];
    char contents[][512];
} __attribute__((packed));

#define TAR_REGTYPE  '0'    // Regular File
#define TAR_AREGTYPE '\0'   // Regular File
#define TAR_LNKTYPE  '1'    // Hard Link
#define TAR_SYMTYPE  '2'    // Symbolic Link
#define TAR_CHRTYPE  '3'    // Character Special
#define TAR_BLKTYPE  '4'    // Block Special
#define TAR_DIRTYPE  '5'    // Directory
#define TAR_FIFOTYPE '6'    // FIFO Special
#define TAR_CONTTYPE '7'    // reserved

unsigned long tar_oct_atoi(const char *str, ssize_t len);
bool tar_header_is_empty(struct tar_entry *ptr);
bool tar_header_is_end(struct tar_entry *ptr);
struct tar_entry *tar_get_next_entry_nocheck(struct tar_entry *ptr);
struct tar_entry *tar_get_next_entry(struct tar_entry *ptr);
struct tar_entry *tar_get_nth_entry(struct tar_entry *ptr, size_t n);
struct tar_entry *tar_lookup_name(struct tar_entry *ptr, const char *name, size_t *result_index_ptr);
