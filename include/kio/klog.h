#pragma once

#include "info.h"
#include "util/util.h"
#include <stdnoreturn.h>
#include <stdbool.h>

#undef printf

#define KLOG_MAX_MESSAGE_LEN 256

enum klog_level {
    KLOG_LEVEL_INFO,
    KLOG_LEVEL_WARN,
    KLOG_LEVEL_PANIC,
    KLOG_LEVEL_DEBUG,
};

void klog_func(enum klog_level level, bool print_file, const char *file, unsigned line, const char *reason, ...)
    __attribute__ ((format(printf, 5, 6)));
void klog_print_stack_trace(void);
noreturn void kpanic_blocker(void);
static void kdebug_dummy(int dummy, ...) { UNUSED(dummy); }

#define klog_loc_X(level, print_file, file, line, ...) klog_func(level, print_file, file, line, __VA_ARGS__)
#define klog_loc(level, print_file, file, line, ...) klog_loc_X(level, print_file, file, line, __VA_ARGS__)
#define kinfo_loc(file, line, ...) klog_loc(KLOG_LEVEL_INFO, false, file, line, __VA_ARGS__)
#define kwarn_loc(file, line, ...) klog_loc(KLOG_LEVEL_WARN, true, file, line, __VA_ARGS__)
#define kpanic_expr_loc(file, line, ...) (klog_loc(KLOG_LEVEL_PANIC, true, file, line, __VA_ARGS__), kpanic_blocker())
#define kpanic_loc(file, line, ...) do { kpanic_expr_loc(file, line, __VA_ARGS__); } while (0)
#if INFO_VERSION_IS_CLEAN
#define kdebug_loc(file, line, ...) _Static_assert(0, "Clean release versions don't support kdebug")
#elif INFO_VERSION_IS_RELEASE
#define kdebug_loc(file, line, ...) do { kdebug_dummy(0, __VA_ARGS__); } while(0)
#else
#define kdebug_loc(file, line, ...) klog_loc(KLOG_LEVEL_DEBUG, true, file, line, __VA_ARGS__)
#endif
#define ktrace_loc(file, line) kdebug_loc(file, line, "Trace")

#define klog(level, print_file, ...) klog_loc(level, print_file, INFO_FILE, INFO_LINE, __VA_ARGS__)
#define kinfo(...) kinfo_loc(INFO_FILE, INFO_LINE, __VA_ARGS__)
#define kwarn(...) kwarn_loc(INFO_FILE, INFO_LINE, __VA_ARGS__)
#define kpanic(...) kpanic_loc(INFO_FILE, INFO_LINE, __VA_ARGS__)
#define kpanic_expr(...) kpanic_expr_loc(INFO_FILE, INFO_LINE, __VA_ARGS__)
#define kdebug(...) kdebug_loc(INFO_FILE, INFO_LINE, __VA_ARGS__)
#define ktrace() ktrace_loc(INFO_FILE, INFO_LINE)

#define kassert(cond)                                   \
    do {                                                \
        if (__builtin_expect (!(cond), 0)) {            \
            kpanic("Assertion \"%s\" failed", #cond);   \
        }                                               \
    } while (0)
