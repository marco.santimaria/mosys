#pragma once

#include "info.h"
#include "util/util.h"
#include <stdnoreturn.h>

#ifdef NDEBUG
#define assert(condition) NOTHING
#else
noreturn void assert_fail(const char *condition, const char *file, unsigned int line, const char *function);
#define assert(condition) (((condition) ? NOTHING : assert_fail(#condition, INFO_FILE, INFO_LINE, INFO_FUNC)))
#endif

#define static_assert _Static_assert
