#pragma once

#include "mem/umem.h"
#include "kio/klog.h"
#include "util/util.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

// TODO: Make sure results aren't discarded

#define FLEX_INF (SIZE_MAX)

typedef enum flex_type {
    FLEX_TYPE_NULL = 0,
    FLEX_TYPE_KERNEL,
    FLEX_TYPE_USER,
} flex_type;

typedef union flex_ptr_mut {
    void *kernel;
    umem_ptr user;
} flex_ptr_mut;
_Static_assert(sizeof(flex_ptr_mut) == sizeof(void *), "Bad flex_ptr_mut size");
_Static_assert(sizeof(flex_ptr_mut) == sizeof(umem_ptr), "Bad flex_ptr_mut size");

typedef union flex_ptr_const {
    const void *kernel;
    umem_ptr_const user;
} flex_ptr_const;
_Static_assert(sizeof(flex_ptr_const) == sizeof(const void *), "Bad flex_ptr_const size");
_Static_assert(sizeof(flex_ptr_const) == sizeof(umem_ptr_const), "Bad flex_ptr_const size");

#define FLEX_PTR_MUT_NULL ((flex_ptr_mut) { 0 })
#define FLEX_PTR_CONST_NULL ((flex_ptr_const) { 0 })

static inline bool flex_ptr_mut_is_null(flex_ptr_mut flx_ptr) {
    // TODO: This is slightly sloppy
    return !flx_ptr.kernel;
}
static inline bool flex_ptr_const_is_null(flex_ptr_const flx_ptr) {
    // TODO: This is slightly sloppy
    return !flx_ptr.kernel;
}
#define flex_ptr_is_null(flx_ptr) _Generic((flx_ptr),   \
    flex_ptr_mut: flex_ptr_mut_is_null,                 \
    flex_ptr_const: flex_ptr_const_is_null              \
)(flx_ptr)

typedef struct flex_mut {
    flex_type type;
    flex_ptr_mut ptr;
    size_t size;
} flex_mut;

typedef struct flex_const {
    flex_type type;
    flex_ptr_const ptr;
    size_t size;
} flex_const;

#define FLEX_MUT_NULL (flex_mut_null())
#define FLEX_CONST_NULL (flex_const_null())

// Do nothing but provide a hint about the pointed-to type
#define FLEX_MUT(type) flex_mut
#define FLEX_CONST(type) flex_const
#define FLEX_PTR_MUT(type) flex_ptr_mut
#define FLEX_PTR_CONST(type) flex_ptr_const

typedef FLEX_MUT(char) flex_mut_str;
typedef FLEX_CONST(char) flex_const_str;

typedef flex_mut flex_mut_vect;
typedef flex_const flex_const_vect;

typedef flex_mut_vect flex_mut_vect_mut;
typedef flex_mut_vect flex_mut_vect_const;
typedef flex_const_vect flex_const_vect_mut;
typedef flex_const_vect flex_const_vect_const;

typedef enum flex_result {
    FLEX_RESULT_SUCCESS     = 0,

    FLEX_RESULT_DEST_FAULT  = 1 << 0,
    FLEX_RESULT_DEST_SMALL  = 1 << 1,

    FLEX_RESULT_SRC_FAULT   = 1 << 4,
    FLEX_RESULT_SRC_SMALL   = 1 << 5,

    FLEX_RESULT_MAX         = 0xF,
} flex_result;

static inline bool flex_mut_is_null(flex_mut flx) {
    return flx.type == FLEX_TYPE_NULL;
}
static inline bool flex_const_is_null(flex_const flx) {
    return flx.type == FLEX_TYPE_NULL;
}
#define flex_is_null(flx) _Generic((flx),   \
    flex_mut: flex_mut_is_null,             \
    flex_const: flex_const_is_null          \
)(flx)

#define flex_assert_null(flx) kassert(flex_is_null(flx))
#define flex_assert_nonnull(flx) kassert(!flex_is_null(flx))

static inline void flex_mut_init_null(flex_mut *flx) {
    flx->type = FLEX_TYPE_NULL;
    memset(&flx->ptr, 0, sizeof(flx->ptr));
    flx->size = 0;
}

static inline void flex_mut_init_kernel(flex_mut *flx, void *buf, size_t size) {
    if (buf) {
        flx->type = FLEX_TYPE_KERNEL;
        flx->ptr.kernel = buf;
        flx->size = size;
    } else {
        flex_mut_init_null(flx);
    }
}

static inline void flex_mut_init_user(flex_mut *flx, umem_ptr buf, size_t size) {
    if (buf) {
        flx->type = FLEX_TYPE_USER;
        flx->ptr.user = buf;
        flx->size = size;
    } else {
        flex_mut_init_null(flx);
    }
}

static inline void flex_const_init_null(flex_const *flx) {
    flx->type = FLEX_TYPE_NULL;
    memset(&flx->ptr, 0, sizeof(flx->ptr));
    flx->size = 0;
}

static inline void flex_const_init_kernel(flex_const *flx, const void *buf, size_t size) {
    if (buf) {
        flx->type = FLEX_TYPE_KERNEL;
        flx->ptr.kernel = buf;
        flx->size = size;
    } else {
        flex_const_init_null(flx);
    }
}

static inline void flex_const_init_user(flex_const *flx, umem_ptr_const buf, size_t size) {
    if (buf) {
        flx->type = FLEX_TYPE_USER;
        flx->ptr.user = buf;
        flx->size = size;
    } else {
        flex_const_init_null(flx);
    }
}

static inline flex_mut flex_mut_null() {
    flex_mut flx;
    flex_mut_init_null(&flx);
    return flx;
}

static inline flex_mut flex_mut_kernel(void *buf, size_t size) {
    flex_mut flx;
    flex_mut_init_kernel(&flx, buf, size);
    return flx;
}

static inline flex_mut flex_mut_user(umem_ptr buf, size_t size) {
    flex_mut flx;
    flex_mut_init_user(&flx, buf, size);
    return flx;
}

static inline flex_mut flex_mut_generic(flex_type type, flex_ptr_mut flx_ptr, size_t size) {
    switch (type) {
    case FLEX_TYPE_NULL: return flex_mut_null();
    case FLEX_TYPE_KERNEL: return flex_mut_kernel(flx_ptr.kernel, size);
    case FLEX_TYPE_USER: return flex_mut_user(flx_ptr.user, size);
    default: kpanic("Unexpected flex type %d", (int) type);
    }
}

static inline flex_const flex_const_null() {
    flex_const flx;
    flex_const_init_null(&flx);
    return flx;
}

static inline flex_const flex_const_kernel(const void *buf, size_t size) {
    flex_const flx;
    flex_const_init_kernel(&flx, buf, size);
    return flx;
}

static inline flex_const flex_const_user(umem_ptr_const buf, size_t size) {
    flex_const flx;
    flex_const_init_user(&flx, buf, size);
    return flx;
}

static inline flex_const flex_const_generic(flex_type type, flex_ptr_const flx_ptr, size_t size) {
    switch (type) {
    case FLEX_TYPE_NULL: return flex_const_null();
    case FLEX_TYPE_KERNEL: return flex_const_kernel(flx_ptr.kernel, size);
    case FLEX_TYPE_USER: return flex_const_user(flx_ptr.user, size);
    default: kpanic("Unexpected flex type %d", (int) type);
    }
}

static inline flex_mut_str flex_mut_str_null() {
    return flex_mut_null();
}

static inline flex_mut_str flex_mut_str_kernel(char *buf) {
    return flex_mut_kernel(buf, FLEX_INF);
}

static inline flex_mut_str flex_mut_str_user(UMEM_PTR(char) buf) {
    return flex_mut_user(buf, FLEX_INF);
}

static inline flex_mut_str flex_mut_str_generic(flex_type type, flex_ptr_mut flx_ptr) {
    switch (type) {
    case FLEX_TYPE_NULL: return flex_mut_str_null();
    case FLEX_TYPE_KERNEL: return flex_mut_str_kernel(flx_ptr.kernel);
    case FLEX_TYPE_USER: return flex_mut_str_user(flx_ptr.user);
    default: kpanic("Unexpected flex type %d", (int) type);
    }
}

static inline flex_const_str flex_const_str_null() {
    return flex_const_null();
}

static inline flex_const_str flex_const_str_kernel(const char *buf) {
    return flex_const_kernel(buf, FLEX_INF);
}

static inline flex_const_str flex_const_str_user(UMEM_PTR(const char) buf) {
    return flex_const_user(buf, FLEX_INF);
}

static inline flex_const_str flex_const_str_generic(flex_type type, flex_ptr_const flx_ptr) {
    switch (type) {
    case FLEX_TYPE_NULL: return flex_const_str_null();
    case FLEX_TYPE_KERNEL: return flex_const_str_kernel(flx_ptr.kernel);
    case FLEX_TYPE_USER: return flex_const_str_user(flx_ptr.user);
    default: kpanic("Unexpected flex type %d", (int) type);
    }
}

static inline flex_const flex_make_const(flex_mut flx) {
    flex_const result = {
        .type = flx.type,
        .size = flx.size,
    };
    memcpy(&result.ptr, &flx.ptr, sizeof(flx.ptr));
    return result;
}

struct flex_mut_pinned_no_check {
    bool fault;
    void *ptr;
    size_t size;
};

struct flex_const_pinned_no_check {
    bool fault;
    const void *ptr;
    size_t size;
};

#if defined __has_c_attribute
#   if __has_c_attribute(gnu::cleanup)
#       define flex_mut_pinned [[gnu::cleanup(flex_mut_cleanup_assert_null)]] struct flex_mut_pinned_no_check
#       define flex_const_pinned [[gnu::cleanup(flex_const_cleanup_assert_null)]] struct flex_const_pinned_no_check
#   else
typedef struct flex_mut_pinned_no_check flex_mut_pinned;
typedef struct flex_const_pinned_no_check flex_const_pinned;
#   endif
#else
#   warning "No __has_c_attribute"
typedef struct flex_mut_pinned_no_check flex_mut_pinned;
typedef struct flex_const_pinned_no_check flex_const_pinned;
#endif

__attribute__ ((warn_unused_result)) static inline struct flex_mut_pinned_no_check flex_mut_pin_must_cleanup(flex_mut flx) {
    if (flx.size == FLEX_INF) {
        kpanic("Can't pin unlimited-size flex_mut");
    }

    if (flx.type == FLEX_TYPE_NULL) {
        return (struct flex_mut_pinned_no_check) {
            .fault = false,
            .ptr = NULL,
            .size = 0,
        };
    } else {
        // TODO: Pin in MMU
        return (struct flex_mut_pinned_no_check) {
            // TODO: This isn't the right way
            .fault = false,
            .ptr = flx.ptr.kernel,
            .size = flx.size,
        };
    }
}

__attribute__ ((warn_unused_result)) static inline struct flex_const_pinned_no_check flex_const_pin_must_cleanup(flex_const flx) {
    if (flx.size == FLEX_INF) {
        kpanic("Can't pin unlimited-size flex_mut");
    }

    if (flx.type == FLEX_TYPE_NULL) {
        return (struct flex_const_pinned_no_check) {
            .fault = false,
            .ptr = NULL,
            .size = 0,
        };
    } else {
        // TODO: Pin in MMU
        return (struct flex_const_pinned_no_check) {
            // TODO: This isn't the right way
            .fault = false,
            .ptr = flx.ptr.kernel,
            .size = flx.size,
        };
    }
}

#define flex_pin_must_cleanup(flx) _Generic((flx),  \
    flex_mut: flex_mut_pin_must_cleanup,            \
    flex_const: flex_const_pin_must_cleanup         \
)(flx)

static inline void flex_mut_unpin(struct flex_mut_pinned_no_check *flx_pin) {
    // TODO: Unpin in MMU
    memset(flx_pin, 0, sizeof(*flx_pin));
}

static inline void flex_const_unpin(struct flex_const_pinned_no_check *flx_pin) {
    // TODO: Unpin in MMU
    memset(flx_pin, 0, sizeof(*flx_pin));
}

#define flex_unpin(flx_pin) _Generic((flx_pin),             \
    struct flex_mut_pinned_no_check *: flex_mut_unpin,      \
    struct flex_const_pinned_no_check *: flex_const_unpin   \
)(flx_pin)

static inline void flex_mut_cleanup_assert_null(struct flex_mut_pinned_no_check *flx_pin) {
    kassert(!flx_pin->ptr);
    kassert(!flx_pin->size);
}

static inline void flex_const_cleanup_assert_null(struct flex_const_pinned_no_check *flx_pin) {
    kassert(!flx_pin->ptr);
    kassert(!flx_pin->size);
}

flex_mut flex_mut_str_measure(flex_mut_str flx_str);
flex_const flex_const_str_measure(flex_const_str flx_str);
#define flex_str_measure(flx_str) _Generic((flx_str),   \
    flex_mut_str: flex_mut_str_measure,                 \
    flex_const_str: flex_const_str_measure              \
)(flx_str)

flex_mut flex_mut_vect_measure(flex_mut_vect flx_vect);
flex_const flex_const_vect_measure(flex_const_vect flx_vect);
#define flex_vect_measure(flx_vect) _Generic((flx_vect),    \
    flex_mut_vect: flex_mut_vect_measure,                   \
    flex_const_vect: flex_const_vect_measure                \
)(flx_vect)

flex_result flex_set(flex_mut dest, int ch, size_t count);

flex_result flex_from_mut(void *dest, flex_mut src, size_t count);
flex_result flex_from_const(void *dest, flex_const src, size_t count);
#define flex_from(dest, src, count) _Generic((src), \
    flex_mut: flex_from_mut,                        \
    flex_const: flex_from_const                     \
)(dest, src, count)

flex_result flex_to(flex_mut dest, const void *src, size_t count);

flex_result flex_btwn_mut_mut(flex_mut dest, flex_mut src, size_t count);
flex_result flex_btwn_mut_const(flex_mut dest, flex_const src, size_t count);
#define flex_btwn(dest, src, count) _Generic((src), \
    flex_mut: flex_btwn_mut_mut,                    \
    flex_const: flex_btwn_mut_const                 \
)(dest, src, count)

flex_mut flex_add_mut(flex_mut base, size_t addend);
flex_const flex_add_const(flex_const base, size_t addend);
#define flex_add(base, addend) _Generic((base), \
    flex_mut: flex_add_mut,                     \
    flex_const: flex_add_const                  \
)(base, addend)
