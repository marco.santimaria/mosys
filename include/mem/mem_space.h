#pragma once

#include "mem/mem_mapping.h"
#include "util/mutex.h"
#include <stdbool.h>

#define MEM_SPACE_MAPS_INIT_COUNT 4

typedef struct mem_space {
    mutex mtx;

    size_t maps_space;  // Count allocated
    size_t maps_used;   // Count used
    mem_mapping **maps;
} mem_space;

#define MEM_SPACE_INIT ((mem_space) { MUTEX_INIT, 0, 0, NULL })

void mem_space_init(mem_space *mm_spc);
void mem_space_delete_maps(mem_space *mm_spc);              // Clears out all individual maps
void mem_space_release_maps_array(mem_space *mm_spc);       // Frees maps arrays, after deleting maps, if needed
void mem_space_replace(mem_space *dest_mm_spc, mem_space *src_mm_spc);
bool mem_space_expand(mem_space *mm_spc);
// Takes possession of map
// Only pass kheap maps
bool mem_space_append(mem_space *mm_spc, mem_mapping *map);
bool mem_space_clone(mem_space *dest_mm_spc, mem_space *src_mm_spc);

mem_mapping *mem_space_find_map(mem_space *mm_spc, umem_ptr u_ptr);
mem_mapping *mem_space_find_map_nolock(mem_space *mm_spc, umem_ptr u_ptr);

void *mem_space_u_to_k(mem_space *mm_spc, umem_ptr u_ptr);
void *mem_space_u_to_k_nolock(mem_space *mm_spc, umem_ptr u_ptr);

void *mem_space_u_to_k_sized(mem_space *mm_spc, umem_ptr u_ptr, size_t size);
void *mem_space_u_to_k_sized_nolock(mem_space *mm_spc, umem_ptr u_ptr, size_t size);
