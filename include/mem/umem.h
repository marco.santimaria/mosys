#pragma once

#include "mem/ownedmem.h"
#include <stdalign.h>
#include <stddef.h>
#include <stdint.h>

#if 1
typedef uintptr_t umem_ptr;
typedef uintptr_t umem_ptr_const;
#define UMEM_PTR(type) umem_ptr
#define UMEM_CONST_PTR(type) umem_ptr
#define UMEM_ELMNT_INC(type) (sizeof(type))
typedef ptrdiff_t umem_ptrdiff;
#else
#define UMEM_PTR(type) type *
#define UMEM_CONST_PTR(type) type *const
typedef UMEM_PTR(void) umem_ptr;
typedef UMEM_PTR_CONST(void) umem_ptr_const;
#define UMEM_ELMNT_INC(type) (1)
typedef ptrdiff_t umem_ptrdiff;
#endif

#define UMEM_NULL ((umem_ptr) NULL)
#define UMEM_ADD(ptr, val, type) ((ptr) + (size_t) (val) * UMEM_ELMNT_INC(type))

bool umem_object_valid(umem_ptr ptr, size_t size, size_t align);
#define UMEM_OBJECT_VALID(ptr, type) umem_object_valid(ptr, sizeof(type), alignof(type))

[[nodiscard]] bool umem_copy_kk(void *dest, const void *src, size_t count);
[[nodiscard]] bool umem_copy_ku(void *dest, umem_ptr_const src, size_t count);
[[nodiscard]] bool umem_copy_uk(umem_ptr dest, const void *src, size_t count);
[[nodiscard]] bool umem_copy_uu(umem_ptr dest, umem_ptr_const src, size_t count);
