#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum module_status {
    MODULE_UNLOADED = 0,
    MODULE_DEFERRED_LOAD,
    MODULE_LOADED,
    MODULE_SKIPPED_LOAD,
    MODULE_FAILED_LOAD,
} module_status;

typedef module_status (*module_init)(void);
typedef void (*module_deinit)(void);

typedef struct module {
    module_init init;
    module_deinit deinit;
    const char *name;
    const char *const *deps;
    module_status status;
} module;

#define MODULE_DEFINE(module_desc) static module *const module_desc ## _ptr __attribute__((used, section (".module_tab"))) = &module_desc

int module_init_static();
void module_deinit_static();
bool module_is_loaded(const char *name);
bool module_has_deps_loaded(module *module_ptr);
