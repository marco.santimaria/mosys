#pragma once

#include "network/network_types.h"
#include <stdbool.h>
#include <stdint.h>

typedef enum ethertype {
    ETHERTYPE_NONE  = 0,

    ETHERTYPE_IPV4  = 0x0800,
    ETHERTYPE_ARP   = 0x0806,

    ETHERTYPE_MAX   = 0xFFFF,
} ethertype;
