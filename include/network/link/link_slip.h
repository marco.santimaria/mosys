#pragma once

#include "config.h"
#include "network/link/link.h"
#include "uapi/sys/types.h"
#include "util/circbuf.h"

#if NETWORK_LINK_SLIP_ENABLE

#if !NETWORK_LINK_ENABLE
#error NETWORK_LINK_SLIP_ENABLE requires NETWORK_LINK_ENABLE
#endif

#if !NETWORK_INTERNET_IPV4_ENABLE
#error NETWORK_LINK_SLIP_ENABLE requires NETWORK_INTERNET_IPV4_ENABLE
#endif

#define LINK_SLIP_MTU 1006

extern const link_ops link_slip_dtlnk_ops;

typedef struct link_slip_priv {
    circbuf_char cb;
    char buf[2 * LINK_SLIP_MTU + 1];
} link_slip_priv;
#define LINK_SLIP_PRIV_INIT(name) (struct link_slip_priv) { .cb = { .buf = (name).buf, .size = sizeof((name).buf), .head = 0, .tail = 0 }, .buf = { 0 } }

ssize_t link_slip_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
);

char *link_slip_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
);

size_t link_slip_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_slip_decode_frame(
    struct netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
);

bool link_slip_send_encoded_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
);

bool link_slip_poll(
    struct netdev *ntdv,
    int *errno_ptr
);

internet_type link_slip_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
);

#endif
