#pragma once

#include "util/util.h"

typedef uint8_t net_u8;
typedef uint16_t net_u16;
typedef uint32_t net_u32;
typedef uint64_t net_u64;

#define hton_u8(value) to_big_u8(value)
#define hton_u16(value) to_big_u16(value)
#define hton_u32(value) to_big_u32(value)
#define hton_u64(value) to_big_u64(value)

#define ntoh_u8(value) from_big_u8(value)
#define ntoh_u16(value) from_big_u16(value)
#define ntoh_u32(value) from_big_u32(value)
#define ntoh_u64(value) from_big_u64(value)
