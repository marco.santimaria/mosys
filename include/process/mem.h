#pragma once

#include "mem/umem.h"
#include "uapi/sys/types.h"

umem_ptr process_mmap(umem_ptr addr, size_t length, int prot, int flags, int fd, off_t offset, int *errno_ptr);
int process_munmap(umem_ptr addr, size_t length, int *errno_ptr);
