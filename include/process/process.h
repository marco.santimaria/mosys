#pragma once

#include "mem/flex.h"
#include "kalloc.h"
#include "mem/ownedmem.h"
#include "syscall.h"
#include "mem/umem.h"
#include "mem/mem_mapping.h"
#include "mem/mem_space.h"
#include "thread/thread.h"
#include "thread/thread_list.h"
#include "util/list.h"
#include "util/mutex.h"
#include "util/wakelist.h"
#include "vfs/vfs.h"
#include "vfs/fdesc.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdnoreturn.h>

// TODO: Should be in uapi
#define CLONE_VM        0x0001
#define CLONE_VFORK     0x0002
#define CLONE_THREAD    0x0004
#define CLONE_SETTLS    0x0400

typedef int pid_t;

typedef enum process_state {
    PROCESS_STATE_READY = 0,
    PROCESS_STATE_EXIT,
} process_state;

typedef struct process {
    thread_list owned_threads;  // Threads that belong to this process

    wakelist waitpid_wklst;     // Self adds, children wake
    wakelist vfork_wklst;       // Parent adds, self wakes

    pid_t pid;
    mem_space space;
    fdesc_list *fdl;
    mutex cwd_mtx;
    fdesc *cwd;

    process_state state;
    int status;

    list_member process_list_link;
    struct process *parent;
    LIST(struct process) children;
    mutex children_mtx;
    list_member children_link;
} process;

typedef void (*process_entry)();

typedef struct process_execve_info {
    flex_const_str pathname;
    flex_const_vect_mut argv;
    flex_const_vect_mut envp;
    unsigned interp_depth;
    int fd;
    mem_space space;
    int new_argc;
    UMEM_PTR(UMEM_CONST_PTR(char)) new_argv;
    UMEM_PTR(UMEM_CONST_PTR(char)) new_envp;
    // TODO: Replace the below entries with a platform-dependant struct
    umem_ptr entry;
    umem_ptr stack_ptr;
} process_execve_info;
#define PROCESS_EXECVE_INFO_INIT {  \
    .fd = -1,                       \
    .space = MEM_SPACE_INIT,        \
}

void process_init(void);
void process_exec_init(void);

process *process_new(process *parent);
void process_delete(process *this);
void process_adopt_by(process *this, process *new_parent);
void process_give_thread(process *this, thread *thrd);
void process_take_thread(process *this, thread *thrd);

noreturn void process_enter_sup(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr);
noreturn void process_enter_usr(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr);
noreturn void process_enter(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr);
struct exception_state *process_enter_setup(void *ssp, umem_ptr usp, process_entry pc, uint16_t sr);

noreturn void process_enter_state(struct exception_state *ssp);

void process_release_resources(process *this);

process *process_clone(unsigned long flags, umem_ptr stack,
                    //    int *parent_tid, int *child_tid,
                       unsigned long tls,
                       int *errno_ptr);

int process_execve(process_execve_info *exec_info, int *errno_ptr);

noreturn void process_exit_thread(int status);

pid_t process_waitpid(pid_t pid, FLEX_MUT(int) wstatus, int options, int *errno_ptr);

bool process_threads_setup_clone(thread *thrd);

process *process_get_sched(void);
