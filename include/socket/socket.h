#pragma once

#include "config.h"
#include "mem/flex.h"
#include "util/mutex.h"
#include "util/semaphore.h"
#include "uapi/sys/socket.h"
#include "uapi/sys/types.h"
#include "uapi/sys/un.h"
#include <stdbool.h>
#include <stddef.h>

#if SOCKET_ENABLE

struct local_stream_info {
    char *buffer;
    size_t size;
    size_t head;
    size_t tail;
};

typedef struct socket_info {
    mutex mtx;
    size_t ref_count;

    int domain;
    int type;
    int protocol;

    // Contents of listening backlog is not protected by the overall mutex, but the pointer and count are
    struct socket_info **listen_backlog;
    semaphore listen_backlog_sem;
    size_t listen_backlog_count;
    mutex listen_backlog_mtx;

    union {
        struct sockaddr generic;
        struct sockaddr_un local;
    } addr;
    socklen_t addrlen;

    union {
        struct {
            bool is_open;
            struct local_stream_info stream;
        } local;
    };

    struct socket_info *peer;
    union {
        struct sockaddr generic;
        struct sockaddr_un local;
    } peer_addr;
    socklen_t peer_addrlen;
} socket_info;

socket_info *socket_info_new(void);
void socket_info_delete(socket_info *vn);
void socket_info_ref(socket_info *sock);
void socket_info_unref(socket_info *sock);

// Must be called with sock locked
int socket_info_listen_vect_create(socket_info *sock, int backlog, int *errno_ptr);
int socket_info_listen_backlog_put(socket_info *sock, socket_info *connection_peer, int *errno_ptr);
socket_info *socket_info_listen_backlog_get(socket_info *sock, int *errno_ptr);
socket_info *socket_info_listen_backlog_get_blocking(socket_info *sock, int *errno_ptr);

socket_info *socket_socket(int domain, int type, int protocol, int *errno_ptr);
int socket_socketpair(int domain, int type, int protocol, socket_info *socks[2], int *errno_ptr);
int socket_close(socket_info *sock, int *errno_ptr);
ssize_t socket_recvfrom(socket_info *sock, flex_mut buf, size_t len, int flags,
                        FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
ssize_t socket_sendto(socket_info *sock, flex_const buf, size_t len, int flags,
                      FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr);
int socket_bind(socket_info *sock, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);
int socket_accept(socket_info *sock, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
int socket_connect(socket_info *sock, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);

#endif
