#pragma once

#include "exception.h"
#include "kalloc.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void syscall_init(void);

bool syscall_number_is_valid(unsigned long number);
void syscall(struct exception_state *state);
