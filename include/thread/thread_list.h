#pragma once

#include "thread/thread.h"
#include "util/list.h"
#include "util/spinlock.h"

typedef struct thread_list {
    spinlock lock;
    LIST(struct thread) list;
} thread_list;

#define THREAD_LIST_INIT { .lock = SPINLOCK_INIT, .list = LIST_INIT }

extern thread_list thread_ready_list;
