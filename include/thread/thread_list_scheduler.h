#pragma once

#include "thread/thread_list.h"

void thread_list_scheduler_enqueue(thread_list *list, struct thread *enq_thread);
struct thread *thread_list_scheduler_dequeue(thread_list *list);
