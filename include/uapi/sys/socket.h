#pragma once

#define SOCK_RAW 1
#define SOCK_DGRAM 2
#define SOCK_SEQPACKET 4
#define SOCK_STREAM 3

#define AF_INET 1
#define AF_INET6 2
#define AF_UNIX 3
#define AF_LOCAL 3
#define AF_UNSPEC 0

#ifndef SA_FAMILY_T
#define SA_FAMILY_T
typedef unsigned short sa_family_t;
#endif

#ifndef SOCKLEN_T
#define SOCKLEN_T
typedef unsigned long socklen_t;
#endif

struct sockaddr {
    sa_family_t sa_family;
    char        sa_data[14];
};
