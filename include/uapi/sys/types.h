#pragma once

typedef signed long blkcnt_t;
typedef signed long blksize_t;
typedef unsigned long clock_t;
typedef unsigned short clockid_t;
// typedef signed long dev_t;
typedef unsigned long fsblkcnt_t;
typedef unsigned long fsfilcnt_t;
typedef unsigned int gid_t;
typedef unsigned int id_t;
typedef unsigned long ino_t;
// TODO: key_t
typedef unsigned short mode_t;
typedef unsigned int nlink_t;
typedef signed long off_t;
typedef signed int pid_t;
// TODO: pthread_*
typedef unsigned long size_t;
typedef signed long ssize_t;
typedef signed long suseconds_t;
typedef unsigned long time_t;
// TODO: timer_t
// TODO: trace_*
typedef unsigned int uid_t;
typedef unsigned long useconds_t;
