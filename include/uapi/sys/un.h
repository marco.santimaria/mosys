#pragma once

#ifndef SA_FAMILY_T
#define SA_FAMILY_T
typedef unsigned short sa_family_t;
#endif

struct sockaddr_un {
    sa_family_t sun_family;
    char sun_path[108];
};
