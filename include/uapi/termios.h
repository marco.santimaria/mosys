#pragma once

// TODO: Should this exist?
#define TTY_MAGIC 0x5401

typedef unsigned int cc_t;
typedef unsigned int speed_t;
typedef unsigned int tcflag_t;

#define NCCS    11

// Subscripts
#define VEOF    0
#define VEOL    1
#define VERASE  2
#define VINTR   3
#define VKILL   4
#define VMIN    5
#define VQUIT   6
#define VSTART  7
#define VSTOP   8
#define VSUSP   9
#define VTIME   10

// Input Modes
#define BRKINT  (0x1 << 0)
#define ICRNL   (0x1 << 1)
#define IGNBRK  (0x1 << 2)
#define IGNCR   (0x1 << 3)
#define IGNPAR  (0x1 << 4)
#define INLCR   (0x1 << 5)
#define INPCK   (0x1 << 6)
#define ISTRIP  (0x1 << 7)
#define IXANY   (0x1 << 8)
#define IXOFF   (0x1 << 9)
#define IXON    (0x1 << 10)
#define PARMRK  (0x1 << 11)

// Output Modes
#define OPOST   (0x1 << 0)
#define ONLCR   (0x1 << 1)
#define OCRNL   (0x1 << 2)
#define ONOCR   (0x1 << 3)
#define ONLRET  (0x1 << 4)
#define OFDEL   (0x1 << 5)
#define OFILL   (0x1 << 6)
#define NLDLY   (0x1 << 7)
#define   NL0   (0x0 << 7)
#define   NL1   (0x1 << 7)
#define CRDLY   (0x3 << 8)
#define   CR0   (0x0 << 8)
#define   CR1   (0x1 << 8)
#define   CR2   (0x2 << 8)
#define   CR3   (0x3 << 8)
#define TABDLY  (0x3 << 10)
#define   TAB0  (0x0 << 10)
#define   TAB1  (0x1 << 10)
#define   TAB2  (0x2 << 10)
#define   TAB3  (0x3 << 10)
#define BSDLY   (0x1 << 12)
#define   BS0   (0x0 << 12)
#define   BS1   (0x1 << 12)
#define VTDLY   (0x1 << 13)
#define   VT0   (0x0 << 13)
#define   VT1   (0x1 << 13)
#define FFDLY   (0x1 << 14)
#define   FF0   (0x0 << 14)
#define   FF1   (0x1 << 14)

// Baud Rate Selection
#define B0      0
#define B50     50
#define B75     75
#define B110    110
#define B134    134
#define B150    150
#define B200    200
#define B300    300
#define B600    600
#define B1200   1200
#define B1800   1800
#define B2400   2400
#define B4800   4800
#define B9600   9600
#define B19200  19200
#define B38400  38400

// Control Modes
#define CSIZE   (0x3 << 0)
#define   CS5   (0x0 << 0)
#define   CS6   (0x1 << 0)
#define   CS7   (0x2 << 0)
#define   CS8   (0x3 << 0)
#define CSTOPB  (0x1 << 1)
#define CREAD   (0x1 << 2)
#define PARENB  (0x1 << 3)
#define PARODD  (0x1 << 4)
#define HUPCL   (0x1 << 5)
#define CLOCAL  (0x1 << 6)

// Local Modes
#define ECHO    (0x1 << 0)
#define ECHOE   (0x1 << 1)
#define ECHOK   (0x1 << 2)
#define ECHONL  (0x1 << 3)
#define ICANON  (0x1 << 4)
#define IEXTEN  (0x1 << 5)
#define ISIG    (0x1 << 6)
#define NOFLSH  (0x1 << 7)
#define TOSTOP  (0x1 << 8)

// Attribute Selection
#define TCSANOW     0
#define TCSADRAIN   1
#define TCSAFLUSH   2

// Line Control
#define TCIFLUSH    0
#define TCIOFLUSH   1
#define TCOFLUSH    2
#define TCIOFF      0
#define TCION       1
#define TCOOFF      2
#define TCOON       3

struct termios {
    tcflag_t c_iflag;
    tcflag_t c_oflag;
    tcflag_t c_cflag;
    tcflag_t c_lflag;

    cc_t c_cc[NCCS];

    speed_t c_ispeed;
    speed_t c_ospeed;
};

// TODO: Define functions
