#pragma once

typedef unsigned long clock_t;
typedef unsigned long size_t;
typedef unsigned long time_t;

typedef unsigned short clockid_t;

// TODO: locale_t

typedef signed int pid_t;

struct sigevent;

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
};

struct timespec {
    time_t tv_sec;
    long tv_nsec;
};

#ifndef NULL
#define NULL ((void *) 0)
#endif

// CLOCKS_PER_SEC

// TODO: CLOCK_*

// TODO: TIMER_*
