#pragma once

#include <assert.h>
#include <stdint.h>
#include <stdbool.h>

typedef intmax_t ubsan_s_int_max;
typedef uintmax_t ubsan_u_int_max;

typedef long double ubsan_float_max;

typedef struct ubsan_source_location {
	const char* filename;
	uint32_t line;
	uint32_t column;
} ubsan_source_location;

static inline bool ubsan_source_location_is_invalid(const ubsan_source_location *this) { return !this->filename; }
static inline ubsan_source_location ubsan_source_location_acquire(ubsan_source_location *this) {
	// TODO: This should be atomic
	uint32_t old_column = this->column;
	this->column = ~UINT32_C(0);
	return (ubsan_source_location) { this->filename, this->line, old_column };
}
static inline bool ubsan_source_location_is_disabled(const ubsan_source_location *this) { return this->column == ~UINT32_C(0); }

static inline const char *ubsan_source_location_get_filename(const ubsan_source_location *this) { return this->filename; }
static inline unsigned ubsan_source_location_get_line(const ubsan_source_location *this) { return this->line; }
static inline unsigned ubsan_source_location_get_column(const ubsan_source_location *this) { return this->column; }

typedef enum ubsan_type_kind {
	UBSAN_TK_INTEGER 	= 0x0000,
	UBSAN_TK_FLOAT 		= 0x0001,
	UBSAN_TK_UNKNOWN 	= 0xFFFF,
} ubsan_type_kind;

typedef struct ubsan_type_descriptor {
	uint16_t type_kind;	// ubsan_type_kind
	uint16_t type_info;
	char type_name[];
} ubsan_type_descriptor;

static inline const char *ubsan_type_descriptor_get_type_name(const ubsan_type_descriptor *this) { return this->type_name; }

static inline ubsan_type_kind ubsan_type_descriptor_get_kind(const ubsan_type_descriptor *this) { return (ubsan_type_kind) this->type_kind; }

static inline bool ubsan_type_descriptor_is_integer_ty(const ubsan_type_descriptor *this) {
	return ubsan_type_descriptor_get_kind(this) == UBSAN_TK_INTEGER;
}
static inline bool ubsan_type_descriptor_is_signed_integer_ty(const ubsan_type_descriptor *this) {
	return ubsan_type_descriptor_is_integer_ty(this) && (this->type_info & 1);
}
static inline bool ubsan_type_descriptor_is_unsigned_integer_ty(const ubsan_type_descriptor *this) {
	return ubsan_type_descriptor_is_integer_ty(this) && !(this->type_info & 1);
}
static inline unsigned ubsan_type_descriptor_get_integer_bit_width(const ubsan_type_descriptor *this) {
	assert(ubsan_type_descriptor_is_integer_ty(this));
	return 1 << (this->type_info >> 1);
}

static inline bool ubsan_type_descriptor_is_float_ty(const ubsan_type_descriptor *this) {
	return ubsan_type_descriptor_get_kind(this) == UBSAN_TK_FLOAT;
}
static inline unsigned ubsan_type_descriptor_get_float_bit_width(const ubsan_type_descriptor *this) {
	assert(ubsan_type_descriptor_is_float_ty(this));
	return this->type_info;
}

typedef uintptr_t ubsan_value_handle;

const char *ubsan_get_objc_class_name(ubsan_value_handle pointer);

typedef struct ubsan_value {
    const ubsan_type_descriptor *type;
    ubsan_value_handle val;
} ubsan_value;

static inline bool ubsan_value_is_inline_int(const ubsan_value *this) {
	assert(ubsan_type_descriptor_is_integer_ty(this->type));
	const unsigned inline_bits = sizeof(ubsan_value_handle) * 8;
	const unsigned bits = ubsan_type_descriptor_get_integer_bit_width(this->type);
	return bits <= inline_bits;
}

static inline bool ubsan_value_is_inline_float(const ubsan_value *this) {
	assert(ubsan_type_descriptor_is_float_ty(this->type));
	const unsigned inline_bits = sizeof(ubsan_value_handle) * 8;
	const unsigned bits = ubsan_type_descriptor_get_float_bit_width(this->type);
	return bits <= inline_bits;
}

static inline const ubsan_type_descriptor *ubsan_value_get_type(const ubsan_value *this) { return this->type; }

ubsan_s_int_max ubsan_value_get_s_int_value(const ubsan_value *this);

ubsan_u_int_max ubsan_value_get_u_int_value(const ubsan_value *this);

ubsan_u_int_max ubsan_value_get_positive_int_value(const ubsan_value *this);

static inline bool ubsan_value_is_minus_one(const ubsan_value *this) {
	return ubsan_type_descriptor_is_signed_integer_ty(ubsan_value_get_type(this)) && ubsan_value_get_s_int_value(this) == -1;
}

static inline bool ubsan_value_is_negative(const ubsan_value *this) {
	return ubsan_type_descriptor_is_signed_integer_ty(ubsan_value_get_type(this)) && ubsan_value_get_s_int_value(this) < 0;
}

ubsan_float_max ubsan_value_get_float_value(const ubsan_value *this);
