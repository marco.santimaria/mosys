#pragma once

#include <stdint.h>

typedef uint16_t usb_hid_usage_id_button;

const char *usb_hid_usage_id_button_to_str(usb_hid_usage_id_button usage_id);
