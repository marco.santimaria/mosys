#pragma once

#define USB_HID_USAGE_ID_CONSUMER(name) usb_hid_usage_id_consumer_ ## name

typedef enum usb_hid_usage_id_consumer {
    USB_HID_USAGE_ID_CONSUMER(consumer_control)     = 0x01,
    USB_HID_USAGE_ID_CONSUMER(numeric_key_pad)      = 0x02,
    USB_HID_USAGE_ID_CONSUMER(programmable_buttons) = 0x03,
    USB_HID_USAGE_ID_CONSUMER(microphone)           = 0x04,
    USB_HID_USAGE_ID_CONSUMER(headphone)            = 0x05,
    USB_HID_USAGE_ID_CONSUMER(graphic_equalizer)    = 0x06,
    USB_HID_USAGE_ID_CONSUMER(ac_pan)               = 0x238,
    // TODO: Add the rest
} usb_hid_usage_id_consumer;

const char *usb_hid_usage_id_consumer_to_str(usb_hid_usage_id_consumer usage_id);
