#pragma once

#define USB_HID_USAGE_ID_LED(name)  usb_hid_usage_id_led_ ## name

typedef enum usb_hid_usage_id_led {
    USB_HID_USAGE_ID_LED(num_lock)      = 0x01,
    USB_HID_USAGE_ID_LED(caps_lock)     = 0x02,
    USB_HID_USAGE_ID_LED(scroll_lock)   = 0x03,
    USB_HID_USAGE_ID_LED(compose)       = 0x04,
    USB_HID_USAGE_ID_LED(kana)          = 0x05,
    // TODO: Add the rest
} usb_hid_usage_id_led;

const char *usb_hid_usage_id_led_to_str(usb_hid_usage_id_led usage_id);
