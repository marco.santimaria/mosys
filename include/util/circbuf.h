#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define CIRCBUF_HEADER_TEMPLATE(type)                                                               \
typedef struct circbuf_ ## type {                                                                   \
    type *buf;                                                                                      \
    size_t size;                                                                                    \
    size_t head;                                                                                    \
    size_t tail;                                                                                    \
} circbuf_ ## type;                                                                                 \
                                                                                                    \
bool circbuf_ ## type ## _is_empty(const circbuf_ ## type *crcbf);                                  \
bool circbuf_ ## type ## _is_full(const circbuf_ ## type *crcbf);                                   \
bool circbuf_ ## type ## _is_almost_full(const circbuf_ ## type *crcbf);                            \
bool circbuf_ ## type ## _put(circbuf_ ## type *crcbf, type value);                                 \
bool circbuf_ ## type ## _get(circbuf_ ## type *crcbf, type *value_p);                              \
size_t circbuf_ ## type ## _dump(circbuf_ ## type *crcbf, size_t n, type a[n]);                     \
size_t circbuf_ ## type ## _dump_until(circbuf_ ## type *crcbf, size_t n, type a[n], type term);    \
bool circbuf_ ## type ## _contains(circbuf_ ## type *crcbf, type val);                              \
bool circbuf_ ## type ## _remove_last(circbuf_ ## type *crcbf);                                     \
bool circbuf_ ## type ## _remove_all(circbuf_ ## type *crcbf);

CIRCBUF_HEADER_TEMPLATE(char)
CIRCBUF_HEADER_TEMPLATE(uint8_t)

#undef CIRCBUF_HEADER_TEMPLATE
