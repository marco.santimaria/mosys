#pragma once

#include "util/util.h"
#include <stdbool.h>

typedef struct list_member {
    struct list_member *prev;
    struct list_member *next;
} list_member;

typedef struct list {
    list_member *head;
    list_member *tail;
} list;
#define LIST(type) list
#define LIST_INIT { 0 }

static void insert_raw(list *lst, list_member *prev, list_member *inserted, list_member *next) {
    inserted->prev = prev;
    inserted->next = next;

    if (prev) {
        prev->next = inserted;
    } else {
        lst->head = inserted;
    }

    if (next) {
        next->prev = inserted;
    } else {
        lst->tail = inserted;
    }
}

static void remove_raw(list *lst, list_member *prev, list_member *next) {
    if (prev) {
        prev->next = next;
    } else {
        lst->head = next;
    }

    if (next) {
        next->prev = prev;
    } else {
        lst->tail = prev;
    }
}

#define list_containerof(ptr, type, member)             \
    __extension__ ({                                    \
        __auto_type _list_containerof_ptr = (ptr);      \
        _list_containerof_ptr ?                         \
            containerof(_list_containerof_ptr,          \
                type, member) :                         \
            NULL;  \
    })

#define list_memberof(ptr, member)                      \
    __extension__ ({                                    \
        __auto_type _list_memberof_ptr = (ptr);         \
        _list_memberof_ptr ?                            \
            &_list_memberof_ptr->member :               \
            NULL;                                       \
    })

#define list_prev_member(ptr) ((ptr)->prev)
#define list_prev(ptr, member)                      \
    __extension__ ({                                \
        list_member *_prev = list_prev_member(ptr); \
        list_containerof(_prev, type, member);      \
    })

#define list_next_member(ptr) ((ptr)->next)
#define list_next(ptr, member)                      \
    __extension__ ({                                \
        list_member *_next = list_next_member(ptr); \
        list_containerof(_next, type, member);      \
    })

#define list_head_member(lst) ((lst)->head)
#define list_head(lst, type, member)                \
    __extension__ ({                                \
        list_member *_head = list_head_member(lst); \
        list_containerof(_head, type, member);      \
    })

#define list_tail_member(lst) ((lst)->tail)
#define list_tail(lst, type, member)                \
    __extension__ ({                                \
        list_member *_tail = list_tail_member(lst); \
        list_containerof(_tail, type, member);      \
    })

static inline void list_insert_before_member(list *lst, list_member *inserted, list_member *before) {
    insert_raw(lst, before->prev, inserted, before);
}
#define list_insert_before(lst, inserted, before, member)       \
    list_insert_before_member(lst,                              \
        list_memberof(inserted, member),                        \
        list_memberof(before, member))

static inline void list_insert_after_member(list *lst, list_member *inserted, list_member *after) {
    insert_raw(lst, after, inserted, after->next);
}
#define list_insert_after(lst, inserted, after, member)         \
    list_insert_after_member(lst,                               \
        list_memberof(inserted, member),                        \
        list_memberof(after, member))

static inline void list_push_front_member(list *lst, list_member *inserted) {
    insert_raw(lst, NULL, inserted, lst->head);
}
#define list_push_front(lst, inserted, member)  \
    list_push_front_member(lst,                 \
        list_memberof(inserted, member))

static inline void list_push_back_member(list *lst, list_member *inserted) {
    insert_raw(lst, lst->tail, inserted, NULL);
}
#define list_push_back(lst, inserted, member)   \
    list_push_back_member(lst,                  \
        list_memberof(inserted, member))

static inline list_member *list_remove_member(list *lst, list_member *removed) {
    if (removed) {
        remove_raw(lst, removed->prev, removed->next);

        removed->prev = NULL;
        removed->next = NULL;
    }

    return removed;
}
#define list_remove(lst, removed, member)                       \
    list_containerof(                                           \
        list_remove_member(                                     \
            (lst),                                              \
            list_memberof(removed, member)),                    \
        __typeof__ (*(removed)), member)

#define list_pop_front(lst, type, member)                   \
    __extension__ ({                                        \
        __auto_type _list_pop_front_lst = (lst);            \
        __auto_type _head = list_head(                      \
            _list_pop_front_lst, type, member               \
        );                                                  \
        list_remove(_list_pop_front_lst, _head, member);    \
    })

#define list_pop_back(lst, type, member)                    \
    __extension__ ({                                        \
        __auto_type _list_pop_back_lst = (lst);             \
        __auto_type _tail = list_tail(                      \
            _list_pop_back_lst, type, member                \
        );                                                  \
        list_remove(_list_pop_back_lst, _tail, member);     \
    })

#define list_apply(lst, func, type, member, ...)            \
    __extension__ ({                                        \
        list_member *_mmbr_ptr = list_head_member(lst);     \
        bool _continue = true;                              \
        while (_continue && _mmbr_ptr) {                    \
            type *_ptr =                                    \
                list_containerof(_mmbr_ptr, type, member);  \
            _continue = func(_ptr, __VA_ARGS__);            \
            _mmbr_ptr = list_next_member(_mmbr_ptr);        \
        }                                                   \
        _continue;                                          \
    })
