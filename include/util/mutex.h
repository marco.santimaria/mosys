#pragma once

#include "util/wakelist.h"
#include "util/with.h"
#include <stdbool.h>

typedef struct mutex {
    bool is_held;
    wakelist wklst;
} mutex;

#define MUTEX_INIT {            \
    .is_held = false,           \
    .wklst = WAKELIST_INIT,     \
}

void mutex_init(mutex *mtx);
void mutex_deinit(mutex *mtx);

void mutex_lock(mutex *mtx);
bool mutex_trylock(mutex *mtx);
void mutex_unlock(mutex *mtx);

#define MUTEX_WITH(mtx, ...) WITH(_mutex_with_info, mutex_lock(mtx), mutex_unlock(mtx), __VA_ARGS__)
#define MUTEX_TRY_WITH(mtx, ...) TRY_WITH(_mutex_with_info, mutex_trylock(mtx), mutex_unlock(mtx), __VA_ARGS__)
#define MUTEX_LEAVE_UNLOCK() CONTINUE_IN_WITH(_mutex_with_info)
#define MUTEX_LEAVE_NO_UNLOCK() BREAK_IN_WITH(_mutex_with_info)
#define MUTEX_RETURN_NO_UNLOCK() RETURN_IN_WITH(_mutex_with_info)
#define MUTEX_RETURN_VALUE_NO_UNLOCK(value) RETURN_VALUE_IN_WITH(_mutex_with_info, value)
#define MUTEX_GOTO_NO_UNLOCK(label) GOTO_IN_WITH(_mutex_with_info, label)
