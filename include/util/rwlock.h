#pragma once

#include "util/mutex.h"
#include "util/with.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct rwlock {
    mutex r;
    mutex g;
    size_t b;
} rwlock;

#define RWLOCK_INIT { MUTEX_INIT, MUTEX_INIT, 0 }

void rwlock_init(rwlock *rwl);
void rwlock_read_begin(rwlock *rwl);
void rwlock_read_end(rwlock *rwl);
void rwlock_write_begin(rwlock *rwl);
void rwlock_write_end(rwlock *rwl);

#define RWLOCK_WITH(rwl, type, statement) WITH(_rwlock_with_info, rwlock_ ## type ## _begin(rwl), rwlock_ ## type ## _end(rwl), statement)
#define RWLOCK_READ_WITH(rwl, statement) RWLOCK_WITH(rwl, read, statement)
#define RWLOCK_WRITE_WITH(rwl, statement) RWLOCK_WITH(rwl, write, statement)
#define RWLOCK_LEAVE_UNLOCK() CONTINUE_IN_WITH(_rwlock_with_info)
#define RWLOCK_LEAVE_NO_UNLOCK() BREAK_IN_WITH(_rwlock_with_info)
#define RWLOCK_RETURN_NO_UNLOCK() RETURN_IN_WITH(_rwlock_with_info)
#define RWLOCK_RETURN_VALUE_NO_UNLOCK(value) RETURN_VALUE_IN_WITH(_rwlock_with_info, value)
#define RWLOCK_GOTO_NO_UNLOCK(label) GOTO_IN_WITH(_rwlock_with_info, label)
