#pragma once

#include "util/wakelist.h"
#include <stdbool.h>

typedef int semaphore_value;

typedef struct semaphore {
    semaphore_value value;
    wakelist wklst;
} semaphore;

void semaphore_init(semaphore *smphrm, semaphore_value value);
void semaphore_deinit(semaphore *smphr);

void semaphore_wait(semaphore *smphr);
bool semaphore_trywait(semaphore *smphr);
void semaphore_signal(semaphore *smphr);
