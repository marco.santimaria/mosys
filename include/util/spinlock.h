#pragma once

#include "config.h"
#include "kio/klog.h"
#include "util/with.h"
#include <stdatomic.h>
#include <stdbool.h>

typedef struct {
    atomic_flag flag;
} spinlock;

#define SPINLOCK_INIT { ATOMIC_FLAG_INIT }

static inline void spinlock_lock(spinlock *spnlck) {
    while (atomic_flag_test_and_set(&spnlck->flag)) {
#if SYMMETRIC_MULTIPROCESSING
        // Spin
#else
        kpanic("Tried to wait on a spinlock without SMP");
#endif
    }
}

static inline void spinlock_unlock(spinlock *spnlck) {
    atomic_flag_clear(&spnlck->flag);
}

#define SPINLOCK_WITH(spnlck, statement) WITH(_spinlock_with_info, spinlock_lock(spnlck), spinlock_unlock(spnlck), statement)
#define SPINLOCK_LEAVE_UNLOCK() CONTINUE_IN_WITH(_spinlock_with_info)
#define SPINLOCK_LEAVE_NO_UNLOCK() BREAK_IN_WITH(_spinlock_with_info)
#define SPINLOCK_RETURN_NO_UNLOCK() RETURN_IN_WITH(_spinlock_with_info)
#define SPINLOCK_RETURN_VALUE_NO_UNLOCK(value) RETURN_VALUE_IN_WITH(_spinlock_with_info, value)
#define SPINLOCK_GOTO_NO_UNLOCK(label) GOTO_IN_WITH(_spinlock_with_info, label)
