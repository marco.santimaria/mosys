#pragma once

#include "util/util.h"
#include "kio/klog.h"

enum with_exit_reason {
    WITH_EXIT_REASON_NONE,
    WITH_EXIT_REASON_CONTINUE,
    WITH_EXIT_REASON_BREAK,
    WITH_EXIT_REASON_RETURN,
    WITH_EXIT_REASON_GOTO,
};

struct with_info {
    bool flag;
    enum with_exit_reason exit_reason;
    const char *file;
    unsigned line;
};

#if UTIL_WITH_CLEANUP_CHECK
static inline bool util_with_get_expected_flag(struct with_info *info) {
    switch (info->exit_reason) {
    case WITH_EXIT_REASON_NONE:
        kpanic_loc(info->file, info->line, "Invalid return or goto in WITH block");
    case WITH_EXIT_REASON_CONTINUE:
        return false;
    case WITH_EXIT_REASON_BREAK:
    case WITH_EXIT_REASON_RETURN:
    case WITH_EXIT_REASON_GOTO:
        return true;
    default:
        unreachable();
    }
}

static inline void util_with_cleanup_check(struct with_info *info) {
    if (info->flag != util_with_get_expected_flag(info)) {
        kpanic_loc(info->file, info->line, "Unknown error in WITH block");
    }
}

#define UTIL_WITH_INFO_ATTR __attribute__ ((cleanup(util_with_cleanup_check)))
#else
#define UTIL_WITH_INFO_ATTR
#endif

#define TRY_LOOP_WITH(info, enter, exit, ...)                   \
    for (                                                       \
        struct with_info info UTIL_WITH_INFO_ATTR =             \
            {                                                   \
                (enter),                                        \
                WITH_EXIT_REASON_NONE,                          \
                INFO_FILE,                                      \
                INFO_LINE                                       \
            };                                                  \
        info.flag;                                              \
    ) for (                                                     \
        bool _detected_continue;                                \
        info.flag;                                              \
        _detected_continue ?                                    \
            kpanic_expr("Invalid continue in WITH block") :     \
            NOTHING                                             \
    ) {                                                         \
        _detected_continue = true;                              \
        switch (0) {                                            \
        default:                                                \
            info.exit_reason = WITH_EXIT_REASON_NONE;           \
            { __VA_ARGS__ }                                     \
            kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
            info.exit_reason = WITH_EXIT_REASON_CONTINUE;       \
        }                                                       \
        _detected_continue = false;                             \
                                                                \
        switch (info.exit_reason) {                             \
        case WITH_EXIT_REASON_NONE:                             \
            kpanic("Invalid break in WITH block");              \
        case WITH_EXIT_REASON_CONTINUE:                         \
            info.flag = (exit);                                 \
            break;                                              \
        case WITH_EXIT_REASON_BREAK:                            \
            break;                                              \
        case WITH_EXIT_REASON_GOTO:                             \
            kpanic("Invalid GOTO_IN_WITH within WITH block");   \
        case WITH_EXIT_REASON_RETURN:                           \
            unreachable();                                      \
        }                                                       \
    }
#define TRY_WITH(info, enter, exit, ...) TRY_LOOP_WITH(info, (enter), ((exit), false), __VA_ARGS__)
#define WITH(info, enter, exit, ...) TRY_WITH(info, ((enter), true), (exit), __VA_ARGS__)

#define CONTINUE_IN_WITH(info) STATEMENT(                   \
        kassert(info.flag);                                 \
        kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
        info.exit_reason = WITH_EXIT_REASON_CONTINUE;       \
        break;                                              \
    )

#define BREAK_IN_WITH(info) STATEMENT(                      \
        kassert(info.flag);                                 \
        kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
        info.exit_reason = WITH_EXIT_REASON_BREAK;          \
        break;                                              \
    )

#define RETURN_IN_WITH(info) STATEMENT(                     \
        kassert(info.flag);                                 \
        kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
        info.exit_reason = WITH_EXIT_REASON_RETURN;         \
        return;                                             \
    )

#define RETURN_VALUE_IN_WITH(info, value) STATEMENT(        \
        kassert(info.flag);                                 \
        kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
        info.exit_reason = WITH_EXIT_REASON_RETURN;         \
        return (value);                                     \
    )

#define GOTO_IN_WITH(info, label) STATEMENT(                \
        kassert(info.flag);                                 \
        kassert(info.exit_reason == WITH_EXIT_REASON_NONE); \
        info.exit_reason = WITH_EXIT_REASON_GOTO;           \
        goto label;                                         \
    )
