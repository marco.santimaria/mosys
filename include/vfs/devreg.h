#pragma once

#include "util/rwlock.h"

#define DEVREG_NAME_MAX_LENGTH 32

typedef struct devreg {
    rwlock rwl;
    size_t ref_count;

    char name[DEVREG_NAME_MAX_LENGTH + 1];
    int major;
    int minor;

    struct devreg *next;
} devreg;

devreg *devreg_new(const char *name, int major, int minor);
void devreg_delete(devreg *dr);
void devreg_ref(devreg *dr);
void devreg_unref(devreg *dr);
devreg *devreg_lookup(const char *name);
devreg *devreg_get_nth(unsigned long n);
