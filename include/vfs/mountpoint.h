#pragma once

#include "vfs/superblock.h"
#include <stdint.h>

typedef struct mountpoint {
    size_t ref_count;

    struct superblock *sb;
    const char *mount_path;

    struct mountpoint *next;
} mountpoint;

mountpoint *vfs_mountpoint_new(struct superblock *sb, const char *mount_path);
void vfs_mountpoint_delete(mountpoint *mp);
void vfs_mountpoint_ref_nolock(mountpoint *mp);     // mountpoint_list_rwl must be locked
void vfs_mountpoint_unref_nolock(mountpoint *mp);   // mountpoint_list_rwl must be locked
void vfs_mountpoint_ref(mountpoint *mp);
void vfs_mountpoint_unref(mountpoint *mp);
mountpoint *vfs_mountpoint_lookup(const char *path, const char **subpath_ptr);
