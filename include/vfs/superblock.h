#pragma once

#include "util/hashtable.h"
#include "util/mutex.h"
#include "vfs/fs.h"
#include "vfs/vnode.h"
#include <stdint.h>

typedef struct simplefs_sb_data {
    uint64_t data_size_blocks;
    uint64_t index_size_bytes;
    uint64_t total_blocks;
    uint32_t reserved_blocks;
} simplefs_sb_data;

typedef struct superblock {
    mutex mtx;
    size_t ref_count;

    int device_major;               // Major number of host device
    int device_minor;               // Minor number of host device

    unsigned long block_size;       // Block size in bytes
    unsigned char block_size_width; // Block size width in bits
    uint64_t timestamp;

    struct filesystem_type *type;
    struct vnode *mounted;
    struct vnode *covered;

    hashtable vn_hshtbl;
    mutex vn_hshtbl_mtx;

    union {
        simplefs_sb_data simplefs;
        void *           generic;
    } data;
} superblock;

superblock *vfs_superblock_new(struct filesystem_type *type);
void vfs_superblock_delete(superblock *sb);
void vfs_superblock_ref(superblock *sb);
void vfs_superblock_unref(superblock *sb);
