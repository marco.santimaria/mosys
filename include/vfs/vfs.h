#pragma once

#include "config.h"
#include "mem/flex.h"
#include "socket/socket.h"
#include "uapi/limits.h"
#include "uapi/sys/types.h"
#include "util/mutex.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "vfs/fs.h"
#include "vfs/mountpoint.h"
#include "vfs/superblock.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>

struct mountpoint;
struct file_operations;
struct dirent;

enum {
    O_RDONLY    = 0x0001,
    O_WRONLY    = 0x0002,
    O_RDWR      = O_RDONLY | O_WRONLY,
    O_CREAT     = 0x0004,
};

#define SEEK_SET    0
#define SEEK_CUR    1
#define SEEK_END    2

typedef struct dirent {
    ino_t ino;
    char name[NAME_MAX + 1];
} dirent;

// FD file operations

int vfs_open(flex_const_str pathname, int flags, mode_t mode, int *errno_ptr);
int vfs_close(int fd, int *errno_ptr);
void vfs_close_all(void);
ssize_t vfs_read(int fd, flex_mut buf, size_t count, int *errno_ptr);
ssize_t vfs_pread(int fd, flex_mut buf, size_t count, off_t offset, int *errno_ptr);
ssize_t vfs_write(int fd, flex_const buf, size_t count, int *errno_ptr);
ssize_t vfs_pwrite(int fd, flex_const buf, size_t count, off_t offset, int *errno_ptr);
int vfs_ioctl(int fd, unsigned long request, flex_mut argp, int *errno_ptr);
ssize_t vfs_getdents(int fd, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr);
off_t vfs_lseek(int fd, off_t offset, int whence, int *errno_ptr);
int vfs_socket(int domain, int type, int protocol, int *errno_ptr);
int vfs_socketpair(int domain, int type, int protocol, FLEX_MUT(int) sv, int *errno_ptr);
int vfs_bind(int sockfd, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);
ssize_t vfs_recvfrom(int sockfd, flex_mut buf, size_t len, int flags,
                     FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
ssize_t vfs_sendto(int sockfd, flex_const buf, size_t len, int flags,
                   FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr);
int vfs_accept(int sockfd, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr);
int vfs_connect(int sockfd, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr);

// FS operations

int vfs_mount(flex_const_str source, flex_const_str target,
              flex_const_str filesystemtype, unsigned long mountflags,
              flex_const data,
              int *errno_ptr);

// Process operations

int vfs_chdir(flex_const_str path, int *errno_ptr);
int vfs_fchdir(int fd, int *errno_ptr);
