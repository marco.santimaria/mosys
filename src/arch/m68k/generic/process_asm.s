    .text

    .globl  process_enter_state
    .type   process_enter_state, @function
| noreturn void process_enter_state(struct syscall_state *ssp)
process_enter_state:
    move.l  (4,%sp),%sp       | ssp to SSP
    jmp     exception_leave
