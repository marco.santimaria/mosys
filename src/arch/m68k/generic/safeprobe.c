#include "safeprobe.h"
#include "exception.h"
#include "kio/klog.h"
#include "util/mutex.h"
#include <stdatomic.h>

static mutex safeprobe_mtx = MUTEX_INIT;
static atomic_flag safeprobe_flag = ATOMIC_FLAG_INIT;
static exception_func safeprobe_replaced_vector;

static void safeprobe_bus_error(struct exception_state *state);

void safeprobe_begin(void) {
    mutex_lock(&safeprobe_mtx);
    atomic_flag_test_and_set(&safeprobe_flag);
    // TODO: Use exception constants instead of an integer when they are added.
    safeprobe_replaced_vector = exception_vector_replace(safeprobe_bus_error, 2);
    if (!safeprobe_replaced_vector) {
        kwarn("Safe probing is clobbering bus error handler");
    }
}

bool safeprobe_trybegin(void) {
    if (!mutex_trylock(&safeprobe_mtx)) {
        return false;
    }
    atomic_flag_test_and_set(&safeprobe_flag);
    // TODO: Use exception constants instead of an integer when they are added.
    safeprobe_replaced_vector = exception_vector_replace(safeprobe_bus_error, 2);
    if (!safeprobe_replaced_vector) {
        kwarn("Safe probing is clobbering bus error handler");
    }
    return true;
}

bool safeprobe_end_was_clean(void) {
    // TODO: Use exception constants instead of an integer when they are added.
    exception_vector_replace(safeprobe_replaced_vector, 2);
    bool was_clean = atomic_flag_test_and_set(&safeprobe_flag);
    mutex_unlock(&safeprobe_mtx);
    return was_clean;
}

static void safeprobe_bus_error(struct exception_state *state) {
    // Require a long format for recovery
    // Add other formats if needed
    if (state->format_vector_offset >> 12 != 0x8) {
        kpanic("Safeprobe error handling requires a long format exception frame");
    }

    atomic_flag_clear(&safeprobe_flag);

    // We only want to safely handle data accesses
    if (state->fmt_0x8.special_status_word & (1U << 13)) {
        kpanic("Safeprobe-caught bus error was on instruction fetch, not data");
    }

    // Don't re-run access, just read 0s
    state->fmt_0x8.special_status_word |= (1U << 15);
    state->fmt_0x8.data_input_buffer = 0;
}
