    .section .init, "x"

    .globl  _start
    .type   _start, @function
_start:
    lea.l   _init_stack_end,%a7
    jsr     _kinit
    jsr     kmain

_halt:
    stop    #0x2700
    bra     _halt
