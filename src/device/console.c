#include "device/console.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "kio/kio.h"
#include "util/rwlock.h"
#include <stdbool.h>

static rwlock console_rwl = RWLOCK_INIT;
static devreg *console_target_dr = NULL;

bool console_init(const char *console_name) {
    bool result;

    RWLOCK_WRITE_WITH(&console_rwl, {
        console_target_dr = devreg_lookup(console_name);
        if (console_target_dr) {
            result = true;
        } else {
            result = false;
        }
    })

    return result;
}

ssize_t console_read(flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    RWLOCK_READ_WITH(&console_rwl, {
        if (console_target_dr) {
            result = device_read(console_target_dr->major, console_target_dr->minor, buf, count, offset, errno_ptr);
        } else {
            *errno_ptr = ENODEV;
            result = -1;
        }
    })

    return result;
}

ssize_t console_write(flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    RWLOCK_READ_WITH(&console_rwl, {
        if (console_target_dr) {
            result = device_write(console_target_dr->major, console_target_dr->minor, buf, count, offset, errno_ptr);
        } else {
            *errno_ptr = ENODEV;
            result = -1;
        }
    })

    return result;
}

int console_ioctl(unsigned long request, flex_mut argp, int *errno_ptr) {
    ssize_t result;

    RWLOCK_READ_WITH(&console_rwl, {
        if (console_target_dr) {
            result = device_ioctl(console_target_dr->major, console_target_dr->minor, request, argp, errno_ptr);
        } else {
            *errno_ptr = ENODEV;
            result = -1;
        }
    })

    return result;
}
