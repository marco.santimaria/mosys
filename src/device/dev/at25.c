#include "config.h"
#include "module.h"
#include "device/device.h"
#include "device/hardware/mc68901.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "uapi/mosys/spi/spidev.h"
#include "uapi/mosys/spi/at25.h"
#include "util/circbuf.h"
#include "util/util.h"
#include "vfs/devreg.h"
#include <limits.h>
#include <string.h>

#if DEV_AT25_ENABLE

static module_status dev_at25_init(void);
static void dev_at25_deinit(void);

static ssize_t dev_at25_read(int minor, void *buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_at25_write(int minor, const void *buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_at25_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr);

static int at25_wait_wip(int minor, int *errno_ptr);
static int at25_read(int minor, unsigned int page, unsigned char offset_in_page, unsigned char count, char buf[count], int *errno_ptr);
static int at25_write(int minor, unsigned int page, unsigned char offset_in_page, unsigned char count, const char buf[count], int *errno_ptr);

static module dev_at25 = {
    .init = dev_at25_init,
    .deinit = dev_at25_deinit,
    .name = "dev_at25",
};
MODULE_DEFINE(dev_at25);

static int dev_at25_major = 0;
static const int dev_spidev_major = 3;

#define AT25_READ   0x03
#define AT25_WRITE  0x02
#define AT25_WRDI   0x04
#define AT25_WREN   0x06
#define AT25_RDSR   0x05
#define AT25_WRSR   0x01

#define AT25_SR_WPEN        0x80
#define AT25_SR_WPEN_SHIFT  7
#define AT25_SR_BP1         0x08
#define AT25_SR_BP0         0x04
#define AT25_SR_BP          (AT25_SR_BP1 | AT25_SR_BP0)
#define AT25_SR_BP_SHIFT    2
#define AT25_SR_WEL         0x02
#define AT25_SR_WEL_SHIFT   1
#define AT25_SR_WIP         0x01
#define AT25_SR_WIP_SHIFT   0

static const unsigned int at25_end_page = 0x200;
static const unsigned int at25_page_size = 0x40;

static device_operations dev_at25_dops = {
    .read = dev_at25_read,
    .write = dev_at25_write,
    .ioctl = dev_at25_ioctl,
};

static module_status dev_at25_init(void) {
    dev_at25_major = device_register(dev_at25_major, &dev_at25_dops);
    if (dev_at25_major < 0) {
        kpanic("Failed to register at25 devices");
    }

    devreg_new("at250", dev_at25_major, 0); // Minor number matches spidev minor number

    return MODULE_LOADED;
}

static void dev_at25_deinit(void) {
    device_unregister(dev_at25_major);
}

static ssize_t dev_at25_read(int minor, void *buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (offset > ULONG_MAX) {
        return 0;
    }
    unsigned long current_offset = offset;

    size_t count_done = 0;
    size_t count_left = count;

    while (count_left > 0) {
        unsigned long this_page_long = current_offset / at25_page_size;
        if (this_page_long > UINT_MAX) {
            break;
        }
        unsigned int this_page = this_page_long;
        unsigned char this_offset_in_page = current_offset % at25_page_size;
        unsigned char this_count = min(count_left, at25_page_size - this_offset_in_page);

        if (at25_read(minor, this_page, this_offset_in_page, this_count, (char *) buf + count_done, errno_ptr) < 0) {
            return -1;
        }

        current_offset += this_count;
        count_done += this_count;
        count_left -= this_count;
    }

    return count;
}

static ssize_t dev_at25_write(int minor, const void *buf, size_t count, unsigned long long offset, int *errno_ptr) {
    if (offset > ULONG_MAX) {
        return 0;
    }
    unsigned long current_offset = offset;

    size_t count_done = 0;
    size_t count_left = count;

    while (count_left > 0) {
        unsigned long this_page_long = current_offset / at25_page_size;
        if (this_page_long > UINT_MAX) {
            break;
        }
        unsigned int this_page = this_page_long;
        unsigned char this_offset_in_page = current_offset % at25_page_size;
        unsigned char this_count = min(count_left, at25_page_size - this_offset_in_page);

        if (at25_write(minor, this_page, this_offset_in_page, this_count, (const char *) buf + count_done, errno_ptr) < 0) {
            return -1;
        }

        current_offset += this_count;
        count_done += this_count;
        count_left -= this_count;
    }

    return count;
}

static int dev_at25_ioctl(int minor, unsigned long request, void *argp, bool check_user_pointers, int *errno_ptr) {
    if (_IOC_TYPE(request) != AT25_IOC_MAGIC) {
        *errno_ptr = EINVAL;
        return -1;
    }

    char wren_tx_buf[1] = { AT25_WREN };
    char sr_buf[2];

    struct spi_ioc_transfer transfer[3] = {
        {
            .tx_buf = wren_tx_buf,
            .rx_buf = NULL,
            .len = 1,
            .cs_change = true,
        },
        {
            .tx_buf = sr_buf,
            .rx_buf = sr_buf,
            .len = 2,
            .cs_change = true,
        },
    };

    int status;

    // Verify request data
    switch(_IOC_NR(request)) {
    case 0:
        if (_IOC_SIZE(request) != sizeof(bool)) {
            *errno_ptr = EINVAL;
            return -1;
        }
        if (check_user_pointers && !user_ptr_is_valid(argp, sizeof(bool))) {
            *errno_ptr = EFAULT;
            return -1;
        }
        break;
    case 1:
        if (_IOC_SIZE(request) != sizeof(uint8_t)) {
            *errno_ptr = EINVAL;
            return -1;
        }
        if (check_user_pointers && !user_ptr_is_valid(argp, sizeof(uint8_t))) {
            *errno_ptr = EFAULT;
            return -1;
        }
        break;
    default:
        *errno_ptr = EINVAL;
        return -1;
    }

    status = at25_wait_wip(minor, errno_ptr);
    if (status != 0) {
        return status;
    }

    // Read STATUS register
    sr_buf[0] = AT25_RDSR;
    status = device_ioctl(dev_spidev_major, minor, SPI_IOC_MESSAGE(1), &transfer[1], false, errno_ptr);
    if (status != 0) {
        return status;
    }

    if (_IOC_DIR(request) == _IOC_READ) {
        // Store read STATUS register field
        switch(_IOC_NR(request)) {
        case 0:
            *(bool *) argp = sr_buf[1] & AT25_SR_WPEN;
            break;
        case 1:
            *(uint8_t *) argp = (sr_buf[1] & AT25_SR_BP) >> AT25_SR_BP_SHIFT;
            break;
        default:
            *errno_ptr = EINVAL;
            return -1;
        }

        return 0;
    } else if (_IOC_DIR(request) == _IOC_WRITE) {
        // Modify read STATUS register field
        switch(_IOC_NR(request)) {
        case 0:
            sr_buf[1] = (sr_buf[1] & ~AT25_SR_WPEN) | (*(bool *) argp << AT25_SR_WPEN_SHIFT);
            break;
        case 1:
            sr_buf[1] = (sr_buf[1] & ~AT25_SR_BP) | (*(uint8_t *) argp << AT25_SR_BP_SHIFT);
            break;
        default:
            *errno_ptr = EINVAL;
            return -1;
        }

        // Write STATUS register
        sr_buf[0] = AT25_WRSR;
        status = device_ioctl(dev_spidev_major, minor, SPI_IOC_MESSAGE(2), &transfer, false, errno_ptr);
        if (status != 0) {
            return status;
        }

        return 0;
    } else {
        return 0;
    }
}

static int at25_wait_wip(int minor, int *errno_ptr) {
    char sr_buf[2];

    struct spi_ioc_transfer transfer = {
        .tx_buf = sr_buf,
        .rx_buf = sr_buf,
        .len = 2,
        .cs_change = true,
    };

    do {
        sr_buf[0] = AT25_RDSR;

        int status = device_ioctl(dev_spidev_major, minor, SPI_IOC_MESSAGE(1), &transfer, false, errno_ptr);
        if (status != 0) {
            return status;
        }
    } while (sr_buf[1] & AT25_SR_WIP);

    return 0;
}

static int at25_read(int minor, unsigned int page, unsigned char offset_in_page, unsigned char count, char buf[count], int *errno_ptr) {
    if (page >= at25_end_page ||
        offset_in_page + count > at25_page_size) {
        *errno_ptr = EINVAL;
        return -1;
    }

    int status = at25_wait_wip(minor, errno_ptr);
    if (status != 0) {
        return status;
    }

    const uint16_t address = page * at25_page_size + offset_in_page;
    char read_tx_buf[count + 3];
    read_tx_buf[0] = AT25_READ;
    read_tx_buf[1] = (address >> 8) & 0xFF;
    read_tx_buf[2] = address & 0xFF;
    char read_rx_buf[count + 3];

    struct spi_ioc_transfer transfer[1] = {
        {
            .tx_buf = read_tx_buf,
            .rx_buf = read_rx_buf,
            .len = count + 3,
            .cs_change = true,
        },
    };

    status = device_ioctl(dev_spidev_major, minor, SPI_IOC_MESSAGE(1), transfer, false, errno_ptr);
    if (status == 0) {
        memcpy(buf, &read_rx_buf[3], count);
        return count;
    } else {
        return status;
    }
}

static int at25_write(int minor, unsigned int page, unsigned char offset_in_page, unsigned char count, const char buf[count], int *errno_ptr) {
    if (page >= at25_end_page ||
        offset_in_page + count > at25_page_size) {
        *errno_ptr = EINVAL;
        return -1;
    }

    int status = at25_wait_wip(minor, errno_ptr);
    if (status != 0) {
        return status;
    }

    const uint16_t address = page * at25_page_size + offset_in_page;

    char wren_tx_buf[1] = { AT25_WREN };

    char write_tx_buf[count + 3];
    write_tx_buf[0] = AT25_WRITE;
    write_tx_buf[1] = (address >> 8) & 0xFF;
    write_tx_buf[2] = address & 0xFF;
    memcpy(&write_tx_buf[3], buf, count);

    struct spi_ioc_transfer transfer[2] = {
        {
            .tx_buf = wren_tx_buf,
            .rx_buf = NULL,
            .len = 1,
            .cs_change = true,
        },
        {
            .tx_buf = write_tx_buf,
            .rx_buf = NULL,
            .len = count + 3,
            .cs_change = true,
        },
    };

    status = device_ioctl(dev_spidev_major, minor, SPI_IOC_MESSAGE(2), transfer, false, errno_ptr);
    if (status == 0) {
        return count;
    } else {
        return status;
    }
}

#endif
