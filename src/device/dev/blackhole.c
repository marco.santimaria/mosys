#include "device/device.h"
#include "vfs/devreg.h"
#include "uapi/errno.h"
#include "module.h"
#include "kio/klog.h"
#include "util/util.h"
#include <string.h>

static module_status dev_blackhole_init(void);
static void dev_blackhole_deinit(void);

static ssize_t dev_blackhole_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_blackhole_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_blackhole_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module dev_blackhole = {
    .init = dev_blackhole_init,
    .deinit = dev_blackhole_deinit,
    .name = "dev_blackhole",
};
MODULE_DEFINE(dev_blackhole);

static const int dev_blackhole_major = 1;

static device_operations dev_blackhole_dops = {
    dev_blackhole_read,
    dev_blackhole_write,
    dev_blackhole_ioctl,
};

static module_status dev_blackhole_init(void) {
    if (dev_blackhole_major != device_register(dev_blackhole_major, &dev_blackhole_dops)) {
        kpanic("Failed to register blackhole devices");
    }

    devreg_new("null", dev_blackhole_major, 0);
    devreg_new("zero", dev_blackhole_major, 1);
    devreg_new("full", dev_blackhole_major, 2);
    devreg_new("mem",  dev_blackhole_major, 3);

    return MODULE_LOADED;
}

static void dev_blackhole_deinit(void) {
    device_unregister(dev_blackhole_major);
}

static ssize_t dev_blackhole_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // null
        return 0;
    case 1: // zero
    case 2: // full
        flex_set(buf, 0, count);
        return count;
    case 3: // mem
        if (offset > SIZE_MAX) {
            return 0;
        } else if (offset > SIZE_MAX - count) {
            count = SIZE_MAX - offset + 1;
        }
        flex_to(buf, (const void *) (size_t) offset, count);
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static ssize_t dev_blackhole_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    switch(minor) {
    case 0: // null
    case 1: // zero
        return count;
    case 2: // full
        *errno_ptr = ENOSPC;
        return -1;
    case 3: // mem
        if (offset > SIZE_MAX) {
            return 0;
        } else if (offset > SIZE_MAX - count) {
            count = SIZE_MAX - offset + 1;
        }
        flex_from((void *) (size_t) offset, buf, count);
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static int dev_blackhole_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    UNUSED(minor);
    UNUSED(request);
    UNUSED(argp);

    *errno_ptr = EINVAL;
    return -1;
}
