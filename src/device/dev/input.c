#include "device/dev/input.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "kio/kio.h"
#include "uapi/asm/ioctl.h"
#include "util/rwlock.h"
#include "util/util.h"
#include <string.h>

#define INPUT_MAX 64

static module_status input_init(void);
static void input_deinit(void);

static ssize_t input_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t input_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int input_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static module input = {
    .init = input_init,
    .deinit = input_deinit,
    .name = "input",
};
MODULE_DEFINE(input);

static const int input_major = 13;

static input_info *input_list[INPUT_MAX] = {0};
static rwlock input_list_lock;

static device_operations input_dops = {
    .read = input_read,
    .write = input_write,
    .ioctl = input_ioctl,
};

static module_status input_init(void) {
    if (input_major != device_register(input_major, &input_dops)) {
        kpanic("Failed to register input devices");
    }

    return MODULE_LOADED;
}

static void input_deinit(void) {

}

static ssize_t input_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    if (minor < 0 || minor >= INPUT_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    input_info *input;

    RWLOCK_READ_WITH(&input_list_lock, {
        input = input_list[minor];
        // TODO: Need to lock input?
    })

    if (!input) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    count = round_down(count, sizeof(struct input_event));

    if (input->ops->read) {
        result = input->ops->read(input, buf, count, offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

static ssize_t input_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    ssize_t result;

    if (minor < 0 || minor >= INPUT_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    input_info *input;

    RWLOCK_READ_WITH(&input_list_lock, {
        input = input_list[minor];
        // TODO: Need to lock input?
    })

    if (!input) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    count = round_down(count, sizeof(struct input_event));

    if (input->ops->write) {
        result = input->ops->write(input, buf, count, offset, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

static int input_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    int result;

    if (minor < 0 || minor >= INPUT_MAX) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    input_info *input;

    RWLOCK_READ_WITH(&input_list_lock, {
        input = input_list[minor];
        // TODO: Need to lock input?
    })

    if (!input) {
        *errno_ptr = ENOENT;
        result = -1;
        goto done;
    }

    if (_IOC_TYPE(request) == INPUT_MAGIC) {
        switch(_IOC_NR(request)) {
            // TODO: Handle generic cases
        default:
            *errno_ptr = EINVAL;
            result = -1;
        }
    } else if (input->ops->ioctl) {
        result = input->ops->ioctl(input, request, argp, errno_ptr);
    } else {
        *errno_ptr = EINVAL;
        result = -1;
    }

done:
    return result;
}

input_info *input_new(void) {
    input_info *result = kalloc(sizeof(input_info));
    if (result) {
        // Do any init
    }
    return result;
}

void input_delete(input_info *input) {
    kfree(input);
}

void input_register(input_info *input) {
    bool success = false;

    RWLOCK_WRITE_WITH(&input_list_lock, {
        for (size_t i = 0; i < INPUT_MAX; ++i) {
            if (!input_list[i]) {
                input_list[i] = input;

                char tmp_name[DEVREG_NAME_MAX_LENGTH + 1];
                kio_snprintf(tmp_name, sizeof(tmp_name), "input%u", (unsigned) i);
                devreg_new(tmp_name, input_major, i);

                success = true;

                break;
            }
        }
    })

    if (!success) {
        kpanic("Too many input devices");
    }
}
