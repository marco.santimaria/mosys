#include "config.h"
#include "module.h"
#include "device/device.h"
#include "device/hardware/mc68901.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "util/circbuf.h"
#include "vfs/devreg.h"
#include <string.h>

#if DEV_MC68901_ENABLE

static module_status dev_mc68901_init(void);
static void dev_mc68901_deinit(void);

static void mc68901_rcv_buf_full(void);
static void mc68901_rcv_err(void);
static void mc68901_xmit_buf_empty(void);
static void mc68901_xmit_err(void);

static ssize_t dev_mc68901_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr);
static ssize_t dev_mc68901_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr);
static int dev_mc68901_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr);

static const char *const dev_mc68901_deps[] = {
    "tty",
    NULL,
};
static module dev_mc68901 = {
    .init = dev_mc68901_init,
    .deinit = dev_mc68901_deinit,
    .name = "dev_mc68901",
    .deps = dev_mc68901_deps,
};
MODULE_DEFINE(dev_mc68901);

static const int dev_mc68901_major = 2;

static uint8_t rx_buf[256];
static circbuf_uint8_t rx_circbuf = {
    rx_buf,
    256,
};
static uint8_t tx_buf[256];
static circbuf_uint8_t tx_circbuf = {
    tx_buf,
    256,
};

static device_operations dev_mc68901_dops = {
    dev_mc68901_read,
    dev_mc68901_write,
    dev_mc68901_ioctl,
};

static module_status dev_mc68901_init(void) {
    // Setup Timer D to 307.2 kHz = 3.6864 MHz / 4 / 3
    mc68901_tcdcr_dc_set(MC68901_TCDCR_DC_STOPPED);
    mc68901_tddr_set(3);
    mc68901_tcdcr_dc_set(MC68901_TCDCR_DC_DELAY_4);

    // Set USART to /16 ASYNC 8N1
    mc68901_ucr_set(MC68901_UCR_CLK_16 | MC68901_UCR_WL_8 | MC68901_UCR_ST_1_1__ASYNC);

    // Set USART to enable RX
    mc68901_rsr_set(MC68901_RSR_RE);

    // Set USART idle TX pin state high and enable TX
    mc68901_tsr_set(MC68901_TSR_HL_HIGH | MC68901_TSR_TE);

    // Install interrupt handlers
    mc68901_interrupt_install(mc68901_rcv_buf_full, MC68901_INTERRUPT_RCV_BUF_FULL);
    mc68901_interrupt_install(mc68901_rcv_err, MC68901_INTERRUPT_RCV_ERR);
    mc68901_interrupt_install(mc68901_xmit_buf_empty, MC68901_INTERRUPT_XMIT_BUF_EMPTY);
    mc68901_interrupt_install(mc68901_xmit_err, MC68901_INTERRUPT_XMIT_ERR);

    // Enable MC68901 interrupts
    mc68901_ier_bitset(
        MC68901_INTERRUPT_RCV_BUF_FULL |
        MC68901_INTERRUPT_RCV_ERR |
        MC68901_INTERRUPT_XMIT_BUF_EMPTY |
        MC68901_INTERRUPT_XMIT_ERR
    );
    mc68901_imr_bitset(
        MC68901_INTERRUPT_RCV_BUF_FULL |
        MC68901_INTERRUPT_RCV_ERR |
        MC68901_INTERRUPT_XMIT_BUF_EMPTY |
        MC68901_INTERRUPT_XMIT_ERR
    );

    // Enable (lower) RTS
    mc68901_gpdr_set_masked(0, 1U << 7);

    if (dev_mc68901_major != device_register(dev_mc68901_major, &dev_mc68901_dops)) {
        kpanic("Failed to register mc68901 devices");
    }

    devreg_new("uart0", dev_mc68901_major, 0);

    return MODULE_LOADED;
}

static void dev_mc68901_deinit(void) {
    device_unregister(dev_mc68901_major);
}

static int mc68901_recv(void) {
    // Enable (lower) RTS
    // TODO: Don't hardcode
    mc68901_gpdr_set_masked(0, 1U << 7);

    int result = -1;

    thread_scheduler_lock();

    uint8_t c;
    if (circbuf_uint8_t_get(&rx_circbuf, &c)) {
        result = c;
    }

    thread_scheduler_unlock();

    return result;
}

static void mc68901_send(uint8_t c) {
    // Enable (lower) RTS
    // TODO: Don't hardcode
    mc68901_gpdr_set_masked(0, 1U << 7);

    bool has_sent = false;
    while (!has_sent) {
        thread_scheduler_lock();

        if (mc68901_tsr_get() & MC68901_TSR_BE) {
            mc68901_udr_set(c);
            has_sent = true;
        } else {
            has_sent = circbuf_uint8_t_put(&tx_circbuf, c);
        }

        thread_scheduler_unlock();
    }
}

static ssize_t dev_mc68901_read(int minor, flex_mut buf, size_t count, unsigned long long offset, int *errno_ptr) {
    flex_mut_pinned buf_pin = flex_pin(buf);
    switch(minor) {
    case 0: // uart0
        for (size_t i = 0; i < count; ++i) {
            int tmp = mc68901_recv();
            if (tmp >= 0) {
                ((uint8_t *) buf_pin.ptr)[i] = tmp;
            } else {
                return i;
            }
        }
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static ssize_t dev_mc68901_write(int minor, flex_const buf, size_t count, unsigned long long offset, int *errno_ptr) {
    flex_const_pinned buf_pin = flex_pin(buf);
    switch(minor) {
    case 0: // uart0
        for (size_t i = 0; i < count; ++i) {
            mc68901_send(((uint8_t *) buf_pin.ptr)[i]);
        }
        return count;
    default:
        *errno_ptr = ENOENT;
        return -1;
    }
}

static int dev_mc68901_ioctl(int minor, unsigned long request, flex_mut argp, int *errno_ptr) {
    *errno_ptr = EINVAL;
    return -1;
}

__attribute__((interrupt)) static void mc68901_rcv_buf_full(void) {
    mc68901_isr_bitclear(MC68901_INTERRUPT_RCV_BUF_FULL);

    uint8_t c = mc68901_udr_get();
    circbuf_uint8_t_put(&rx_circbuf, c);
}

__attribute__((interrupt)) static void mc68901_rcv_err(void) {
    mc68901_isr_bitclear(MC68901_INTERRUPT_RCV_ERR);
}

__attribute__((interrupt)) static void mc68901_xmit_buf_empty(void) {
    mc68901_isr_bitclear(MC68901_INTERRUPT_XMIT_BUF_EMPTY);

    uint8_t c;
    if (circbuf_uint8_t_get(&tx_circbuf, &c)) {
        mc68901_udr_set(c);
    }
}

__attribute__((interrupt)) static void mc68901_xmit_err(void) {
    mc68901_isr_bitclear(MC68901_INTERRUPT_XMIT_ERR);
}

#endif
