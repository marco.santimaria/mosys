#include "file/cpio.h"
#include "kio/klog.h"
#include "util/util.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

// TODO: Limit all accesses to the archive

// len == -1 means no length limit
unsigned long cpio_oct_atoi(const char *str, ssize_t len) {
    unsigned long val = 0;
    size_t chars_left = (len == -1) ? SIZE_MAX : (size_t) len;
    while (chars_left > 0 && (*str) != '\0' && (*str) != ' ') {
        if ((*str) < '0' || (*str) > '7') {
            // TODO: Don't panic, this is a user error, not a system one
            kpanic("Unexpected character encountered while parsing cpio numeric field");
        }
        val *= 8;
        val += *str - '0';
        str += 1;
        chars_left -= 1;
    }
    return val;
}

char *cpio_odc_get_file(struct cpio_odc_header *ptr) {
    return cpio_odc_get_name(ptr) + round_up(cpio_odc_get_namesize(ptr), 1);
}

struct cpio_odc_header *cpio_odc_get_next_nocheck(struct cpio_odc_header *ptr) {
    return (struct cpio_odc_header *) (cpio_odc_get_file(ptr) + round_up(cpio_odc_get_filesize(ptr), 2));
}

struct cpio_odc_header *cpio_odc_get_next(struct cpio_odc_header *ptr) {
    if (!ptr)
        return NULL;

    if (cpio_odc_is_trailer(ptr))
        return NULL;

    struct cpio_odc_header *found_entry = cpio_odc_get_next_nocheck(ptr);
    if (cpio_odc_is_trailer(found_entry))
        return NULL;

    return found_entry;
}

bool cpio_odc_is_trailer(struct cpio_odc_header *ptr) {
    // TODO: Use strncmp()
    return strcmp(cpio_odc_get_name(ptr), "TRAILER!!!") == 0;
}

struct cpio_odc_header *cpio_odc_get_nth_entry(struct cpio_odc_header *ptr, size_t n) {
    if (!ptr)
        return NULL;

    if (n == 0) {
        return ptr;
    } else {
        return cpio_odc_get_nth_entry(cpio_odc_get_next(ptr), n - 1);
    }
}

struct cpio_odc_header *cpio_odc_lookup_name(struct cpio_odc_header *ptr, const char *name, size_t *result_index_ptr) {
    size_t result_index = 0;
    while (ptr) {
        if (strncmp(name, cpio_odc_get_name(ptr), cpio_odc_get_namesize(ptr)) == 0) {
            break;
        } else {
            ptr = cpio_odc_get_next(ptr);
        }

        result_index += 1;
    }

    if (ptr && result_index_ptr) {
        *result_index_ptr = result_index;
    }
    return ptr;
}
