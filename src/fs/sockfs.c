#include "config.h"
#include "device/device.h"
#include "vfs/devreg.h"
#include "vfs/fdesc.h"
#include "kalloc.h"
#include "module.h"
#include "socket/socket.h"
#include "kio/klog.h"
#include "vfs/vfs.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>

#if SOCKFS_ENABLE

#if !SOCKET_ENABLE
#error SOCKFS_ENABLE requires SOCKET_ENABLE
#endif

static module_status sockfs_init(void);
static void sockfs_deinit(void);

static vnode *sockfs_fsops_new_vnode(superblock *sb, int *errno_ptr);
static void sockfs_fsops_delete_vnode(superblock *sb, vnode *vn);
static vnode *sockfs_fsops_mount(size_t source_len, const char source[source_len],
                                 unsigned long mountflags, const void *data, bool check_user_pointers,
                                 int *errno_ptr);

static int sockfs_fops_release(fdesc *fdsc, int *errno_ptr);

static atomic_bool sockfs_is_init = false;

static module sockfs = {
    .init = sockfs_init,
    .deinit = sockfs_deinit,
    .name = "sockfs",
};
MODULE_DEFINE(sockfs);

static fs_operations sockfs_fsops = {
    .new_vnode = sockfs_fsops_new_vnode,
    .delete_vnode = sockfs_fsops_delete_vnode,
    .mount = sockfs_fsops_mount,
};

static file_operations sockfs_fops = {
    .release = sockfs_fops_release,
};

static filesystem_type sockfs_fstype = {
    .name = "sockfs",
    .fsops = &sockfs_fsops,
};

superblock *sockfs_sb = NULL;

static module_status sockfs_init(void) {
    vfs_filesystem_type_register(&sockfs_fstype);

    sockfs_is_init = true;
    return MODULE_LOADED;
}

static void sockfs_deinit(void) {
    sockfs_is_init = false;

    // TODO: Figure out what to actually do here
}

static vnode *sockfs_fsops_new_vnode(superblock *sb, int *errno_ptr) {
    vfs_superblock_ref(sb);
    vnode *vn = vfs_vnode_new(sb);
    if (vn) {
        MUTEX_WITH(&vn->mtx) {
            vn->fops = &sockfs_fops;
        }
    } else {
        vfs_superblock_unref(sb);
    }
    return vn;
}

static void sockfs_fsops_delete_vnode(superblock *sb, vnode *vn) {
    if (vn->type == FILE_TYPE_DEVICE) {
        devreg_unref(vn->device.dr);
        vn->device.dr = NULL;
    }
    vfs_superblock_unref(vn->sb);
    vn->sb = NULL;
    vfs_vnode_delete(vn);
    vn = NULL;
}

static vnode *sockfs_fsops_mount(size_t source_len, const char source[source_len],
                                 unsigned long mountflags, const void *data, bool check_user_pointers,
                                 int *errno_ptr) {
    superblock *sb = vfs_superblock_new(&sockfs_fstype);
    if (!sb) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    vnode *root_vn = sb->type->fsops->new_vnode(sb, errno_ptr);
    vfs_superblock_unref(sb);
    if (!root_vn) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    MUTEX_WITH(&root_vn->mtx) {
        root_vn->type = FILE_TYPE_DIRECTORY;
        root_vn->inode = 0;
    }

    MUTEX_WITH(&root_vn->sb->mtx) {
        vfs_vnode_ref(root_vn);
        sb->mounted = root_vn;
    }

    sockfs_sb = sb;

    return root_vn;
}

static int sockfs_fops_release(fdesc *fdsc, int *errno_ptr) {
    if (fdsc->vn->type != FILE_TYPE_SOCKET)
        kpanic("Passed non-socket fdesc as socket");

    int result = socket_close(fdsc->vn->socket.sock, errno_ptr);

    return result;
}

#endif
