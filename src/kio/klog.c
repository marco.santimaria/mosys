#include "kio/klog.h"
#include "config.h"
#include "thread/thread.h"
#include "mem/mem_mapping.h"
#include "kio/kio.h"
#include <stdarg.h>

#if KLOG_USE_COLOURS
#define KLOG_GREEN "\033[32m"
#define KLOG_YELLOW "\033[33;1m"
#define KLOG_RED "\033[31;1;4m"
#define KLOG_BLUE "\033[34m"
#define KLOG_DEFAULT "\033[0m"
#else
#define KLOG_GREEN ""
#define KLOG_YELLOW ""
#define KLOG_RED ""
#define KLOG_BLUE ""
#define KLOG_DEFAULT ""
#endif

void klog_func(enum klog_level level, bool print_file, const char *file, unsigned line, const char *reason, ...) {
#if KLOG_ALWAYS_PRINT_FILE
    print_file = true;
#endif

    const char *level_string;
    switch(level) {
    case KLOG_LEVEL_INFO:
        level_string = KLOG_GREEN "INFO" KLOG_DEFAULT;
        break;
    case KLOG_LEVEL_WARN:
        level_string = KLOG_YELLOW "WARN" KLOG_DEFAULT;
        break;
    case KLOG_LEVEL_PANIC:
        level_string = KLOG_RED "PANIC" KLOG_DEFAULT;
        break;
    case KLOG_LEVEL_DEBUG:
        level_string = KLOG_BLUE "DEBUG" KLOG_DEFAULT;
        break;
    default:
        kpanic("Unknown klog level");
    }

    va_list args;
    va_start(args, reason);
    char message_buffer[KLOG_MAX_MESSAGE_LEN];
    kio_vsnprintf(message_buffer, KLOG_MAX_MESSAGE_LEN, reason, args);
    va_end(args);

    if (print_file) {
        kio_printf("[%s] %s at %s:%u\n", level_string, message_buffer, file, line);
    } else {
        kio_printf("[%s] %s\n", level_string, message_buffer);
    }
}

#define KLOG_RETURN_ADDR_PRINTER(level, done_target)    \
    if (mem_mapping_contains_ptr_k(                     \
        thread_current->sup_stack,                      \
        __builtin_frame_address(level))                 \
    ) {                                                 \
        void *ret_addr =                                \
            __builtin_extract_return_addr(              \
                __builtin_return_address(level)         \
            );                                          \
        kio_printf("%08p\n", ret_addr);                 \
    } else {                                            \
        goto done_target;                               \
    }

void klog_print_stack_trace(void) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wframe-address"
    KLOG_RETURN_ADDR_PRINTER(0, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(1, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(2, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(3, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(4, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(5, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(6, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(7, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(8, done_call_stack);
    KLOG_RETURN_ADDR_PRINTER(9, done_call_stack);
#pragma GCC diagnostic pop
done_call_stack:
}

noreturn void kpanic_blocker(void) {
    kio_printf("Call stack:\n");
    klog_print_stack_trace();

    // TODO: Disable task switching
    while (1) {
        // Spin forever
    }
}
