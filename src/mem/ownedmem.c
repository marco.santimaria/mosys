#include "mem/ownedmem.h"
#include "kalloc.h"
#include "kio/klog.h"

ownedmem_ptr ownedmem_new_from_mem_block(mem_block block) {
    ownedmem_ptr owndmm = kalloc(sizeof(ownedmem));
    if (owndmm) {
        mutex_init(&owndmm->mtx);
        owndmm->ref_count = 1;
        owndmm->block = block;
    }
    return owndmm;
}

void ownedmem_new_from_mem_block_no_kheap(mem_block block, ownedmem *owndmm) {
    mutex_init(&owndmm->mtx);
    owndmm->ref_count = 1;
    owndmm->block = block;
}

ownedmem_ptr ownedmem_new(size_t size) {
    mem_block block = mem_alloc(size);
    ownedmem_ptr owndmm = NULL;
    if (mem_block_is_present(block)) {
        owndmm = ownedmem_new_from_mem_block(block);
        if (!owndmm) {
            mem_free(block);
        }
    }
    return owndmm;
}

bool ownedmem_new_no_kheap(size_t size, ownedmem *owndmm) {
    mem_block block = mem_alloc(size);
    if (mem_block_is_present(block)) {
        ownedmem_new_from_mem_block_no_kheap(block, owndmm);
        return true;
    }
    return false;
}

void ownedmem_delete(ownedmem_ptr owndmm) {
    if (!owndmm) {
        return;
    }

    if (owndmm->ref_count != 0) {
        kpanic("Tried to delete still-referenced ownedmem");
    }

    if (mem_block_is_present(owndmm->block)) {
        mem_free(owndmm->block);
        owndmm->block = MEM_BLOCK_NULL;
    }

    kfree(owndmm);
}

ownedmem_ptr ownedmem_ref(ownedmem_ptr owndmm) {
    if (!owndmm) {
        return NULL;
    }

    MUTEX_WITH(&owndmm->mtx, {
        if (owndmm->ref_count == SIZE_MAX) {
            kpanic("Overflowed ownedmem ref_count");
        }
        owndmm->ref_count += 1;
    })

    return owndmm;
}

void ownedmem_unref(ownedmem_ptr owndmm) {
    if (!owndmm) {
        return;
    }

    MUTEX_WITH(&owndmm->mtx, {
        if (owndmm->ref_count == 0) {
            kpanic("Underflowed ownedmem ref_count");
        }
        owndmm->ref_count -= 1;

        if (owndmm->ref_count == 0) {
            ownedmem_delete(owndmm);
            owndmm = NULL;
            MUTEX_RETURN_NO_UNLOCK();   // Return without releasing mutex, it's been deleted
        }
    })
}

ownedmem_ptr ownedmem_clone(ownedmem_ptr owndmm_orig) {
    if (!owndmm_orig) {
        return NULL;
    }

    mem_block cloned_block;
    MUTEX_WITH(&owndmm_orig->mtx, {
        cloned_block = mem_clone(owndmm_orig->block);
    })

    ownedmem_ptr owndmm = NULL;
    if (mem_block_is_present(cloned_block)) {
        owndmm = ownedmem_new_from_mem_block(cloned_block);
    }

    if (!owndmm) {
        mem_free(cloned_block);
    }

    return owndmm;
}
