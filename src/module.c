#include "module.h"
#include "kio/klog.h"
#include <string.h>

extern module *_module_tab[];
extern module *_module_tab_end[];

int module_init_static() {
    bool did_init;
    do {
        did_init = false;
        for (module **module_tab_ptr = _module_tab; module_tab_ptr < _module_tab_end; ++module_tab_ptr) {
            module *module_ptr = *module_tab_ptr;

            switch (module_ptr->status) {
            case MODULE_UNLOADED:
            case MODULE_DEFERRED_LOAD:
                if (module_has_deps_loaded(module_ptr)) {
                    if (module_ptr->init) {
                        module_ptr->status = module_ptr->init();
                    }
                } else {
                    module_ptr->status = MODULE_DEFERRED_LOAD;
                }
                break;
            default:
                // Don't try to load modules which already succeeded or failed
                continue;
            }

            switch (module_ptr->status) {
            case MODULE_UNLOADED:
                kpanic("Module \"%s\" refused to try to load", module_ptr->name);
                break;
            case MODULE_DEFERRED_LOAD:
                break;
            case MODULE_LOADED:
                did_init = true;
                kinfo("Module \"%s\" loaded", module_ptr->name);
                break;
            case MODULE_SKIPPED_LOAD:
                kinfo("Module \"%s\" skipped loading", module_ptr->name);
                break;
            case MODULE_FAILED_LOAD:
                kwarn("Module \"%s\" failed loading", module_ptr->name);
                break;
            default:
                kpanic("Module \"%s\" init returned unknown result %d", module_ptr->name, (int) module_ptr->status);
                break;
            }
        }
    } while (did_init);

    for (module **module_tab_ptr = _module_tab; module_tab_ptr < _module_tab_end; ++module_tab_ptr) {
        module *module_ptr = *module_tab_ptr;

        switch (module_ptr->status) {
        case MODULE_LOADED:
        case MODULE_SKIPPED_LOAD:
        case MODULE_FAILED_LOAD:
            // Do nothing, these are the expected results
            break;
        case MODULE_DEFERRED_LOAD:
            // A forever-deferred load of a static module is a failure
            module_ptr->status = MODULE_FAILED_LOAD;
            kwarn("Module \"%s\" failed loading", module_ptr->name);
            break;
        case MODULE_UNLOADED:
            kpanic("Module \"%s\" was never loaded", module_ptr->name);
            module_ptr->status = MODULE_FAILED_LOAD;
            break;
        default:
            kpanic("Module \"%s\" has unexpected status %d", module_ptr->name, (int) module_ptr->status);
            module_ptr->status = MODULE_FAILED_LOAD;
            break;
        }
    }

    return 0;
}

void module_deinit_static() {
    for (module **module_tab_ptr = _module_tab_end; module_tab_ptr > _module_tab; --module_tab_ptr) {
        module *module_ptr = *(module_tab_ptr - 1);
        if (module_ptr->deinit) {
            module_ptr->deinit();
            kinfo("Module deinit: %s", module_ptr->name);
        }
    }
}

bool module_is_loaded(const char *name) {
    for (module **module_tab_ptr = _module_tab; module_tab_ptr < _module_tab_end; ++module_tab_ptr) {
        module *module_ptr = *module_tab_ptr;

        if (strcmp(name, module_ptr->name) == 0) {
            if (module_ptr->status == MODULE_LOADED) {
                return true;
            }
        }
    }

    return false;
}

bool module_has_deps_loaded(module *module_ptr) {
    if (!module_ptr->deps) {
        // No deps to load first
        return true;
    }

    for (const char *const *dep_str_ptr = module_ptr->deps; *dep_str_ptr; ++dep_str_ptr) {
        if (!module_is_loaded(*dep_str_ptr)) {
            return false;
        }
    }

    return true;
}
