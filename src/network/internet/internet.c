#include "network/internet/internet.h"
#include "network/internet/internet_ipv4.h"
#include "kio/klog.h"

#if NETWORK_INTERNET_ENABLE

internet_type internet_type_from_ethertype(ethertype type) {
    switch (type) {

#if NETWORK_INTERNET_IPV4_ENABLE
    case ETHERTYPE_IPV4:
        return INTERNET_TYPE_IPV4;
#endif

    default:
        return INTERNET_TYPE_NONE;
    }
}

ethertype internet_type_to_ethertype(internet_type type) {
    switch (type) {

#if NETWORK_INTERNET_IPV4_ENABLE
    case INTERNET_TYPE_IPV4:
        return ETHERTYPE_IPV4;
#endif

    default:
        return ETHERTYPE_NONE;
    }
}

void internet_receive_packet(
    internet_type type,
    netdev *ntdv, const link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length]
) {
    if (type == INTERNET_TYPE_NONE) {
        kwarn("Tried to receive a network packet with no type");
        return;
    }

    switch (type) {
    case INTERNET_TYPE_NONE:
        kwarn("Tried to receive a network packet with no type");
        return;

#if NETWORK_INTERNET_IPV4_ENABLE
    case INTERNET_TYPE_IPV4:
        internet_ipv4_receive_packet(ntdv, dtlnk_info, packet_length, packet);
        break;
#endif

    default:
        kpanic("Unknown network type");
    }
}

#endif
