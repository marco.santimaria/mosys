#include "network/internet/internet_ipv4.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "uapi/errno.h"

#if NETWORK_INTERNET_IPV4_ENABLE

bool internet_ipv4_make_frame(
    const internet_ipv4_packet_info *info,
    netdev *ntdv, const link_frame_info *dtlnk_info,
    size_t datagram_length, char **datagram_out,
    size_t *packet_length_out, char **packet_out,
    size_t *frame_length_out, char **frame_out,
    int *errno_ptr
) {
    // TODO: Support longer header
    size_t header_length = internet_ipv4_needed_header_length(info);
    size_t packet_length = datagram_length + header_length;

    char *packet, *frame;
    size_t frame_length;
    if (!link_make_frame(ntdv, dtlnk_info, packet_length, &packet, &frame_length, &frame, errno_ptr)) {
        return false;
    }

    if (datagram_out) {
        *datagram_out = packet + header_length;
    }
    if (packet_length_out) {
        *packet_length_out = packet_length;
    }
    if (packet_out) {
        *packet_out = packet;
    }
    if (frame_length_out) {
        *frame_length_out = frame_length;
    }
    if (frame_out) {
        *frame_out = frame;
    }
    return true;
}

size_t internet_ipv4_needed_header_length(const internet_ipv4_packet_info *info) {
    return 20;
}

size_t internet_ipv4_header_length(size_t packet_length, const char packet[packet_length]) {
    if (packet_length < 20) {
        return 0;
    }

    uint8_t version_ihl;
    memcpy(&version_ihl, packet + 0, 1);
    uint8_t ihl = (version_ihl >> 0) & 0x0F;

    size_t header_length = ihl * 4;
    if (header_length > packet_length) {
        return 0;
    }

    return header_length;
}

bool internet_ipv4_encode_header(
    const internet_ipv4_packet_info *info,
    uint16_t total_length,
    size_t header_length, char header[header_length]
) {
    kassert(header_length % 4 == 0);
    fill_zero_size(header, header_length);

    // Octet 0
    uint8_t version = 4;
    uint8_t ihl = header_length / 4;
    uint8_t version_ihl = (version << 4) | (ihl << 0);
    memcpy(header + 0, &version_ihl, 1);

    // Octet 1
    // TODO
    // Leave blank

    // Octets 2-3
    net_u16 total_length_net = hton_u16(total_length);
    memcpy(header + 1, &total_length_net, 2);

    // Octets 4-5
    net_u16 identification_net = hton_u16(info->identification);
    memcpy(header + 2, &identification_net, 2);

    // Octets 6-7
    // TODO
    // Leave blank

    // Octet 8
    memcpy(header + 8, &info->time_to_live, 1);

    // Octet 9
    memcpy(header + 9, &info->protocol, 1);

    // Octets 10-11
    // Checksum to be calculated, leave 0 for now

    // Octets 12-15
    memcpy(header + 12, &info->src_ip_addr, 4);

    // Octets 16-19
    memcpy(header + 16, &info->dst_ip_addr, 4);

    // Calculate checksum
    uint16_t checksum = internet_ipv4_header_checksum(header_length, header);
    net_u16 checksum_net = hton_u16(checksum);
    memcpy(header + 10, &checksum_net, 2);

    return true;
}

bool network_ipv4_decode_header(
    size_t packet_length, const char packet[packet_length],
    uint16_t *total_length_out,
    internet_ipv4_packet_info *info_out
) {
    size_t header_length = internet_ipv4_header_length(packet_length, packet);
    if (header_length == 0) {
        return false;
    }

    const char *header = packet;
    if (internet_ipv4_header_checksum(header_length, header) != 0x0000) {
        return false;
    }

    // TODO: Handle variable-length
    if (header_length != 20) {
        return false;
    }

    uint16_t total_length;
    internet_ipv4_packet_info info;

    // Octet 0
    uint8_t version_ihl;
    memcpy(&version_ihl, header + 0, 1);
    uint8_t version = (version_ihl >> 4) & 0x0F;
    uint8_t ihl = (version_ihl >> 0) & 0x0F;
    if (version != 4 || ihl * 4 != header_length) {
        return false;
    }

    // Octet 1
    // TODO

    // Octets 2-3
    net_u16 total_length_net;
    memcpy(&total_length_net, header + 2, 2);
    total_length = ntoh_u16(total_length_net);

    // Octets 4-5
    net_u16 identification_net;
    memcpy(&identification_net, header + 2, 2);
    info.identification = ntoh_u16(identification_net);

    // Octets 6-7
    // TODO

    // Octet 8
    memcpy(&info.time_to_live, header + 8, 1);

    // Octet 9
    memcpy(&info.protocol, header + 9, 1);

    // Octets 10-11
    // Checksum already checked

    // Octets 12-15
    memcpy(&info.src_ip_addr, header + 12, 4);

    // Octets 16-19
    memcpy(&info.dst_ip_addr, header + 16, 4);

    if (total_length_out) {
        *total_length_out = total_length;
    }
    if (info_out) {
        *info_out = info;
    }

    return true;
}

uint16_t internet_ipv4_header_checksum(size_t header_length, const char header[header_length]) {
    kassert(header_length % 2 == 0);
    size_t length_words = header_length / 2;

    uint32_t sum = 0;
    for (size_t i = 0; i < length_words; ++i) {
        net_u16 this_word_net;
        memcpy(&this_word_net, &header[2 * i], 2);
        uint16_t this_word = ntoh_u16(this_word_net);
        sum += this_word;
    }

    while (sum & 0xFFFF0000) {
        uint16_t overflow = sum >> 16;
        sum &= 0x0000FFFF;
        sum += overflow;
    }

    uint16_t checksum = sum;
    checksum = ~checksum;
    return checksum;
}

bool internet_ipv4_send_packet(
    const internet_ipv4_packet_info *info,
    netdev *ntdv, const link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    size_t header_length = internet_ipv4_needed_header_length(info);
    char *header = packet;

    if (!internet_ipv4_encode_header(info, packet_length, header_length, header)) {
        *errno_ptr = EINVAL;
        return false;
    }

    return link_send_frame(ntdv, dtlnk_info, packet_length, packet, frame_length, frame, errno_ptr);
}

void internet_ipv4_receive_packet(
    netdev *ntdv, const link_frame_info *dtlnk_info,
    size_t packet_length, char packet[packet_length]
) {
    uint16_t total_length;
    internet_ipv4_packet_info info;
    if (!network_ipv4_decode_header(packet_length, packet, &total_length, &info)) {
        return;
    }

    // TODO
    kdebug(
        "Recevied IPv4 packet with total length %zu from " INTERNET_IPV4_PRIIPV4 " to " INTERNET_IPV4_PRIIPV4,
        packet_length, INTERNET_IPV4_ARGIPV4(info.src_ip_addr), INTERNET_IPV4_ARGIPV4(info.dst_ip_addr)
    );
}

#endif
