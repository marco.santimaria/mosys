#include "network/link/link_ethernet.h"
#include "network/network_types.h"
#include "network/link/link.h"
#include "config.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include <assert.h>

#if NETWORK_LINK_ETHERNET_ENABLE

// TODO: This isn't constant
#define ETHERNET_HEADER_LENGTH 14

ssize_t link_ethernet_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
) {
    if (payload_length > SSIZE_MAX - ETHERNET_HEADER_LENGTH) {
        *errno_ptr = EINVAL;
        return -1;
    }
    if (payload_length > ntdv->mtu) {
        kpanic("Invalid packet length");
    }
    return payload_length + ETHERNET_HEADER_LENGTH;
}

size_t link_ethernet_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (frame_length < ETHERNET_HEADER_LENGTH) {
        *errno_ptr = EINVAL;
        return 0;
    }

    if (info->type != LINK_TYPE_ETHERNET) {
        kpanic("link_frame_info::type mismatch");
    }
    const link_ethernet_frame_info *info_ethernet = &info->ethernet;

    size_t payload_offset = ETHERNET_HEADER_LENGTH;
    kassert(payload = frame + payload_offset);

    memcpy(frame + 0, &info_ethernet->mac_dst, 6);
    memcpy(frame + 6, &info_ethernet->mac_src, 6);

    if (info_ethernet->type <= 1500) {
        // Don't support 802.3
        *errno_ptr = EINVAL;
        return 0;
    } else if (info_ethernet->type >= 1536) {
        net_u16 ethertype_net = hton_u16(info_ethernet->type);
        memcpy(frame + 12, &ethertype_net, 2);
    } else {
        *errno_ptr = EINVAL;
        return 0;
    }

    return frame_length;
}

bool link_ethernet_decode_frame(
    netdev *ntdv,
    size_t frame_length, char frame[frame_length],
    link_frame_info *info_out,
    size_t *payload_length_out, char **payload_out,
    int *errno_ptr
) {
    link_ethernet_frame_info info_ethernet;
    size_t payload_length;
    char *payload;

    if (frame_length < ETHERNET_HEADER_LENGTH) {
        *errno_ptr = EINVAL;
        return false;
    }

    memcpy(&info_ethernet.mac_dst, frame + 0, 6);
    memcpy(&info_ethernet.mac_src, frame + 6, 6);

    net_u16 type_or_length_net;
    memcpy(&type_or_length_net, frame + 12, 2);
    uint16_t type_or_length = ntoh_u16(type_or_length_net);
    if (type_or_length <= 1500) {
        if (type_or_length > frame_length - ETHERNET_HEADER_LENGTH) {
            *errno_ptr = EINVAL;
            return false;
        }
        // Don't support 802.3
        *errno_ptr = EINVAL;
        return false;
    } else if (type_or_length >= 1536) {
        payload_length = frame_length - ETHERNET_HEADER_LENGTH;
        info_ethernet.type = type_or_length;
    } else {
        *errno_ptr = EINVAL;
        return false;
    }

    payload = frame + ETHERNET_HEADER_LENGTH;

    if (info_out) {
        info_out->type = LINK_TYPE_ETHERNET;
        info_out->ethernet = info_ethernet;
    }
    if (payload_length_out) {
        *payload_length_out = payload_length;
    }
    if (payload_out) {
        *payload_out = payload;
    }

    return true;
}

internet_type link_ethernet_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
) {
    if (info->type != LINK_TYPE_ETHERNET) {
        kpanic("link_frame_info::type mismatch");
    }
    const link_ethernet_frame_info *info_ethernet = &info->ethernet;

    return internet_type_from_ethertype(info_ethernet->type);
}

#endif
