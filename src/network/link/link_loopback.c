#include "network/link/link_loopback.h"
#include "network/link/link.h"
#include "network/link/link_generic.h"
#include "uapi/errno.h"
#include "kio/klog.h"

#if NETWORK_LINK_LOOPBACK_ENABLE

const link_ops link_loopback_dtlnk_ops = {
    .needed_frame_length = link_loopback_needed_frame_length,
    .alloc_raw_frame = NULL,
    .alloc_frame = NULL,
    .dealloc_frame = NULL,
    .make_frame = NULL,
    .payload_in_frame = link_loopback_payload_in_frame,
    .encode_frame = link_loopback_encode_frame,
    .decode_frame = NULL,
    .send_encoded_frame = link_loopback_send_encoded_frame,
    .poll = NULL,
    .identify_network_type = link_loopback_identify_network_type,
};

ssize_t link_loopback_needed_frame_length(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length,
    int *errno_ptr
) {
    if (payload_length > SSIZE_MAX) {
        *errno_ptr = EINVAL;
        return -1;
    }
    return payload_length;
}
char *link_loopback_payload_in_frame(
    struct netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    size_t payload_length,
    int *errno_ptr
) {
    return frame;
}

size_t link_loopback_encode_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t payload_length, char payload[payload_length],
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    if (info->type != LINK_TYPE_LOOPBACK) {
        kpanic("link_frame_info::type mismatch");
    }

    return payload_length;
}

bool link_loopback_send_encoded_frame(
    netdev *ntdv, const link_frame_info *info,
    size_t frame_length, char frame[frame_length],
    int *errno_ptr
) {
    link_receive_decoded_frame(ntdv, frame_length, frame, info, frame_length, frame, errno_ptr);
    return true;
}

internet_type link_loopback_identify_network_type(
    struct netdev *ntdv, const link_frame_info *info
) {
    if (info->type != LINK_TYPE_LOOPBACK) {
        kpanic("link_frame_info::type mismatch");
    }
    const link_loopback_frame_info *info_loopback = &info->loopback;

    return internet_type_from_ethertype(info_loopback->type);
}

#endif
