#include "process/process.h"
#include "config.h"
#include "file/elf.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "mem/mem_mapping.h"
#include "uapi/errno.h"
#include "uapi/sys/mman.h"
#include "util/mutex.h"
#include "util/util.h"
#include "vfs/vfs.h"
#include <stdbool.h>
#include <string.h>

static mem_mapping *process_make_arg_env_space(flex_const_vect_mut argv, flex_const_vect_mut envp,
                                               int *new_argc_ptr, umem_ptr *new_argv_ptr, umem_ptr *new_envp_ptr,
                                               int *errno_ptr) {
    mem_mapping *args_map = NULL;

    flex_type argv_type = argv.type;
    flex_type envp_type = envp.type;

    flex_const argv_flx = flex_vect_measure(argv);
    flex_const envp_flx = flex_vect_measure(envp);

    flex_const_pinned argv_pin = flex_pin_must_cleanup(argv_flx);
    if (argv_pin.fault) {
        *errno_ptr = EFAULT;
        goto done_cleanup_none;
    }
    flex_const_pinned envp_pin = flex_pin_must_cleanup(envp_flx);
    if (envp_pin.fault) {
        *errno_ptr = EFAULT;
        goto done_cleanup_argv_pin;
    }

    const flex_ptr_mut *argv_ptr = argv_pin.ptr;
    size_t argv_max_count = argv_pin.size / sizeof(flex_ptr_mut);

    const flex_ptr_mut *envp_ptr = envp_pin.ptr;
    size_t envp_max_count = envp_pin.size / sizeof(flex_ptr_mut);

    size_t argv_str_count = 0;  // Does not include NULL
    if (argv_ptr) {
        for (size_t i = 0; i < argv_max_count; ++i) {
            if (flex_ptr_is_null(argv_ptr[i])) {
                argv_str_count = i;
            }
        }
    }
    // TODO: Going past max count will cause no arguments instead of failing
    size_t argv_len = (argv_str_count + 1) * sizeof(flex_ptr_mut);  // Includes NULL

    size_t envp_str_count = 0;  // Does not include NULL
    if (envp_ptr) {
        for (size_t i = 0; i < envp_max_count; ++i) {
            if (flex_ptr_is_null(envp_ptr[i])) {
                envp_str_count = i;
            }
        }
    }
    // TODO: Going past max count will cause no arguments instead of failing
    size_t envp_len = (envp_str_count + 1) * sizeof(flex_ptr_mut);  // Includes NULL

    size_t args_ptrs_len = argv_len + envp_len;

    // TODO: We currently duplicate a lot of work
    // TODO: Need to check at each step
    // TODO: Make sure there's not theoretical chance to overflow by passsing the same string over and over

    size_t argv_strs_len = 0;
    for (size_t i = 0; i < argv_str_count; ++i) {
        flex_ptr_mut this_str = argv_ptr[i];
        flex_mut_str this_str_flx = flex_mut_str_generic(argv_type, this_str);
        flex_mut this_flx = flex_str_measure(this_str_flx);
        argv_strs_len += this_flx.size;
    }

    size_t envp_strs_len = 0;
    for (size_t i = 0; i < envp_str_count; ++i) {
        flex_ptr_mut this_str = envp_ptr[i];
        flex_mut_str this_str_flx = flex_mut_str_generic(envp_type, this_str);
        flex_mut this_flx = flex_str_measure(this_str_flx);
        envp_strs_len += this_flx.size;
    }

    size_t needed_args_len = argv_len + envp_len + argv_strs_len + envp_strs_len;
    size_t needed_args_pages = round_up(needed_args_len, MEM_PAGE_SIZE) / MEM_PAGE_SIZE;
    if (needed_args_pages > PROCESS_ARGS_MAX_PAGES) {
        *errno_ptr = E2BIG;
        goto done_cleanup_both_pins;
    }

    args_map = mem_mapping_new(needed_args_pages * MEM_PAGE_SIZE);
    char *args_map_k_at = mem_mapping_k_at(args_map);
    UMEM_PTR(char) args_map_u_at = mem_mapping_u_at(args_map);

    flex_ptr_mut *args_ptrs = (flex_ptr_mut *) args_map_k_at;
    char *args_strs = args_map_k_at + args_ptrs_len;

    size_t args_ptrs_index = 0;
    size_t args_strs_index = 0;

    // Check that each string is actually NUL-terminated

    for (size_t i = 0; i < argv_str_count; ++i) {
        flex_ptr_mut this_str = argv_ptr[i];
        flex_mut_str this_str_flx = flex_mut_str_generic(argv_type, this_str);
        flex_mut this_flx = flex_str_measure(this_str_flx);
        flex_mut_pinned this_pin = flex_pin_must_cleanup(this_flx);
        if (this_pin.fault) {
            *errno_ptr = EFAULT;
            goto done_cleanup_args_map_and_both_pins;
        }

        memcpy(args_strs + args_strs_index, this_pin.ptr, this_pin.size);
        args_ptrs[args_ptrs_index++] = (flex_ptr_mut) { .user = args_map_u_at + args_ptrs_len + args_strs_index };
        args_strs_index += this_pin.size;

        flex_unpin(&this_pin);
    }
    args_ptrs[args_ptrs_index++] = FLEX_PTR_MUT_NULL;

    for (size_t i = 0; i < envp_str_count; ++i) {
        flex_ptr_mut this_str = envp_ptr[i];
        flex_mut_str this_str_flx = flex_mut_str_generic(envp_type, this_str);
        flex_mut this_flx = flex_str_measure(this_str_flx);
        flex_mut_pinned this_pin = flex_pin_must_cleanup(this_flx);
        if (this_pin.fault) {
            *errno_ptr = EFAULT;
            goto done_cleanup_args_map_and_both_pins;
        }

        memcpy(args_strs + args_strs_index, this_pin.ptr, this_pin.size);
        args_ptrs[args_ptrs_index++] = (flex_ptr_mut) { .user = args_map_u_at + args_ptrs_len + args_strs_index };
        args_strs_index += this_pin.size;

        flex_unpin(&this_pin);
    }
    args_ptrs[args_ptrs_index++] = FLEX_PTR_MUT_NULL;

    *new_argc_ptr = argv_str_count;
    *new_argv_ptr = args_map_u_at + 0;
    *new_envp_ptr = args_map_u_at + argv_len;

    goto done_cleanup_both_pins;

done_cleanup_args_map_and_both_pins:
    mem_mapping_delete(args_map);
    args_map = NULL;
    args_map_k_at = NULL;
    args_map_u_at = UMEM_NULL;
done_cleanup_both_pins:
    flex_unpin(&envp_pin);
done_cleanup_argv_pin:
    flex_unpin(&argv_pin);
done_cleanup_none:
    return args_map;
}

int process_execve_elf(process_execve_info *exec_info, int *errno_ptr) {
    bool elf_result = elf_load_file(exec_info->fd, &exec_info->space, &exec_info->entry, errno_ptr);

    // TODO: When rewriting ELF code, provide a specific failure reason
    if (!elf_result) {
        *errno_ptr = ENOMEM;
        goto failed_elf_load_file;
    } else if (!exec_info->entry) {
        *errno_ptr = ENOEXEC;
        goto failed_elf_entry;
    }

    // Copy argv and envp to mem_mapping at thread_current->owner_process->arg_env_space
    // TODO: This should probably just go on the stack
    mem_mapping *new_arg_env_space = process_make_arg_env_space(exec_info->argv, exec_info->envp, &exec_info->new_argc, &exec_info->new_argv, &exec_info->new_envp, errno_ptr);
    if (!new_arg_env_space) {
        goto failed_new_arg_env_space;
    }

    mem_mapping *new_usr_stack = mem_mapping_new(PROCESS_USR_STACK_SIZE);
    if (!new_usr_stack) {
        *errno_ptr = ENOMEM;
        goto failed_new_usr_stack;
    }

    // Assemble the rest of memory space
    mem_space_append(&exec_info->space, new_arg_env_space);
    mem_space_append(&exec_info->space, new_usr_stack);

    // Set other execution info
    exec_info->stack_ptr = mem_mapping_u_at(new_usr_stack) + new_usr_stack->size;

    return 0;

    mem_mapping_delete(new_usr_stack);
failed_new_usr_stack:
    mem_mapping_delete(new_arg_env_space);
failed_new_arg_env_space:
failed_elf_entry:
failed_elf_load_file:
    mem_space_release_maps_array(&exec_info->space);
    return -1;
}

int process_execve_enter(process_execve_info *exec_info, int *errno_ptr) {
    UNUSED(errno_ptr);

    // This is the point of no return
    mem_space_replace(&thread_current->owner_process->space, &exec_info->space);

    // Unblock waiting parent
    WAKELIST_WITH_LOCK(&thread_current->owner_process->vfork_wklst, {
        wakelist_wake_all(&thread_current->owner_process->vfork_wklst);
    })

    if (thread_current->user_state) {
        fill_zero(&thread_current->user_state->d);
        fill_zero(&thread_current->user_state->a);
        thread_current->user_state->a[0] = (uintptr_t) exec_info->new_argv;
        thread_current->user_state->a[1] = (uintptr_t) exec_info->new_envp;
        thread_current->user_state->usp = (uint32_t) exec_info->stack_ptr;
        thread_current->user_state->sr = 0x0000;
        thread_current->user_state->pc = (uint32_t) exec_info->entry;
        thread_current->user_state->format_vector_offset = 0;
        return exec_info->new_argc;
    } else {
        // Note that argv and envp are only valid when thread_current->user_state is non-NULL
        process_enter_usr(
            mem_mapping_k_at(thread_current->sup_stack) + thread_current->sup_stack->size,
            exec_info->stack_ptr,
            (process_entry) (uintptr_t) exec_info->entry,
            0x0000);
        kpanic("process_enter_usr() returned");
    }
}

int process_execve(process_execve_info *exec_info, int *errno_ptr) {
    if (exec_info->interp_depth > PROCESS_INTERP_LIMIT) {
        *errno_ptr = ELOOP;
        goto failed_interp_depth;
    }

    exec_info->fd= vfs_open(exec_info->pathname, O_RDONLY, 0, errno_ptr);
    if (exec_info->fd < 0) {
        // vfs_open() sets errno at errno_ptr
        goto failed_open;
    }

    char *magic_buf = kalloc(PROCESS_EXEC_MAGIC_SIZE);
    if (!magic_buf) {
        *errno_ptr = ENOMEM;
        goto failed_kalloc;
    }
    flex_mut magic_buf_flx = flex_mut_kernel(magic_buf, PROCESS_EXEC_MAGIC_SIZE);

    ssize_t read_size = vfs_pread(exec_info->fd, magic_buf_flx, PROCESS_EXEC_MAGIC_SIZE, 0, errno_ptr);
    if (read_size < 0) {
        // vfs_read() sets errno at errno_ptr
        goto failed_read;
    }

    int result;
    // TODO: Factor out signature check
    if (read_size >= 4 && memcmp(magic_buf + 0, "\177ELF", 4) == 0) {
        // TODO: Handle PT_INTERP
        result = process_execve_elf(exec_info, errno_ptr);
    } else if (read_size >= 2 && memcmp(magic_buf + 0, "#!", 2) == 0) {
        // TODO: Implement
        kwarn("#! interpreter not implmented");
        *errno_ptr = ENOEXEC;
        goto failed_format;
#if 0
        if (read_size >= 2) {
            // TODO: Using sscanf() would be better
            if (memcmp(magic_buf, "#!", 2) == 0) {
                if (interp_depth > 3) {
                    *errno_ptr = ELOOP;
                    goto failed_interpreter;
                }

                // Get length of interpreter line
                char *end_interp_line = strnchr(magic_buf, read_size, '\n');
                if (!end_interp_line) {
                    *errno_ptr = ENOEXEC;
                    goto failed_interpreter;
                }

                // Copy interpreter line to a buffer
                size_t interp_line_len = end_interp_line - magic_buf;
                char interp_line[interp_line_len + 1];
                memcpy(interp_line, magic_buf, interp_line_len);
                interp_line[interp_line_len] = '\0';

                kfree(magic_buf);
                magic_buf = NULL;

                char *interp_statement = &interp_line[2];
                // TODO: Use strtok or friends
                // TODO: See how spaces are handled in interpreter statements
                // char *interpreter = strtok(interp_statement, " \t");
                // char *interp_args = strtok(NULL, " \t");
                while (*interp_statement == ' ' || *interp_statement == '\t') {
                    interp_statement += 1;
                }
                char *space = strchr(interp_statement, ' ');
                if (space) {
                    *space = '\0';
                }
                char *interpreter = interp_statement;
                char *interp_args = space ? space + 1 : NULL;
                if (interp_args) {
                    while (*interp_args == ' ' || *interp_args == '\t') {
                        interp_args += 1;
                    }
                    if (strlen(interp_args) == 0) {
                        interp_args = NULL;
                    }
                }

                size_t interp_argv_len = argv_len + 2;
                char *interp_argv[interp_argv_len];
                interp_argv[0] = interpreter;
                if (interp_args) {
                    interp_argv[1] = interp_args;
                }
                if (argv) {
                    memcpy(&interp_argv[interp_args ? 2 : 1], argv, argv_len * sizeof(argv[0]));
                } else {
                    interp_argv[interp_args ? 2 : 1] = NULL;
                }

                return process_execve(interpreter, interp_argv,
                                    envp,
                                    interp_depth + 1,
                                    errno_ptr);
            }
        }
#endif
    } else {
        *errno_ptr = ENOEXEC;
        goto failed_format;
    }

    int close_status = vfs_close(exec_info->fd, errno_ptr);
    if (close_status < 0) {
        kpanic("Failed to close file descriptor in execve");
        // vfs_close() sets errno at errno_ptr
        goto failed_close;
    }

    kfree(magic_buf);
    magic_buf_flx = FLEX_MUT_NULL;

    if (result >= 0) {
        result = process_execve_enter(exec_info, errno_ptr);
    }

    return result;

    int errno_dummy;    // Used to call cleanup functions without modifying actual failure cause
failed_close:
    mem_space_release_maps_array(&exec_info->space);
    goto failed_do_free_magic_buff;
failed_format:
failed_read:
    vfs_close(exec_info->fd, &errno_dummy);
failed_do_free_magic_buff:
    kfree(magic_buf);
failed_open:
failed_kalloc:
failed_interp_depth:
    return -1;
}
