#include "reboot.h"
#include "reboot_arch.h"
#include "kio/klog.h"
#include "mem/flex.h"
#include "uapi/errno.h"
#include "uapi/mosys/reboot.h"
#include <stddef.h>

int reboot(int magic, int magic2, int cmd, flex_const_str arg, int *errno_ptr) {
    if ((unsigned int) magic != MOSYS_REBOOT_MAGIC1 || (unsigned int) magic2 != MOSYS_REBOOT_MAGIC2) {
        *errno_ptr = EINVAL;
        return -1;
    }

    flex_const arg_flx_str;
    flex_result arg_copy_result;
    char arg_buffer[256];

    switch ((unsigned int) cmd) {
    case MOSYS_REBOOT_CMD_HALT:
        reboot_halt();
        break;

    case MOSYS_REBOOT_CMD_POWER_OFF:
        reboot_power_off();
        break;

    case MOSYS_REBOOT_CMD_RESTART:
        reboot_restart(NULL);
        break;

    case MOSYS_REBOOT_CMD_RESTART2:
        arg_flx_str = flex_str_measure(arg);
        arg_copy_result = flex_from_const(arg_buffer, arg_flx_str, min(sizeof arg_buffer, arg_flx_str.size));
        if (arg_copy_result != FLEX_RESULT_SUCCESS) {
            return -EFAULT;
        }
        arg_buffer[sizeof arg_buffer - 1] = '\0';
        reboot_restart(arg_buffer);
        break;

    default:
        *errno_ptr = EINVAL;
        return -1;
    }

    unreachable();
}

noreturn void reboot_restart(const char *arg) {
    if (arg) {
        kinfo("Restarting system with command '%s'", arg);
    } else {
        kinfo("Restarting system");
    }

    reboot_arch_restart(arg);
}

noreturn void reboot_halt(void) {
    kinfo("System halted");

    reboot_arch_halt();
}

noreturn void reboot_power_off(void) {
    kinfo("Power down");

    reboot_arch_power_off();
}
