#include "socket/socket.h"
#include "socket/socket_local.h"
#include "socket/socket_ip.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "uapi/errno.h"
#include "uapi/sys/socket.h"
#include "util/util.h"
#include <string.h>

#if SOCKET_ENABLE

socket_info *socket_info_new(void) {
    socket_info *sock = kalloc(sizeof(socket_info));
    if (sock) {
        mutex_init(&sock->mtx);
        mutex_init(&sock->listen_backlog_mtx);
        sock->ref_count = 1;
    }

    return sock;
}

void socket_info_delete(socket_info *sock) {
    if (sock->ref_count != 0) {
        kpanic("Tried to delete still-referenced socket_info");
    }
    kfree(sock);
}

void socket_info_ref(socket_info *sock) {
    MUTEX_WITH(&sock->mtx) {
        if (sock->ref_count == SIZE_MAX) {
            kpanic("Overflowed socket_info ref_count");
        }
        sock->ref_count += 1;
    }
}

void socket_info_unref(socket_info *sock) {
    MUTEX_WITH(&sock->mtx, {
        if (sock->ref_count == 0) {
            kpanic("Underflowed socket_info ref_count");
        }
        sock->ref_count -= 1;

        if (sock->ref_count == 0) {
            socket_info_delete(sock);
            MUTEX_RETURN_NO_UNLOCK();   // Return without releasing mutex, it's been deleted
        }
    })
}

int socket_info_listen_backlog_create(socket_info *sock, int backlog, int *errno_ptr) {
    // TODO: Check/truncate backlog value. See documentation on existing implementations.

    int result;

    MUTEX_WITH(&sock->listen_backlog_mtx) {
        if (sock->listen_backlog || sock->listen_backlog_count) {
            // TODO: Properly handle this case
            kpanic("Already listening");
        }

        sock->listen_backlog = kalloc(backlog * sizeof(socket_info *));
        if (sock->listen_backlog) {
            sock->listen_backlog_count = backlog;
            result = 0;
        } else {
            *errno_ptr = ENOMEM;
            result = -1;
        }
    }

    return result;
}

int socket_info_listen_backlog_put(socket_info *sock, socket_info *connection_peer, int *errno_ptr) {
    int result = -1;

    MUTEX_WITH(&connection_peer->listen_backlog_mtx, {
        if (!connection_peer->listen_backlog || !connection_peer->listen_backlog_count) {
            *errno_ptr = ECONNREFUSED;
            MUTEX_LEAVE_UNLOCK();
        }

        for (size_t i = 0; i < connection_peer->listen_backlog_count; ++i) {
            if (!connection_peer->listen_backlog[i]) {
                socket_info_ref(sock);
                connection_peer->listen_backlog[i] = sock;
                result = 0;
                break;
            }
        }
        if (result != 0)
            *errno_ptr = ECONNREFUSED;
    })

    if (result == 0)
        semaphore_signal(&connection_peer->listen_backlog_sem);

    return result;
}

socket_info *socket_info_listen_backlog_get(socket_info *sock, int *errno_ptr) {
    MUTEX_WITH(&sock->listen_backlog_mtx) {
        if (!sock->listen_backlog || !sock->listen_backlog_count) {
            // TODO: Properly handle this case
            kpanic("Not listening");
        }
    }

    socket_info *result = NULL;
    if (semaphore_trywait(&sock->listen_backlog_sem)) {
        MUTEX_WITH(&sock->listen_backlog_mtx) {
            result = sock->listen_backlog[0];
            if (!result) {
                kpanic("Backlog shouldn't be empty");
            }

            // Advance all items in backlog
            for (size_t i = 0; i < sock->listen_backlog_count - 1; ++i) {
                sock->listen_backlog[i] = sock->listen_backlog[i + 1];
            }
            sock->listen_backlog[sock->listen_backlog_count - 1] = NULL;
        }
    } else {
        *errno_ptr = EWOULDBLOCK;
    }

    return result;
}

socket_info *socket_info_listen_backlog_get_blocking(socket_info *sock, int *errno_ptr) {
    MUTEX_WITH(&sock->listen_backlog_mtx, {
        if (!sock->listen_backlog || !sock->listen_backlog_count) {
            // TODO: Properly handle this case
            kpanic("Not listening");
        }
    })

    socket_info *result = NULL;
    semaphore_wait(&sock->listen_backlog_sem);

    MUTEX_WITH(&sock->listen_backlog_mtx) {
        result = sock->listen_backlog[0];
        if (!result) {
            kpanic("Backlog shouldn't be empty");
        }

        // Advance all items in backlog
        for (size_t i = 0; i < sock->listen_backlog_count - 1; ++i) {
            sock->listen_backlog[i] = sock->listen_backlog[i + 1];
        }
        sock->listen_backlog[sock->listen_backlog_count - 1] = NULL;
    }

    return result;
}

socket_info *socket_socket(int domain, int type, int protocol, int *errno_ptr) {
    socket_info *sock = socket_info_new();
    if (!sock) {
        *errno_ptr = ENOMEM;
        return NULL;
    }

    sock->domain = domain;
    sock->type = type;
    sock->protocol = protocol;

    int result;

    switch (domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_socket(sock, errno_ptr);
        break;
#endif

#if SOCKET_IP_ENABLE
    case AF_INET:
        result = socket_ip_socket(sock, errno_ptr);
        break;
#endif

    default:
        *errno_ptr = EINVAL;
        result = -1;
    }

    if (result < 0) {
        socket_info_unref(sock);
        sock = NULL;
    }

    return sock;
}

int socket_socketpair(int domain, int type, int protocol, socket_info *socks[2], int *errno_ptr) {
    socks[0] = socket_info_new();
    if (!socks[0]) {
        *errno_ptr = ENOMEM;
        return -1;
    }
    socks[1] = socket_info_new();
    if (!socks[1]) {
        socket_info_unref(socks[0]);
        *errno_ptr = ENOMEM;
        return -1;
    }

    socks[0]->domain = domain;
    socks[1]->domain = domain;
    socks[0]->type = type;
    socks[1]->type = type;
    socks[0]->protocol = protocol;
    socks[1]->protocol = protocol;

    int result;

    switch (domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_socketpair(socks, errno_ptr);
        break;
#endif

    default:
        *errno_ptr = EINVAL;
        result = -1;
    }

    if (result < 0) {
        kfree(socks[0]);
        kfree(socks[1]);
        socks[0] = NULL;
        socks[1] = NULL;
    } else {
        socket_info_ref(socks[0]);
        socket_info_ref(socks[1]);
        socks[0]->peer = socks[1];
        socks[1]->peer = socks[0];
    }

    return result;
}

int socket_close(socket_info *sock, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_close(sock, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

ssize_t socket_recvfrom(socket_info *sock, flex_mut buf, size_t len, int flags,
                        FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_recvfrom(sock, buf, len, flags, src_addr, addrlen, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

ssize_t socket_sendto(socket_info *sock, flex_const buf, size_t len, int flags,
                      FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_sendto(sock, buf, len, flags, dest_addr, addrlen, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

int socket_bind(socket_info *sock, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_bind(sock, addr, addrlen, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

int socket_accept(socket_info *sock, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_accept(sock, addr, addrlen, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

int socket_connect(socket_info *sock, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
    int result;

    switch (sock->domain) {

#if SOCKET_LOCAL_ENABLE
    case AF_LOCAL:
        result = socket_local_connect(sock, addr, addrlen, errno_ptr);
        break;
#endif

    default:
        kpanic("Unknown socket domain");
    }

    return result;
}

#endif
