#include "socket/socket_ip.h"
#include "kalloc.h"
#include "uapi/errno.h"
#include "uapi/netinet/in.h"

#if SOCKET_IP_ENABLE

int socket_ip_socket(socket_info *sock, int *errno_ptr) {
    int result = 0;

    if (result == 0) {
        switch (sock->protocol) {
        case 0:
            break;
        default:
            *errno_ptr = EINVAL;
            result = -1;
            break;
        }
    }

    if (result == 0) {
        switch (sock->type) {
        default:
            *errno_ptr = EINVAL;
            result = -1;
            break;
        }
    }

    return result;
}

#endif
