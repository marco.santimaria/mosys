#include "socket/socket_local.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "util/rwlock.h"
#include "util/util.h"
#include "uapi/errno.h"
#include "uapi/sys/socket.h"
#include "uapi/sys/un.h"
#include <string.h>

#if SOCKET_LOCAL_ENABLE

#define SOCKET_LOCAL_BUFFER_SIZE 1024

// TODO: I'm pretty sure this file can cause deadlocks

// TODO: Add pathname support

struct socket_local_bind {
    socket_info *sock;
    struct socket_local_bind *next;
};
static struct socket_local_bind *bound_list = NULL;
static rwlock bound_list_rwl = RWLOCK_INIT;

static struct socket_local_bind *bound_make(socket_info *sock);
static void bound_ummake(socket_info *sock);
static socket_info *bound_lookup_addr(const struct sockaddr_un *addr, socklen_t addrlen);

static socket_info *XXXX_something_XXXX(socket_info *sock, socket_info *connection_peer, int *errno_ptr);

static struct socket_local_bind *bound_make(socket_info *sock) {
    struct socket_local_bind *new_bind = kalloc(sizeof(*new_bind));
    if (new_bind) {
        socket_info_ref(sock);

        RWLOCK_WRITE_WITH(&bound_list_rwl, {
            new_bind->sock = sock;
            new_bind->next = bound_list;
            bound_list = new_bind;
        })
    }

    return new_bind;
}

static socket_info *bound_lookup_addr(const struct sockaddr_un *addr, socklen_t addrlen) {
    if (addrlen < sizeof(sa_family_t) || addrlen > sizeof(*addr)) {
        return NULL;
    }

    socket_info *result = NULL;

    RWLOCK_READ_WITH(&bound_list_rwl, {
        for (struct socket_local_bind *itr = bound_list; itr; itr = itr->next) {
            if (itr->sock->addrlen != addrlen) {
                RWLOCK_LEAVE_UNLOCK();
            }

            if (memcmp(&itr->sock->addr.local, addr, addrlen) == 0) {
                socket_info_ref(itr->sock);
                result = itr->sock;
                RWLOCK_LEAVE_NO_UNLOCK();
            }
        }
    })

    return result;
}

int socket_local_socket(socket_info *sock, int *errno_ptr) {
    int result = 0;

    if (sock->protocol != 0) {
        *errno_ptr = EINVAL;
        result = -1;
        goto done;
    }

    switch (sock->type) {
    case SOCK_STREAM:
        sock->local.stream.buffer = kalloc(SOCKET_LOCAL_BUFFER_SIZE);
        if (!sock->local.stream.buffer) {
            *errno_ptr = ENOMEM;
            result = -1;
        }
        sock->local.stream.size = SOCKET_LOCAL_BUFFER_SIZE;
        break;
    default:
        *errno_ptr = EINVAL;
        result = -1;
        break;
    }

    if (result != 0) {
        goto done;
    }

    sock->local.is_open = true;
    sock->addr.local.sun_family = AF_UNIX;
    sock->addrlen = sizeof(sock->addr.local.sun_family);

done:
    return result;
}

int socket_local_socketpair(socket_info *socks[2], int *errno_ptr) {
    if (socks[0]->domain    != socks[1]->domain     ||
        socks[0]->type      != socks[1]->type       ||
        socks[0]->protocol  != socks[1]->protocol) {
        kpanic("Expected matching sockets");
    }

    if (socket_local_socket(socks[0], errno_ptr) < 0) {
        return -1;
    }
    if (socket_local_socket(socks[1], errno_ptr) < 0) {
        socket_local_close(socks[0], errno_ptr);
        return -1;
    }

    return 0;
}

int socket_local_close(struct socket_info *sock, int *errno_ptr) {
    socket_info *peer;

    // TODO: NEED to unbind

    MUTEX_WITH(&sock->mtx) {
        sock->local.is_open = false;

        if (sock->local.stream.buffer) {
            kfree(sock->local.stream.buffer);
        }
        sock->local.stream.size = 0;

        peer = sock->peer;
        sock->peer = NULL;
    }

    if (peer) {
        MUTEX_WITH(&peer->mtx) {
            if (peer->peer != sock) {
                kpanic("Socket's peer's peer isn't that socket");
            }
            peer->peer = NULL;
            // TODO: Does something else need to be done?
        }
        socket_info_unref(sock);
        socket_info_unref(peer);
    }

    return 0;
}

int socket_local_bind(socket_info *sock, FLEX_CONST(struct sockaddr_un) addr, socklen_t addrlen, int *errno_ptr) {
    if (addrlen < sizeof(sa_family_t) || addrlen > sizeof(struct sockaddr_un)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    flex_const_pinned addr_pin = flex_pin(addr);
    const struct sockaddr_un *addr_ptr = addr_pin.ptr;

    if (addr_ptr->sun_family != AF_LOCAL) {
        *errno_ptr = EINVAL;
        return -1;
    }

    socket_info *existing_sock = bound_lookup_addr(addr_ptr, addrlen);
    if (existing_sock) {
        socket_info_unref(existing_sock);
        *errno_ptr = EADDRINUSE;
        return -1;
    }

    memcpy(&sock->addr.local, addr_ptr, addrlen);
    sock->addrlen = addrlen;

    // TODO: Make this atomic in the lock, to avoid a bind happening between lookup and make causing a double-bind on an address

    if (!bound_make(sock)) {
        *errno_ptr = ENOMEM;
        return -1;
    }

    return 0;
}

ssize_t socket_local_recvfrom(socket_info *sock, flex_mut buf, size_t len, int flags,
                              FLEX_MUT(struct sockaddr_un) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    mutex_lock(&sock->mtx);

    if (!sock->local.is_open) {
        kpanic("Expected open socket");
    }

    if (sock->type == SOCK_STREAM) {
        socket_info *peer = sock->peer;

        // SOCK_STREAM doesn't use src_addr and addrlen
        if (!flex_is_null(src_addr) || !flex_is_null(addrlen)) {
            *errno_ptr = EISCONN;
            return -1;
        }

        if (!peer) {
            *errno_ptr = ENOTCONN;
            return -1;
        }

        struct local_stream_info *info = &sock->local.stream;

        size_t used_size;
        if (info->tail >= info->head) {
            used_size = info->tail - info->head;
        } else {
            used_size = info->size - info->head + info->tail;
        }

        len = min(len, used_size);

        if (len > 0) {
            size_t count_before_end = min(len, info->size - info->head);
            if (count_before_end > 0) {
                if (FLEX_RESULT_SUCCESS != flex_to(buf, &info->buffer[info->head], count_before_end)) {
                    // TODO:
                }
                buf = flex_add(buf, count_before_end);
                info->head = (info->head + count_before_end) % info->size;
            }

            size_t count_from_start = len - count_before_end;
            if (count_from_start > 0) {
                if (FLEX_RESULT_SUCCESS != flex_to(buf, &info->buffer[info->head], count_before_end)) {
                    // TODO:
                }
                buf = flex_add(buf, count_from_start);
                info->head = (info->head + count_from_start) % info->size;
            }
        }

        mutex_unlock(&sock->mtx);

        return len;
    } else {
        kpanic("Unknown local socket type");
    }
}

// TODO: Block instead of truncating, unless non-blocking
ssize_t socket_local_sendto(socket_info *sock, flex_const buf, size_t len, int flags,
                            FLEX_CONST(struct sockaddr_un) dest_addr, socklen_t addrlen, int *errno_ptr) {
    mutex_lock(&sock->mtx);

    if (!sock->local.is_open) {
        kpanic("Expected open socket");
    }

    if (sock->type == SOCK_STREAM) {
        socket_info *peer = sock->peer;

        mutex_unlock(&sock->mtx);

        // SOCK_STREAM doesn't use dest_addr and addrlen
        if (!flex_is_null(dest_addr) || addrlen) {
            *errno_ptr = EISCONN;
            return -1;
        }

        if (!peer) {
            *errno_ptr = ENOTCONN;
            return -1;
        }

        mutex_lock(&peer->mtx);

        if (peer->type != sock->type) {
            kpanic("Peer type mismatch");
        }
        if (!peer->local.is_open) {
            *errno_ptr = EPIPE;
            // Create SIGPIPE
            return -1;
        }

        struct local_stream_info *info = &peer->local.stream;

        size_t used_size;
        if (info->tail >= info->head) {
            used_size = info->tail - info->head;
        } else {
            used_size = info->size - info->head + info->tail;
        }
        size_t free_size = info->size - used_size;

        len = min(len, free_size);

        if (len > 0) {
            size_t count_before_end = min(len, info->size - info->tail);
            if (count_before_end > 0) {
                if (FLEX_RESULT_SUCCESS != flex_from(&info->buffer[info->tail], buf, count_before_end)) {
                    // TODO
                }
                buf = flex_add(buf, count_before_end);
                info->tail = (info->tail + count_before_end) % info->size;
            }

            size_t count_from_start = len - count_before_end;
            if (count_from_start > 0) {
                if (FLEX_RESULT_SUCCESS != flex_from(&info->buffer[info->tail], buf, count_before_end)) {
                    // TODO
                }
                buf = flex_add(buf, count_from_start);
                info->tail = (info->tail + count_from_start) % info->size;
            }
        }

        mutex_unlock(&peer->mtx);

        return len;
    } else {
        kpanic("Unknown local socket type");
    }
}

// TODO: This needs to handle a NULL addr
// TODO: This is suuuper broken
int socket_local_accept(socket_info *sock, FLEX_MUT(struct sockaddr_un) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    flex_mut_pinned addrlen_pin = flex_pin(addrlen);
    socklen_t *addrlen_ptr = addrlen_pin.ptr;

    if (*addrlen_ptr < sizeof(sa_family_t)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    flex_mut_pinned addr_pin = flex_pin(addr);
    struct sockaddr_un *addr_ptr = addr_pin.ptr;

    if (addr_ptr->sun_family != AF_LOCAL) {
        *errno_ptr = EAFNOSUPPORT;
        return -1;
    }

    mutex_lock(&sock->mtx);

    if (!sock->local.is_open) {
        kpanic("Expected open socket");
    }

    if (sock->type == SOCK_STREAM) {
        if (!sock->listen_backlog) {
            *errno_ptr = EINVAL;
            return -1;
        }

        socket_info *current_peer = sock->peer;
        if (current_peer) {
            *errno_ptr = EISCONN;
            mutex_unlock(&sock->mtx);
            return -1;
        }

        socket_info *connection_peer = bound_lookup_addr(addr_ptr, *addrlen_ptr);
        if (!connection_peer) {
            *errno_ptr = ECONNREFUSED;
            return -1;
        }

        mutex_lock(&connection_peer->mtx);

        if (connection_peer->type != SOCK_STREAM) {
            mutex_unlock(&connection_peer->mtx);
            socket_info_unref(connection_peer);
            connection_peer = NULL;

            mutex_unlock(&sock->mtx);

            *errno_ptr = EPROTOTYPE;
            return -1;
        }

        if (!connection_peer->listen_backlog) {
            mutex_unlock(&connection_peer->mtx);
            socket_info_unref(connection_peer);
            connection_peer = NULL;

            mutex_unlock(&sock->mtx);

            *errno_ptr = ECONNREFUSED;
            return -1;
        }

        // May block in XXXX_something_XXXX() unless nonblocking
        // On entry sock and connection_peer are both locked
        socket_info *data_peer = XXXX_something_XXXX(sock, connection_peer, errno_ptr);
        mutex_unlock(&connection_peer->mtx);
        socket_info_unref(connection_peer);
        connection_peer = NULL;
        if (!data_peer) {
            mutex_unlock(&sock->mtx);

            // Call to XXXX_something_XXXX() should set *errno_ptr
            return -1;
        }

        MUTEX_WITH(&data_peer->mtx) {
            sock->peer = data_peer;
            memcpy(&sock->addr.local, &data_peer->addr, data_peer->addrlen);
            sock->peer_addrlen = data_peer->addrlen;

            data_peer->peer = sock;
            memcpy(&data_peer->addr.local, &sock->addr, sock->addrlen);
            data_peer->peer_addrlen = sock->addrlen;
        }

        mutex_unlock(&sock->mtx);

        return 0;
    } else {
        kpanic("Unknown local socket type");
    }
}

static socket_info *XXXX_something_XXXX(socket_info *sock, socket_info *connection_peer, int *errno_ptr) {
    // TODO
    return NULL;
}

int socket_local_connect(socket_info *sock, FLEX_CONST(struct sockaddr_un) addr, socklen_t addrlen, int *errno_ptr) {
    if (addrlen < sizeof(sa_family_t) || addrlen > sizeof(struct sockaddr_un)) {
        *errno_ptr = EINVAL;
        return -1;
    }

    flex_const_pinned addr_pin = flex_pin(addr);
    const struct sockaddr_un *addr_ptr = addr_pin.ptr;

    if (addr_ptr->sun_family != AF_LOCAL) {
        *errno_ptr = EAFNOSUPPORT;
        return -1;
    }

    mutex_lock(&sock->mtx);

    if (!sock->local.is_open) {
        kpanic("Expected open socket");
    }

    if (sock->type == SOCK_STREAM) {
        socket_info *current_peer = sock->peer;
        if (current_peer) {
            *errno_ptr = EISCONN;
            mutex_unlock(&sock->mtx);
            return -1;
        }

        socket_info *connection_peer = bound_lookup_addr(addr_ptr, addrlen);
        if (!connection_peer) {
            *errno_ptr = ECONNREFUSED;
            return -1;
        }

        mutex_lock(&connection_peer->mtx);

        if (connection_peer->type != SOCK_STREAM) {
            mutex_unlock(&connection_peer->mtx);
            socket_info_unref(connection_peer);
            connection_peer = NULL;

            mutex_unlock(&sock->mtx);

            *errno_ptr = EPROTOTYPE;
            return -1;
        }

        if (!connection_peer->listen_backlog) {
            mutex_unlock(&connection_peer->mtx);
            socket_info_unref(connection_peer);
            connection_peer = NULL;

            mutex_unlock(&sock->mtx);

            *errno_ptr = ECONNREFUSED;
            return -1;
        }

        // May block in XXXX_something_XXXX() unless nonblocking
        // On entry sock and connection_peer are both locked
        socket_info *data_peer = XXXX_something_XXXX(sock, connection_peer, errno_ptr);
        mutex_unlock(&connection_peer->mtx);
        socket_info_unref(connection_peer);
        connection_peer = NULL;
        if (!data_peer) {
            mutex_unlock(&sock->mtx);

            // Call to XXXX_something_XXXX() should set *errno_ptr
            return -1;
        }

        MUTEX_WITH(&data_peer->mtx) {
            sock->peer = data_peer;
            memcpy(&sock->addr.local, &data_peer->addr, data_peer->addrlen);
            sock->peer_addrlen = data_peer->addrlen;

            data_peer->peer = sock;
            memcpy(&data_peer->addr.local, &sock->addr, sock->addrlen);
            data_peer->peer_addrlen = sock->addrlen;
        }

        mutex_unlock(&sock->mtx);

        return 0;
    } else {
        kpanic("Unknown local socket type");
    }
}

#endif
