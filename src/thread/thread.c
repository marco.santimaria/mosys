#include "thread/thread.h"
#include "kalloc.h"
#include "kio/klog.h"
#include "led.h"
#include "mem/mem_mapping.h"
#include "mem/ownedmem.h"
#include "timer.h"
#include "thread/timing.h"
#include "thread/thread_list_scheduler.h"
#include "util/util.h"
#include "util/wakelist.h"
#include <stdbool.h>
#include <string.h>

thread *thread_idle = NULL;
thread *thread_current = NULL;

thread_list thread_ready_list = THREAD_LIST_INIT;

static bool thread_is_init = false;

// TODO: For SMP, these can't be static/global
static size_t irq_disable_counter = 0;
static size_t thread_switch_postpone_counter = 0;
static bool thread_switch_postponed_flag = false;

void thread_init(void) {
    thread_idle = thread_new();
    if (!thread_idle) {
        kpanic("Failed to create initial thread");
    }

    thread_current = thread_idle;
    thread_current->state = THREAD_STATE_ACTIVE;

    thread_is_init = true;
}

thread *thread_new(void) {
    return kalloc(sizeof(thread));
}

void thread_delete(thread *this) {
    if (this == thread_current) {
        kpanic("Tried to delete current thread");
    }
    kfree(this);
}

// This happens in timer tick interrupt context
void thread_system_tick(long actual_interval_ns) {
    timing_uptime_tick_us(actual_interval_ns / 1000);
    led_toggle(LED_RED);
    if (thread_is_init) {
        timing_uptime_wake_sleeping();
        thread_schedule();
    }
}

void thread_setup(thread *this, void (*func)(), struct mem_mapping *sup_stack) {
    this->sup_stack = sup_stack;
    this->ssp = mem_mapping_k_at(sup_stack) + sup_stack->size;

    thread_setup_stack(this, func);
}

noreturn void thread_idle_loop(void) {
    while (1) {
        thread_scheduler_lock();
        thread_schedule();
        thread_scheduler_unlock();
    }
}

inline void thread_disable_irq(void) {
    // TODO: A global disable counter doesn't work for SMP
    __asm__ ("ori #(7 << 8), %sr");
    irq_disable_counter++;
    kassert(irq_disable_counter != 0);
}

inline void thread_undisable_irq(void) {
    // TODO: A global disable counter doesn't work for SMP
    kassert(irq_disable_counter > 0);
    if (--irq_disable_counter == 0) {
        kassert(thread_switch_postpone_counter == 0);
        __asm__ ("andi #~(7 << 8), %sr");
    }
}

inline void thread_scheduler_lock(void) {
    // TODO: SMP requires spinlock
    thread_disable_irq();
}

inline void thread_scheduler_unlock(void) {
    // TODO: SMP requires spinlock
    thread_undisable_irq();
}

void thread_enter_critical(void) {
    thread_scheduler_lock();
    thread_switch_postpone_counter++;
}

void thread_leave_critical(void) {
    if (--thread_switch_postpone_counter == 0) {
        if (thread_switch_postponed_flag) {
            thread_switch_postponed_flag = false;
            thread_schedule();
        }
    }
    thread_scheduler_unlock();
}

bool thread_inited(void) {
    thread_scheduler_lock();
    bool result = thread_is_init;
    thread_scheduler_unlock();
    return result;
}

void thread_assert_init(void) {
    thread_scheduler_lock();
    if (!thread_is_init) {
        kpanic("Threading is not initialized but was needed");
    }
    thread_scheduler_unlock();
}

// This must be called with scheduler locked
void thread_schedule(void) {
    if (thread_switch_postpone_counter > 0) {
        thread_switch_postponed_flag = true;
        return;
    }

    thread *next_thread = thread_list_scheduler_dequeue(&thread_ready_list);

    if (next_thread) {
        if (next_thread == thread_idle) {
            // Next thread is the idle thread, try to find an alternative
            thread *non_idle_next_thread = thread_list_scheduler_dequeue(&thread_ready_list);
            if (non_idle_next_thread) {
                // Found another thread to run, re-enqueue idle thread and switch to other instead
                thread_list_scheduler_enqueue(&thread_ready_list, next_thread);
                next_thread = non_idle_next_thread;
            } else if (thread_current->state == THREAD_STATE_ACTIVE) {
                // This thread can keep running
                thread_list_scheduler_enqueue(&thread_ready_list, next_thread);
                return;
            }
        }
        // Switch to dequeued thread
        thread_switch(next_thread);
    } else if (thread_current->state == THREAD_STATE_ACTIVE) {
        return;
    } else {
        kpanic("No thread available to run");
    }
}

// Call with scheduler locked
void thread_terminate(void) {
    thread_release_resources();

    // Only detached threads can be terminated
    kassert(!thread_current->owner_process);

    thread_current->state = THREAD_STATE_TERMINATED;
    thread_schedule();
    thread_scheduler_unlock();

    // TODO: Make sure we can get properly cleaned up

    kpanic("Terminated thread continued");
}

void thread_release_resources(void) {
    // Don't release sup_stack, we're still running in it

    // Currently a no-op
}

// Only to be used by thread_switch
void thread_on_start(void) {
    thread_scheduler_unlock();
}

// Only to be used by thread_switch
void thread_pre_switch(void) {
    // Make current thread be ready if it's running
    if (thread_current->state == THREAD_STATE_ACTIVE) {
        thread_list_scheduler_enqueue(&thread_ready_list, thread_current);
    }
}

// Only to be used by thread_switch
void thread_post_switch(void) {
    // Make current thread be running
    thread_current->state = THREAD_STATE_ACTIVE;
    led_toggle(LED_GREEN);
}

bool thread_wake(thread *this) {
    thread_scheduler_lock();
    bool can_wake = (this->state == THREAD_STATE_SLEEPING);
    if (can_wake) {
        this->state = THREAD_STATE_ACTIVE;
        thread_list_scheduler_enqueue(&thread_ready_list, this);
    }
    thread_scheduler_unlock();
    return can_wake;
}
