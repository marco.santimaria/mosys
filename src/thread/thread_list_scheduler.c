#include "thread/thread_list_scheduler.h"
#include "thread/thread.h"

void thread_list_scheduler_enqueue(thread_list *list, thread *enq_thread) {
    SPINLOCK_WITH(&list->lock, {
        list_push_back(&list->list, enq_thread, scheduler_link);
    })
}

thread *thread_list_scheduler_dequeue(thread_list *list) {
    thread *deq_thread;
    SPINLOCK_WITH(&list->lock, {
        deq_thread = list_pop_front(&list->list, thread, scheduler_link);
    })
    return deq_thread;
}
