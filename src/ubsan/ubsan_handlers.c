#include "ubsan/ubsan_handlers.h"
#include "kio/klog.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#ifndef PRIdMAX
#define PRIdMAX "lld"
#endif
#ifndef PRIuMAX
#define PRIuMAX "llu"
#endif
#ifndef PRIdPTR
#define PRIdPTR "ld"
#endif
#ifndef PRIuPTR
#define PRIuPTR "lu"
#endif
#ifndef PRIfMAX
#define PRIfMAX "Lf"
#endif

#define UBSAN_UNRECOVERABLE_IMPL(checkname, def_args, ...)                  \
    noreturn void __ubsan_handle_ ## checkname def_args {                   \
        ubsan_handle_ ## checkname ## _impl(__VA_ARGS__, KLOG_LEVEL_PANIC); \
        kpanic_blocker();                                                   \
    }

#define UBSAN_RECOVERABLE_IMPL(checkname, def_args, ...)                    \
    void __ubsan_handle_ ## checkname def_args {                            \
        ubsan_handle_ ## checkname ## _impl(__VA_ARGS__, KLOG_LEVEL_WARN);  \
    }                      \
    noreturn void __ubsan_handle_ ## checkname ## _abort def_args {         \
        ubsan_handle_ ## checkname ## _impl(__VA_ARGS__, KLOG_LEVEL_PANIC); \
        kpanic_blocker();                                                   \
    }

#define UBSAN_OVERFLOW_HANDLER_IMPL(opname, op)                                                             \
    void ubsan_handle_ ## opname ## _overflow_impl(                                                         \
        ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs, enum klog_level level) { \
        ubsan_handle_integer_overflow_impl(data, lhs, op, rhs, level);                                      \
    }

#define UBSAN_OVERFLOW_UNRECOVERABLE(opname)            \
UBSAN_UNRECOVERABLE_IMPL(                               \
    opname ## _overflow,                                \
    (                                                   \
        ubsan_overflow_data *data,                      \
        ubsan_value_handle lhs, ubsan_value_handle rhs  \
    ),                                                  \
    data, lhs, rhs                                      \
)

#define UBSAN_OVERFLOW_RECOVERABLE(opname)              \
UBSAN_RECOVERABLE_IMPL(                                 \
    opname ## _overflow,                                \
    (                                                   \
        ubsan_overflow_data *data,                      \
        ubsan_value_handle lhs, ubsan_value_handle rhs  \
    ),                                                  \
    data, lhs, rhs                                      \
)

#define UBSAN_OVERFLOW_HANDLER_UNRECOVERABLE(opname, op)    \
    UBSAN_OVERFLOW_HANDLER_IMPL(opname, op)                 \
    UBSAN_OVERFLOW_UNRECOVERABLE(opname)

#define UBSAN_OVERFLOW_HANDLER_RECOVERABLE(opname, op)  \
    UBSAN_OVERFLOW_HANDLER_IMPL(opname, op)             \
    UBSAN_OVERFLOW_RECOVERABLE(opname)

typedef enum ubsan_type_check_kind {
    UBSAN_TCK_LOAD,
    UBSAN_TCK_STORE,
    UBSAN_TCK_REFERENCE_BINDING,
    UBSAN_TCK_MEMBER_ACCESS,
    UBSAN_TCK_MEMBER_CALL,
    UBSAN_TCK_CONSTRUCTOR_CALL,
    UBSAN_TCK_DOWNCAST_POINTER,
    UBSAN_TCK_DOWNCAST_REFERENCE,
    UBSAN_TCK_UPCAST,
    UBSAN_TCK_UPCAST_TO_VIRTUALBASE,
    UBSAN_TCK_NONNULL_ASSIGN,
    UBSAN_TCK_DYNAMIC_OPERATION,
} ubsan_type_check_kind;

const char *ubsan_type_check_kinds[] = {
    "load of",
    "store to",
    "reference binding to",
    "member access within",
    "member call on",
    "constructor call on",
    "downcast of",
    "downcast of",
    "upcast of",
    "cast to virtual base of",
    "_Nonnull binding to",
    "dynamic operation on",
};

static void ubsan_handle_type_mismatch_v1_impl(
    ubsan_type_mismatch_data *data,
    ubsan_value_handle pointer,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    uintptr_t alignment = (uintptr_t) 1 << data->log_alignment;

    if (!pointer) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "%s null pointer of type %s",
            ubsan_type_check_kinds[data->type_check_kind],
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else if (pointer & (alignment - 1)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "%s misaligned address %p for type %s, which requires %" PRIuPTR " byte alignment",
            ubsan_type_check_kinds[data->type_check_kind],
            (void *) pointer,
            ubsan_type_descriptor_get_type_name(data->type),
            alignment
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "%s address %p with insufficient space for an object of type %s",
            ubsan_type_check_kinds[data->type_check_kind],
            (void *) pointer,
            ubsan_type_descriptor_get_type_name(data->type)
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    type_mismatch_v1,
    (
        ubsan_type_mismatch_data *data,
        ubsan_value_handle pointer
    ),
    data, pointer
)

static void ubsan_handle_alignment_assumption_impl(
    ubsan_alignment_assumption_data *data,
    ubsan_value_handle pointer, ubsan_value_handle alignment, ubsan_value_handle offset,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    ubsan_source_location assumption_loc = ubsan_source_location_acquire(&data->assumption_loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);
    assert(strlen(assumption_loc.filename) >= INFO_SOURCE_PATH_SIZE);

    uintptr_t real_pointer = pointer - offset;
    uintptr_t lsb = 0;  // TODO: CORRECT THIS
    uintptr_t actual_alignment = (uintptr_t) 1 << lsb;

    uintptr_t mask = alignment - 1;
    uintptr_t misalignment_offset = real_pointer & mask;

    if (!offset) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "assumption of %" PRIuPTR " byte alignment for pointer of type %s failed",
            (uintptr_t) alignment,
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "assumption of %" PRIuPTR " byte alignment (with offset of %" PRIuPTR " byte) for pointer of type %s failed",
            (uintptr_t) alignment,
            (uintptr_t) offset,
            ubsan_type_descriptor_get_type_name(data->type)
        );
    }

    if (!ubsan_source_location_is_invalid(&assumption_loc)) {
        klog_func(
            level, true, assumption_loc.filename + INFO_SOURCE_PATH_SIZE, assumption_loc.line,
            "alignment assumption was specified here"
        );
    }

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "%saddress is %" PRIuPTR " aligned, misalignment offset is %" PRIuPTR " bytes",
        (offset ? "offset " : ""), actual_alignment, misalignment_offset
    );
}
UBSAN_RECOVERABLE_IMPL(
    alignment_assumption,
    (
        ubsan_alignment_assumption_data *data,
        ubsan_value_handle pointer, ubsan_value_handle alignment, ubsan_value_handle offset
    ),
    data, pointer, alignment, offset
)

static void ubsan_handle_integer_overflow_impl(
    ubsan_overflow_data *data,
    ubsan_value_handle lhs, const char *operator, ubsan_value_handle rhs,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);
    bool is_signed = ubsan_type_descriptor_is_signed_integer_ty(data->type);

    ubsan_value lhs_value = { data->type, lhs };
    ubsan_value rhs_value = { data->type, rhs };

    if (is_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "signed integer overflow: %" PRIdMAX " %s %" PRIdMAX " cannot be represented in type %s",
            ubsan_value_get_s_int_value(&lhs_value),
            operator,
            ubsan_value_get_s_int_value(&rhs_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "unsigned integer overflow: %" PRIuMAX " %s %" PRIuMAX " cannot be represented in type %s",
            ubsan_value_get_u_int_value(&lhs_value),
            operator,
            ubsan_value_get_u_int_value(&rhs_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    }
}
UBSAN_OVERFLOW_HANDLER_RECOVERABLE(add, "+")
UBSAN_OVERFLOW_HANDLER_RECOVERABLE(sub, "-")
UBSAN_OVERFLOW_HANDLER_RECOVERABLE(mul, "*")

static void ubsan_handle_negate_overflow_impl(
    ubsan_overflow_data *data,
    ubsan_value_handle old_val,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);
    bool is_signed = ubsan_type_descriptor_is_signed_integer_ty(data->type);

    ubsan_value old_val_value = { data->type, old_val };

    if (is_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "negation of %" PRIdMAX " cannot be represented in type %s; cast to an unsigned type to negate this value to itself",
            ubsan_value_get_s_int_value(&old_val_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "negation of %" PRIuMAX " cannot be represented in type %s",
            ubsan_value_get_u_int_value(&old_val_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    negate_overflow,
    (
        ubsan_overflow_data *data,
        ubsan_value_handle old_val
    ),
    data, old_val
)

static void ubsan_handle_divrem_overflow_impl(
    ubsan_overflow_data *data,
    ubsan_value_handle lhs, ubsan_value_handle rhs,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    ubsan_value lhs_value = { data->type, lhs };
    ubsan_value rhs_value = { data->type, rhs };

    if (ubsan_value_is_minus_one(&rhs_value)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "division of %" PRIdMAX " by -1 cannot be represented in type %s",
            ubsan_value_get_s_int_value(&lhs_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "division by zero"
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    divrem_overflow,
    (
        ubsan_overflow_data *data,
        ubsan_value_handle lhs, ubsan_value_handle rhs
    ),
    data, lhs, rhs
)

static void ubsan_handle_shift_out_of_bounds_impl(
    ubsan_shift_out_of_bounds_data *data,
    ubsan_value_handle lhs, ubsan_value_handle rhs,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    ubsan_value lhs_value = { data->lhs_type, lhs };
    ubsan_value rhs_value = { data->rhs_type, rhs };

    if (ubsan_value_is_negative(&rhs_value)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "shift exponent %" PRIdMAX " is negative",
            ubsan_value_get_s_int_value(&rhs_value)
        );
    } else if (ubsan_value_get_positive_int_value(&rhs_value) >= ubsan_type_descriptor_get_integer_bit_width(data->lhs_type)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "shift exponent %" PRIuMAX " is too large for %u-bit type %s",
            ubsan_value_get_positive_int_value(&rhs_value),
            ubsan_type_descriptor_get_integer_bit_width(data->lhs_type),
            ubsan_type_descriptor_get_type_name(data->lhs_type)
        );
    } else if (ubsan_value_is_negative(&lhs_value)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "left shift of negative value %" PRIdMAX "",
            ubsan_value_get_s_int_value(&lhs_value)
        );
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "left shift of %" PRIuMAX " by %" PRIuMAX " places cannot be represented in type %s",
            ubsan_value_get_positive_int_value(&lhs_value),
            ubsan_value_get_positive_int_value(&rhs_value),
            ubsan_type_descriptor_get_type_name(data->lhs_type)
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    shift_out_of_bounds,
    (
        ubsan_shift_out_of_bounds_data *data,
        ubsan_value_handle lhs, ubsan_value_handle rhs
    ),
    data, lhs, rhs
)

static void ubsan_handle_out_of_bounds_impl(
    ubsan_out_of_bounds_data *data,
    ubsan_value_handle index,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    ubsan_value index_value = { data->index_type, index };

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "index %" PRIuMAX " out of bounds for type %s",
        ubsan_value_get_positive_int_value(&index_value),
        ubsan_type_descriptor_get_type_name(data->index_type)
    );
}
UBSAN_RECOVERABLE_IMPL(
    out_of_bounds,
    (
        ubsan_out_of_bounds_data *data,
        ubsan_value_handle index
    ),
    data, index
)

static void ubsan_handle_builtin_unreachable_impl(
    ubsan_unreachable_data *data,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "execution reached an unreachable program point"
    );
}
UBSAN_UNRECOVERABLE_IMPL(
    builtin_unreachable,
    (
        ubsan_unreachable_data *data
    ),
    data
)

static void ubsan_handle_missing_return_impl(
    ubsan_unreachable_data *data,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "execution reached the end of a value-returning function without returning a value"
    );
}
UBSAN_UNRECOVERABLE_IMPL(
    missing_return,
    (
        ubsan_unreachable_data *data
    ),
    data
)

static void ubsan_handle_vla_bound_not_positive_impl(
    ubsan_vla_bound_data *data,
    ubsan_value_handle bound,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    ubsan_value bound_value = { data->type, bound };

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "variable length array bound evaluates to non-positive value %" PRIdMAX "",
        ubsan_value_get_s_int_value(&bound_value)
    );
}
UBSAN_RECOVERABLE_IMPL(
    vla_bound_not_positive,
    (
        ubsan_vla_bound_data *data,
        ubsan_value_handle bound
    ),
    data, bound
)

// TODO: __ubsan_handle_float_cast_overflow

static void ubsan_handle_load_invalid_value_impl(
    ubsan_invalid_value_data *data,
    ubsan_value_handle val,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    ubsan_value val_value = { data->type, val };

    if (ubsan_type_descriptor_is_signed_integer_ty(data->type)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "load of value %" PRIdMAX ", which is not a valid value for type %s",
            ubsan_value_get_s_int_value(&val_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else if (ubsan_type_descriptor_is_unsigned_integer_ty(data->type)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "load of value %" PRIuMAX ", which is not a valid value for type %s",
            ubsan_value_get_u_int_value(&val_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    } else if (ubsan_type_descriptor_is_float_ty(data->type)) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "load of value %" PRIfMAX ", which is not a valid value for type %s",
            ubsan_value_get_float_value(&val_value),
            ubsan_type_descriptor_get_type_name(data->type)
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    load_invalid_value,
    (
        ubsan_invalid_value_data *data,
        ubsan_value_handle val
    ),
    data, val
)

static void ubsan_handle_implicit_conversion_impl(
    ubsan_implicit_conversion_data *data,
    ubsan_value_handle src, ubsan_value_handle dst,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    const ubsan_type_descriptor *src_ty = data->from_type;
    const ubsan_type_descriptor *dst_ty = data->to_type;

    ubsan_value src_value = { src_ty, src };
    ubsan_value dst_value = { dst_ty, dst };

    bool src_integer = ubsan_type_descriptor_is_integer_ty(src_ty);
    bool src_signed = ubsan_type_descriptor_is_signed_integer_ty(src_ty);
    bool src_float = ubsan_type_descriptor_is_float_ty(src_ty);
    bool dst_integer = ubsan_type_descriptor_is_integer_ty(dst_ty);
    bool dst_signed = ubsan_type_descriptor_is_signed_integer_ty(dst_ty);
    bool dst_float = ubsan_type_descriptor_is_float_ty(dst_ty);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "implicit conversion"
    );

    if (src_integer && src_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "from type %s of value %" PRIdMAX " (%u-bit, signed)",
            ubsan_type_descriptor_get_type_name(src_ty),
            ubsan_value_get_s_int_value(&src_value),
            ubsan_type_descriptor_get_integer_bit_width(src_ty)
        );
    } else if (src_integer && !src_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "from type %s of value %" PRIuMAX " (%u-bit, unsigned)",
            ubsan_type_descriptor_get_type_name(src_ty),
            ubsan_value_get_u_int_value(&src_value),
            ubsan_type_descriptor_get_integer_bit_width(src_ty)
        );
    } else if (src_float) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "from type %s of value %" PRIfMAX " (%u-bit, signed)",
            ubsan_type_descriptor_get_type_name(src_ty),
            ubsan_value_get_float_value(&src_value),
            ubsan_type_descriptor_get_float_bit_width(src_ty)
        );
    } else {
        __builtin_unreachable();
    }

    if (dst_integer && dst_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "to type %s of value %" PRIdMAX " (%u-bit, signed)",
            ubsan_type_descriptor_get_type_name(dst_ty),
            ubsan_value_get_s_int_value(&dst_value),
            ubsan_type_descriptor_get_integer_bit_width(dst_ty)
        );
    } else if (dst_integer && !dst_signed) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "to type %s of value %" PRIuMAX " (%u-bit, unsigned)",
            ubsan_type_descriptor_get_type_name(dst_ty),
            ubsan_value_get_u_int_value(&dst_value),
            ubsan_type_descriptor_get_integer_bit_width(dst_ty)
        );
    } else if (dst_float) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "to type %s of value %" PRIfMAX " (%u-bit, signed)",
            ubsan_type_descriptor_get_type_name(dst_ty),
            ubsan_value_get_float_value(&dst_value),
            ubsan_type_descriptor_get_float_bit_width(dst_ty)
        );
    } else {
        __builtin_unreachable();
    }
}
UBSAN_RECOVERABLE_IMPL(
    implicit_conversion,
    (
        ubsan_implicit_conversion_data *data,
        ubsan_value_handle src, ubsan_value_handle dst
    ),
    data, src, dst
)

static void ubsan_handle_invalid_builtin_impl(
    ubsan_invalid_builtin_data *data,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "passing zero to %s, which is not a valid argument",
        data->kind == UBSAN_BCK_CTZ_PASSED_ZERO ? "ctz()" : data->kind == UBSAN_BCK_CLZ_PASSED_ZERO ? "clz()" : "<unknown>"
    );
}
UBSAN_RECOVERABLE_IMPL(
    invalid_builtin,
    (
        ubsan_invalid_builtin_data *data
    ),
    data
)

static void ubsan_handle_invalid_objc_cast_impl(
    ubsan_invalid_objc_cast_data *data,
    ubsan_value_handle pointer,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    const char *given_class = ubsan_get_objc_class_name(pointer);
    if (!given_class) {
        given_class = "<unknown type>";
    }

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "invalid ObjC cast, object is a %s, but expected a %s",
        given_class,
        ubsan_type_descriptor_get_type_name(data->expected_type)
    );
}
UBSAN_RECOVERABLE_IMPL(
    invalid_objc_cast,
    (
        ubsan_invalid_objc_cast_data *data,
        ubsan_value_handle pointer
    ),
    data, pointer
)

static void ubsan_handle_nonnull_return_v1_impl(
    ubsan_nonnull_return_data *data,
    ubsan_source_location *loc_ptr,
    enum klog_level level
) {
    if (!loc_ptr) {
        __builtin_unreachable();
    }
    ubsan_source_location loc = ubsan_source_location_acquire(loc_ptr);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "null pointer returned from function declared to never return null"
    );

    if (!ubsan_source_location_is_invalid(&data->attr_loc)) {
        klog_func(
            level, true, data->attr_loc.filename + INFO_SOURCE_PATH_SIZE, data->attr_loc.line,
            "returns_nonnull attribute specified here"
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    nonnull_return_v1,
    (
        ubsan_nonnull_return_data *data,
        ubsan_source_location *loc_ptr
    ),
    data, loc_ptr
)

static void ubsan_handle_nullability_return_v1_impl(
    ubsan_nonnull_return_data *data,
    ubsan_source_location *loc_ptr,
    enum klog_level level
) {
    if (!loc_ptr) {
        __builtin_unreachable();
    }
    ubsan_source_location loc = ubsan_source_location_acquire(loc_ptr);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "null pointer returned from function declared to never return null"
    );

    if (!ubsan_source_location_is_invalid(&data->attr_loc)) {
        klog_func(
            level, true, data->attr_loc.filename + INFO_SOURCE_PATH_SIZE, data->attr_loc.line,
            "_Nonnull return type annotation specified here"
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    nullability_return_v1,
    (
        ubsan_nonnull_return_data *data,
        ubsan_source_location *loc_ptr
    ),
    data, loc_ptr
)

static void ubsan_handle_nonnull_arg_impl(
    ubsan_nonnull_arg_data *data,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    ubsan_source_location attr_loc = ubsan_source_location_acquire(&data->attr_loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);
    assert(strlen(attr_loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "null pointer passed as argument %d, which is declared to never be null",
        data->arg_index
    );

    if (!ubsan_source_location_is_invalid(&attr_loc)) {
        klog_func(
            level, true, attr_loc.filename + INFO_SOURCE_PATH_SIZE, attr_loc.line,
            "nonnull attribute specified here"
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    nonnull_arg,
    (
        ubsan_nonnull_arg_data *data
    ),
    data
)

static void ubsan_handle_nullability_arg_impl(
    ubsan_nonnull_arg_data *data,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    ubsan_source_location attr_loc = ubsan_source_location_acquire(&data->attr_loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);
    assert(strlen(attr_loc.filename) >= INFO_SOURCE_PATH_SIZE);

    klog_func(
        level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
        "null pointer passed as argument %d, which is declared to never be null",
        data->arg_index
    );

    if (!ubsan_source_location_is_invalid(&attr_loc)) {
        klog_func(
            level, true, attr_loc.filename + INFO_SOURCE_PATH_SIZE, attr_loc.line,
            "_Nonnull type annotation specified here"
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    nullability_arg,
    (
        ubsan_nonnull_arg_data *data
    ),
    data
)

static void ubsan_handle_pointer_overflow_impl(
    ubsan_pointer_overflow_data *data,
    ubsan_value_handle base,
    ubsan_value_handle result,
    enum klog_level level
) {
    ubsan_source_location loc = ubsan_source_location_acquire(&data->loc);
    assert(strlen(loc.filename) >= INFO_SOURCE_PATH_SIZE);

    if (base == 0 && result == 0) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "applying zero offset to null pointer"
        );
    } else if (base == 0 && result != 0) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "applying non-zero offset %" PRIuPTR " to null pointer",
            result
        );
    } else if (base != 0 && result == 0) {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "applying non-zero offset to non-null pointer %p produced null pointer",
            (void *) base
        );
    } else if (((ptrdiff_t) base >= 0) == ((ptrdiff_t) result >= 0)) {
        if (base > result) {
            klog_func(
                level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
                "addition of unsigned offset to %p overflowed to %p",
                (void *) base,
                (void *) result
            );
        } else {
            klog_func(
                level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
                "subtraction of unsigned offset to %p overflowed to %p",
                (void *) base,
                (void *) result
            );
        }
    } else {
        klog_func(
            level, true, loc.filename + INFO_SOURCE_PATH_SIZE, loc.line,
            "pointer index expression with base %p overflowed to %p",
            (void *) base,
            (void *) result
        );
    }
}
UBSAN_RECOVERABLE_IMPL(
    pointer_overflow,
    (
        ubsan_pointer_overflow_data *data,
        ubsan_value_handle base,
        ubsan_value_handle result
    ),
    data, base, result
)

// TODO: CFI bad type
