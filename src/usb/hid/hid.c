#include "usb/hid.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_type_strs[] = {
    [USB_HID_TYPE(main)]        = "Main",
    [USB_HID_TYPE(global)]      = "Global",
    [USB_HID_TYPE(local)]       = "Local",
    [USB_HID_TYPE(reserved)]    = "Reserved",
};

static const char *usb_hid_main_item_tag_strs[] = {
    [USB_HID_MAIN_ITEM_TAG(input])                  = "Input",
    [USB_HID_MAIN_ITEM_TAG(output])                 = "Output",
    [USB_HID_MAIN_ITEM_TAG(feature])                = "Feature",
    [USB_HID_MAIN_ITEM_TAG(collection])             = "Collection",
    [USB_HID_MAIN_ITEM_TAG(end_collection])         = "End Collection",
};

static const char *usb_hid_global_item_tag_strs[] = {
    [USB_HID_GLOBAL_ITEM_TAG(usage_page])           = "Usage Page",
    [USB_HID_GLOBAL_ITEM_TAG(logical_minimum])      = "Logical Minimum",
    [USB_HID_GLOBAL_ITEM_TAG(logical_maximum])      = "Logical Maximum",
    [USB_HID_GLOBAL_ITEM_TAG(physical_minimum])     = "Physical Minimum",
    [USB_HID_GLOBAL_ITEM_TAG(physical_maximum])     = "Physical Maximum",
    [USB_HID_GLOBAL_ITEM_TAG(unit_exponent])        = "Unit Exponent",
    [USB_HID_GLOBAL_ITEM_TAG(unit])                 = "Unit",
    [USB_HID_GLOBAL_ITEM_TAG(report_size])          = "Report Size",
    [USB_HID_GLOBAL_ITEM_TAG(report_id])            = "Report ID",
    [USB_HID_GLOBAL_ITEM_TAG(report_count])         = "Report Count",
    [USB_HID_GLOBAL_ITEM_TAG(push])                 = "Push",
    [USB_HID_GLOBAL_ITEM_TAG(pop])                  = "Pop",
};

static const char *usb_hid_local_item_tag_strs[] = {
    [USB_HID_LOCAL_ITEM_TAG(usage)]                 = "Usage",
    [USB_HID_LOCAL_ITEM_TAG(usage_minimum)]         = "Usage Minimum",
    [USB_HID_LOCAL_ITEM_TAG(usage_maximum)]         = "Usage Maximum",
    [USB_HID_LOCAL_ITEM_TAG(designator_index)]      = "Designator Index",
    [USB_HID_LOCAL_ITEM_TAG(designator_minimum)]    = "Designator Minimum",
    [USB_HID_LOCAL_ITEM_TAG(designator_maximum)]    = "Designator Maximum",
    [USB_HID_LOCAL_ITEM_TAG(string_index)]          = "String Index",
    [USB_HID_LOCAL_ITEM_TAG(string_maximum)]        = "String Minimum",
    [USB_HID_LOCAL_ITEM_TAG(string_minimum)]        = "String Maximum",
    [USB_HID_LOCAL_ITEM_TAG(delimiter)]             = "Delimiter",
};

const char *usb_hid_type_to_str(usb_hid_type type) {
    if (type < countof(usb_hid_type_strs)) {
        return usb_hid_type_strs[type];
    } else {
        return NULL;
    }
}

const char *usb_hid_main_item_tag_to_str(usb_hid_main_item_tag main_item_tag) {
    if (main_item_tag < countof(usb_hid_main_item_tag_strs)) {
        return usb_hid_main_item_tag_strs[main_item_tag];
    } else {
        return NULL;
    }
}

const char *usb_hid_global_item_tag_to_str(usb_hid_global_item_tag global_item_tag) {
    if (global_item_tag < countof(usb_hid_global_item_tag_strs)) {
        return usb_hid_global_item_tag_strs[global_item_tag];
    } else {
        return NULL;
    }
}

const char *usb_hid_local_item_tag_to_str(usb_hid_local_item_tag local_item_tag) {
    if (local_item_tag < countof(usb_hid_local_item_tag_strs)) {
        return usb_hid_local_item_tag_strs[local_item_tag];
    } else {
        return NULL;
    }
}
