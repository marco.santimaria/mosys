#include "usb/hid/usage.h"
#include <stddef.h>

const char *usb_hid_usage_to_str(usb_hid_usage usage) {
    usb_hid_usage_page  usage_page = (usage >> 16) & 0xFFFF;
    usb_hid_usage_id    usage_id   = (usage >>  0) & 0xFFFF;

    switch (usage_page) {
    case USB_HID_USAGE_PAGE(generic_desktop):
        return usb_hid_usage_id_generic_desktop_to_str(usage_id);
    case USB_HID_USAGE_PAGE(keyboard_keypad):
        return usb_hid_usage_id_keyboard_keypad_to_str(usage_id);
    case USB_HID_USAGE_PAGE(led):
        return usb_hid_usage_id_led_to_str(usage_id);
    case USB_HID_USAGE_PAGE(button):
        return usb_hid_usage_id_button_to_str(usage_id);
    case USB_HID_USAGE_PAGE(consumer):
        return usb_hid_usage_id_consumer_to_str(usage_id);
    default:
        return NULL;
    }
}
