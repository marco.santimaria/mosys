#include "usb/hid/usage_pages.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_usage_page_strs[] = {
    [USB_HID_USAGE_PAGE(generic_desktop)]   = "Generic Desktop",
    [USB_HID_USAGE_PAGE(keyboard_keypad)]   = "Keyboard/Keypad",
    [USB_HID_USAGE_PAGE(led)]               = "LED",
    [USB_HID_USAGE_PAGE(button)]            = "Button",
    [USB_HID_USAGE_PAGE(consumer)]          = "Consumer",
};

const char *usb_hid_usage_page_to_str(usb_hid_usage_page usage_page) {
    if (usage_page < countof(usb_hid_usage_page_strs)) {
        return usb_hid_usage_page_strs[usage_page];
    } else {
        return NULL;
    }
}
