#include "usb/hid/usage_pages/button.h"
#include "kio/kio.h"

const char *usb_hid_usage_id_button_to_str(usb_hid_usage_id_button usage_id) {
    static char string_buf[] = "Button 65535";  // Space for any result

    switch (usage_id) {
    case 0:
        return "No Button Pressed";
    case 1:
        return "Button 1 (primary/trigger)";
    case 2:
        return "Button 2 (secondary)";
    case 3:
        return "Button 3 (tertiary)";
    default:
        kio_snprintf(string_buf, sizeof(string_buf), "Button %hu", (unsigned short) usage_id);
        return string_buf;
    }
}
