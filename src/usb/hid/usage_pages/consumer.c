#include "usb/hid/usage_pages/consumer.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_usage_id_consumer_strs[] = {
    [USB_HID_USAGE_ID_CONSUMER(consumer_control)]       = "Consumer Control",
    [USB_HID_USAGE_ID_CONSUMER(numeric_key_pad)]        = "Numeric Key Pad",
    [USB_HID_USAGE_ID_CONSUMER(programmable_buttons)]   = "Programmable Buttons",
    [USB_HID_USAGE_ID_CONSUMER(microphone)]             = "Microphone",
    [USB_HID_USAGE_ID_CONSUMER(headphone)]              = "Headphone",
    [USB_HID_USAGE_ID_CONSUMER(graphic_equalizer)]      = "Graphic Equalizer",
    [USB_HID_USAGE_ID_CONSUMER(ac_pan)]                 = "AC Pan",
    // TODO: Add the rest
};

const char *usb_hid_usage_id_consumer_to_str(usb_hid_usage_id_consumer usage_id) {
    if (usage_id < countof(usb_hid_usage_id_consumer_strs)) {
        return usb_hid_usage_id_consumer_strs[usage_id];
    } else {
        return NULL;
    }
}
