#include "usb/hid/usage_pages/generic_desktop.h"
#include "util/util.h"
#include <stddef.h>

static const char *const usb_hid_usage_id_generic_desktop_strs[] = {
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(pointer)]             = "Pointer",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(mouse)]               = "Mouse",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(joystick)]            = "Joystick",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(gamepad)]             = "Gamepad",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(keyboard)]            = "Keyboard",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(keypad)]              = "Keypad",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(x)]                   = "X",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(y)]                   = "Y",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(z)]                   = "Z",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(system_control)]      = "System Control",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(system_power_down)]   = "System Power Down",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(system_sleep)]        = "System Sleep",
    [USB_HID_USAGE_ID_GENERIC_DESKTOP(system_wake_up)]      = "System Wake Up",
    // TODO: Add the rest
};

const char *usb_hid_usage_id_generic_desktop_to_str(usb_hid_usage_id_generic_desktop usage_id) {
    if (usage_id < countof(usb_hid_usage_id_generic_desktop_strs)) {
        return usb_hid_usage_id_generic_desktop_strs[usage_id];
    } else {
        return NULL;
    }
}
