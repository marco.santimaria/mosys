#include "vfs/devreg.h"
#include "kio/klog.h"
#include "util/util.h"
#include <string.h>

static devreg *devreg_list = NULL;
static rwlock devreg_list_rwl = RWLOCK_INIT;

devreg *devreg_new(const char *name, int major, int minor) {
    devreg *dr = kalloc(sizeof(devreg));
    if (dr) {
        rwlock_init(&dr->rwl);
        dr->ref_count = 1;
        strncpy(dr->name, name, sizeof(dr->name));
        dr->name[sizeof(dr->name) - 1] = '\0';
        dr->major = major;
        dr->minor = minor;
    }

    RWLOCK_WRITE_WITH(&devreg_list_rwl, {
        // Advance next_ptr to point to the current NULL pointer in list
        devreg **next_ptr = &devreg_list;
        while (*next_ptr) {
            next_ptr = &((*next_ptr)->next);
        }

        // Add fst to list
        dr->next = NULL;
        *next_ptr = dr;
    })

    return dr;
}

void devreg_delete(devreg *dr) {
    RWLOCK_WRITE_WITH(&devreg_list_rwl, {
        // Advance next_ptr to point to an entry pointing to dr
        devreg **next_ptr = &devreg_list;
        while (*next_ptr && *next_ptr != dr) {
            next_ptr = &((*next_ptr)->next);
        }

        if (!*next_ptr) {
            kpanic("Tried to delete devreg not in list");
        }

        // Remove dr from the list
        *next_ptr = (*next_ptr)->next;
    })

    if (dr->ref_count != 0) {
        kpanic("Tried to delete still-referenced devreg");
    }
    kfree(dr);
}

void devreg_ref(devreg *dr) {
    RWLOCK_WRITE_WITH(&dr->rwl, {
        if (dr->ref_count == SIZE_MAX) {
            kpanic("Overflowed devreg ref_count");
        }
        dr->ref_count += 1;
    })
}

void devreg_unref(devreg *dr) {
    RWLOCK_WRITE_WITH(&dr->rwl, {
        if (dr->ref_count == 0) {
            kpanic("Underflowed devreg ref_count");
        }
        dr->ref_count -= 1;

        if (dr->ref_count == 0) {
            devreg_delete(dr);
            dr = NULL;
            RWLOCK_RETURN_NO_UNLOCK();
        }
    })
}

devreg *devreg_lookup(const char *name) {
    devreg *dr;

    RWLOCK_READ_WITH(&devreg_list_rwl, {
        for (dr = devreg_list; dr; dr = dr->next) {
            if (strcmp(dr->name, name) == 0) {
                break;
            }
        }

        if (dr)
            devreg_ref(dr);
    })

    return dr;
}

devreg *devreg_get_nth(unsigned long n) {
    devreg *dr;

    RWLOCK_READ_WITH(&devreg_list_rwl, {
        dr = devreg_list;
        for (; n > 0 && dr; --n) {
            dr = dr->next;
        }

        if (dr)
            devreg_ref(dr);
    })

    return dr;
}
