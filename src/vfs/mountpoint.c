#include "vfs/mountpoint.h"

static mountpoint *mountpoint_list = NULL;
static rwlock mountpoint_list_rwl = RWLOCK_INIT;

mountpoint *vfs_mountpoint_new(superblock *sb, const char *mount_path) {
    mountpoint *mp = kalloc(sizeof(mountpoint));
    if (!mp) {
        return NULL;
    }

    fill_zero(mp);
    mp->ref_count = 1;
    mp->sb = sb;
    mp->mount_path = mount_path;

    RWLOCK_WRITE_WITH(&mountpoint_list_rwl, {
        // Advance next_ptr to point to the current NULL pointer in list
        mountpoint **next_ptr = &mountpoint_list;
        while (*next_ptr != NULL) {
            next_ptr = &((*next_ptr)->next);
        }

        (*next_ptr) = mp;
    })

    return mp;
}

// mountpoint_list_rwl must already be locked for write
void vfs_mountpoint_delete(mountpoint *mp) {
    if (mp->ref_count != 0) {
        kpanic("Tried to delete still-referenced mountpoint");
    }

    RWLOCK_WRITE_WITH(&mountpoint_list_rwl, {
        // Advance next_ptr to point to mp
        mountpoint **next_ptr = &mountpoint_list;
        while (*next_ptr != mp && *next_ptr != NULL) {
            next_ptr = &((*next_ptr)->next);
        }
        if (!*next_ptr) {
            kpanic("Mountpoint to delete is not in mountpoint list");
        }
        *next_ptr = (*next_ptr)->next;
    })

    kfree(mp);
}

void vfs_mountpoint_ref_nolock(mountpoint *mp) {
    if (mp->ref_count == SIZE_MAX) {
        kpanic("Overflowed mountpoint ref_count");
    }
    mp->ref_count += 1;
}

void vfs_mountpoint_unref_nolock(mountpoint *mp) {
    if (mp->ref_count == 0) {
        kpanic("Underflowed mountpoint ref_count");
    }
    mp->ref_count -= 1;

    if (mp->ref_count == 0) {
        vfs_mountpoint_delete(mp);
        mp = NULL;
    }
}

void vfs_mountpoint_ref(mountpoint *mp) {
    RWLOCK_WRITE_WITH(&mountpoint_list_rwl, {
        vfs_mountpoint_ref_nolock(mp);
    })
}

void vfs_mountpoint_unref(mountpoint *mp) {
    RWLOCK_WRITE_WITH(&mountpoint_list_rwl, {
        vfs_mountpoint_unref_nolock(mp);
    })
}

mountpoint *vfs_mountpoint_lookup(const char *path, const char **subpath_ptr) {
    if (path[0] == '\0' ||
        path[0] != '/') {
        // Return early on an empty or non-absolute lookup
        return NULL;
    }

    size_t best_len = 0;
    mountpoint *best_mp = NULL;

    RWLOCK_READ_WITH(&mountpoint_list_rwl, {
        for (mountpoint *it = mountpoint_list; it; it = it->next) {
            size_t mount_path_len = strlen(it->mount_path);
            if (0 == strncmp(path, it->mount_path, mount_path_len)) {
                // Found matching
                if (mount_path_len > best_len) {
                    // Is new longest
                    best_len = mount_path_len;
                    best_mp = it;
                }
            }
        }
    })

    if (best_mp) {
        *subpath_ptr = path + best_len;
        vfs_mountpoint_ref(best_mp);
    }

    return best_mp;
}
