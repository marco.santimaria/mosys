#include "vfs/superblock.h"

superblock *vfs_superblock_new(filesystem_type *type) {
    superblock *sb = kalloc(sizeof(superblock));
    if (!sb) {
        return NULL;
    }

    mutex_init(&sb->mtx);
    sb->ref_count = 1;
    sb->type = type;

    sb->vn_hshtbl = (hashtable) HASHTABLE_INIT;
    mutex_init(&sb->vn_hshtbl_mtx);

    return sb;
}

void vfs_superblock_delete(superblock *sb) {
    kfree(sb);
}

void vfs_superblock_ref(superblock *sb) {
    MUTEX_WITH(&sb->mtx, {
        if (sb->ref_count == SIZE_MAX) {
            kpanic("Overflowed superblock ref_count");
        }
        sb->ref_count += 1;
    })
}

void vfs_superblock_unref(superblock *sb) {
    MUTEX_WITH(&sb->mtx, {
        if (sb->ref_count == 0) {
            kpanic("Underflowed superblock ref_count");
        }
        sb->ref_count -= 1;

        if (sb->ref_count == 0) {
            vfs_superblock_delete(sb);
            sb = NULL;
            MUTEX_RETURN_NO_UNLOCK();   // Return without releasing mutex, it's been deleted
        }
    })
}
