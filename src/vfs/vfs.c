#include "vfs/vfs.h"
#include "uapi/errno.h"
#include "fs/devfs.h"
#include "fs/sockfs.h"
#include "kalloc.h"
#include "process/process.h"
#include "kio/klog.h"
#include "util/rwlock.h"
#include "util/util.h"
#include "vfs/fdesc.h"
#include <limits.h>
#include <string.h>
#include <stdint.h>

// TODO: Should we lock vn before the fops calls?

// Calling function must call vfs_fdesk_unref on non-NULL return value when done
static inline fdesc *fdesc_from_fd(int fd) {
    if (fd < 0 || fd >= VFS_MAX_OPEN_FILES) {
        return NULL;
    }

    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        kpanic("Process has no file descriptor list");
    }

    fdesc *fdsc;
    MUTEX_WITH(&fdl->mtx, {
        fdsc = fdl->fds[fd];
        if (fdsc) {
            vfs_fdesc_ref(fdsc);
        }
    })

    return fdsc;
}

static inline void fdesc_clear_fd(int fd) {
    if (fd < 0 || fd >= VFS_MAX_OPEN_FILES) {
        return;
    }

    fdesc_list *fdl = thread_current->owner_process->fdl;
    if (!fdl) {
        kpanic("Process has no file descriptor list");
    }

    MUTEX_WITH(&fdl->mtx, {
        fdesc *fdsc = fdl->fds[fd];
        fdl->fds[fd] = NULL;
        if (fdsc) {
            vfs_fdesc_unref(fdsc);
        }
    })
}

int vfs_open(flex_const_str pathname, int flags, mode_t mode, int *errno_ptr) {
    fdesc *fdsc = vfs_open_fdesc(pathname, flags, mode, errno_ptr);
    if (!fdsc) {
        return -1;
    }

    int fd = vfs_register_fd(fdsc, errno_ptr);
    if (fd < 0) {
        vfs_close_fdesc(fdsc, errno_ptr);
    }
    return fd;
}

int vfs_close(int fd, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_close_fdesc(fdsc, errno_ptr);

    // Remove file descriptor from process's file descriptor list
    fdesc_clear_fd(fd);

    // TODO: Does this actually close the right thing (device/socket fs vs. actual fs)

    // Delete the file descriptor
    vfs_vnode_unref(fdsc->vn);
    fdsc->vn = NULL;
    vfs_fdesc_unref(fdsc);
    fdsc = NULL;

    return result;
}

void vfs_close_all(void) {
    int errno_tmp = 0;
    for (int fd = 0; fd < VFS_MAX_OPEN_FILES; ++fd) {
        // Discard errors
        vfs_close(fd, &errno_tmp);
    }
}

ssize_t vfs_read(int fd, flex_mut buf, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_read_fdesc(fdsc, buf, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_pread(int fd, flex_mut buf, size_t count, off_t offset, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_pread_fdesc(fdsc, buf, count, offset, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_write(int fd, flex_const buf, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_write_fdesc(fdsc, buf, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_pwrite(int fd, flex_const buf, size_t count, off_t offset, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_pwrite_fdesc(fdsc, buf, count, offset, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_ioctl(int fd, unsigned long request, flex_mut argp, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_ioctl_fdesc(fdsc, request, argp, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_getdents(int fd, FLEX_MUT(dirent) drnt, size_t count, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_getdents_fdesc(fdsc, drnt, count, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

off_t vfs_lseek(int fd, off_t offset, int whence, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    ssize_t result = vfs_lseek_fdesc(fdsc, offset, whence, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_socket(int domain, int type, int protocol, int *errno_ptr) {
    fdesc *fdsc = vfs_socket_fdesc(domain, type, protocol, errno_ptr);
    if (!fdsc) {
        return -1;
    }

    int fd = vfs_register_fd(fdsc, errno_ptr);
    if (fd < 0) {
        vfs_close_fdesc(fdsc, errno_ptr);
    }
    return fd;
}

int vfs_socketpair(int domain, int type, int protocol, FLEX_MUT(int) sv, int *errno_ptr) {
    fdesc *fdscs[2];
    int result = vfs_socketpair_fdesc(domain, type, protocol, fdscs, errno_ptr);
    if (result < 0) {
        return -1;
    }

    int sv_k[2];
    sv_k[0] = vfs_register_fd(fdscs[0], errno_ptr);
    if (sv_k[0] < 0) {
        vfs_close_fdesc(fdscs[0], errno_ptr);
        vfs_close_fdesc(fdscs[1], errno_ptr);
        return -1;
    }
    sv_k[1] = vfs_register_fd(fdscs[1], errno_ptr);
    if (sv_k[1] < 0) {
        fdesc_clear_fd(sv_k[0]);
        vfs_close_fdesc(fdscs[0], errno_ptr);
        vfs_close_fdesc(fdscs[1], errno_ptr);
        return -1;
    }

    if (!flex_to(sv, sv_k, 2 * sizeof(int))) {
        fdesc_clear_fd(sv_k[0]);
        fdesc_clear_fd(sv_k[1]);
        vfs_close_fdesc(fdscs[0], errno_ptr);
        vfs_close_fdesc(fdscs[1], errno_ptr);
        *errno_ptr = EFAULT;
        return -1;
    }

    return 0;
}

int vfs_bind(int sockfd, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(sockfd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_bind_fdesc(fdsc, addr, addrlen, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_recvfrom(int sockfd, flex_mut buf, size_t len, int flags,
                     FLEX_MUT(struct sockaddr) src_addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(sockfd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_recvfrom_fdesc(fdsc, buf, len, flags, src_addr, addrlen, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

ssize_t vfs_sendto(int sockfd, flex_const buf, size_t len, int flags,
                   FLEX_CONST(struct sockaddr) dest_addr, socklen_t addrlen, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(sockfd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_sendto_fdesc(fdsc, buf, len, flags, dest_addr, addrlen, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_accept(int sockfd, FLEX_MUT(struct sockaddr) addr, FLEX_MUT(socklen_t) addrlen, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(sockfd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_accept_fdesc(fdsc, addr, addrlen, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_connect(int sockfd, FLEX_CONST(struct sockaddr) addr, socklen_t addrlen, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(sockfd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_connect_fdesc(fdsc, addr, addrlen, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_mount(flex_const_str source, flex_const_str target,
              flex_const_str filesystemtype, unsigned long mountflags,
              flex_const data,
              int *errno_ptr) {
    vnode *mount_vn = vfs_filesystem_mount(source, filesystemtype, mountflags, data, errno_ptr);
    if (!mount_vn) {
        // *errno_ptr has been set by vfs_filesystem_mount
        return -1;
    }

    flex_const target_meas = flex_str_measure(target);
    flex_const_pinned target_pin = flex_pin_must_cleanup(target_meas);
    if (target_pin.fault) {
        vfs_vnode_unref(mount_vn);
        mount_vn = NULL;
        *errno_ptr = EFAULT;
        return -1;
    }

    // TODO: Decide when or when not to create a target-less mount
    if (target_pin.size > 1) {
        // Create mountpoint
        vfs_superblock_ref(mount_vn->sb);
        mountpoint *mp = vfs_mountpoint_new(mount_vn->sb, target_pin.ptr);
        if (!mp) {
            vfs_superblock_unref(mount_vn->sb);
            flex_unpin(&target_pin);
            vfs_vnode_unref(mount_vn);
            mount_vn = NULL;
            *errno_ptr = EINVAL;    // TODO: What is correct error here?
            return -1;
        }
    }

    flex_unpin(&target_pin);
    return 0;
}

int vfs_chdir(flex_const_str path, int *errno_ptr) {
    // TODO: Add O_DIRECTORY
    fdesc *fdsc = vfs_open_fdesc(path, O_RDONLY, 0, errno_ptr);
    if (!fdsc) {
        return -1;
    }

    int result = vfs_fchdir_fdesc(fdsc, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}

int vfs_fchdir(int fd, int *errno_ptr) {
    fdesc *fdsc = fdesc_from_fd(fd);
    if (!fdsc) {
        *errno_ptr = EBADF;
        return -1;
    }

    int result = vfs_fchdir_fdesc(fdsc, errno_ptr);

    vfs_fdesc_unref(fdsc);
    return result;
}
