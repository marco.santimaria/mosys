#include "vfs/vnode.h"

vnode *vfs_vnode_new(superblock *sb) {
    vnode *vn = kalloc(sizeof(vnode));
    if (vn) {
        mutex_init(&vn->mtx);
        vn->ref_count = 1;
        vn->sb = sb;
    }

    return vn;
}

void vfs_vnode_delete(vnode *vn) {
    if (vn->ref_count != 0) {
        kpanic("Tried to delete still-referenced vnode");
    }
    if (vn->parent) {
        vfs_vnode_unref(vn->parent);
        vn->parent = NULL;
    }
    kfree(vn);
}

void vfs_vnode_ref(vnode *vn) {
    MUTEX_WITH(&vn->mtx, {
        if (vn->ref_count == SIZE_MAX) {
            kpanic("Overflowed vnode ref_count");
        }
        vn->ref_count += 1;
    })
}

void vfs_vnode_unref(vnode *vn) {
    MUTEX_WITH(&vn->mtx, {
        if (vn->ref_count == 0) {
            kpanic("Underflowed vnode ref_count");
        }
        vn->ref_count -= 1;

        if (vn->ref_count == 0) {
            superblock *sb = vn->sb;    // Does this select the right superblock in the case of devices, etc.?
            if (!sb->type || !sb->type->fsops || !sb->type->fsops->delete_vnode) {
                kpanic("Expected to find delete_vnode for mount");
            }
            sb->type->fsops->delete_vnode(sb, vn);
            MUTEX_RETURN_NO_UNLOCK();   // Return without releasing mutex, it's been deleted
        }
    })
}
