set(CMAKE_SYSTEM_NAME "rosco_m68k")
set(CMAKE_SYSTEM_VERSION 1.3)
set(CMAKE_SYSTEM_PROCESSOR m68k)

set(CMAKE_C_COMPILER m68k-elf-gcc)
set(CMAKE_CXX_COMPILER m68k-elf-g++)
set(CMAKE_ASM_COMPILER m68k-elf-gcc)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

add_compile_options(-m68010)
add_link_options(-m68010)

function(mosys_post_build TARGET_NAME)
    add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy mosys.elf "$<TARGET_FILE_DIR:${TARGET_NAME}>/ROSCODE1.ELF"
        COMMAND ${CMAKE_OBJCOPY} ARGS -O binary mosys.elf "$<TARGET_FILE_DIR:${TARGET_NAME}>/ROSCODE1.BIN"
    )
endfunction()
